#pragma once
#ifndef PATHFINDING_H
#define PATHFINDING_H

#include <SDL.h>
#include <vector>
#include "Node.h"
#include "Collidable.h"

class PathFinding
{
public:
	//This is the constructor to create the base for the pathfinding.
	PathFinding();
	//This will create the map for the current level.  You will need to
	//call this once for each new map for each pathfinding,
	void createMap(int widthLevel, int heightLevel, double xSize, double ySize, std::vector<Collidable> listOfCollidables);
	//After the map is created a you call this to find a path from one SDL_Rect
	//to another.
	std::vector<Node*> findPath(SDL_Rect start, SDL_Rect end, double xSize, double ySize);
	//This cleans up the pointers.
	void cleanup();
private:
	//This list help find the path.
	std::vector<Node*> openList;
	std::vector<Node*> closedList;
	//This stores the map in a matrix.
	std::vector<std::vector<Node*>> map;
};

#endif