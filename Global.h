#pragma once
#ifndef GLOBAL_H
#define GLOBAL_H

#include <String>
#include <SDL.h>
#include <SDL_image.h>

#include "Collidable.h"

//This will house all the enums, ints, doubles, functions etc. that
//most of the game will use.

//This enum is used to define sides of any sprite in this game.
enum Side
{
	UP,
	DOWN,
	LEFT,
	RIGHT,
	NA
};

//These variables will not change throughout the game.
extern int SCREEN_WIDTH,
		   SCREEN_HEIGHT,
		   LEVEL_EDITOR_SCREEN_WIDTH,
		   LEVEL_EDITOR_SCREEN_HEIGHT,
		   TILES_IN_WIDTH,
		   TILES_IN_HEIGHT,
		   SCREEN_BPP,
		   TILE_SIZE,
		   MAX_FRAME_INDEX,
		   WLAKABLE_PIXEL_AREA,
		   BBABY_MONSTER_NUMBER,
		   READER_ITEM_NUMBER,
		   POWER_UP_ITEM_NUMBER,
		   READERS_Y_LEVEL,
		   POWER_UPS_Y_LEVEL,
		   READER_CLIPBOARD_ITEM_NUMBER,
		   DOOR_TILE_NUMBER,
		   DIALOGUE_ITEM_NUMBER,
		   //This value will be used as a sentinal value; a value that
		   //tells the program that the value is invalid.
		   NA_VALUE,
		   //A negative NA value.
		   NNA_VALUE,
		   DEFAULT_ZERO;

extern std::string LEVEL_MASTER_TEXT,
				   MORA_ICON_FILENAME,
	               BBABY_ICON_FILENAME,
	               CLIPBOARD_ITEM_THIS_IS_A_TEST_ICON_FILENAME,
				   HP_CONTAINER_FILENAME,
				   HP_BAR_FILENAME,
				   MP_CONTAINER_FILENAME,
				   MP_BAR_FILENAME,
				   FEAR_CONTAINER_FILENAME,
				   FEAR_BAR_FILENAME,
				   FEAR_ICON_FILENAME,
				   BLACK_OUT_BOX_FILENAME,
				   DAMAGE_INDICATOR_FILENAME,
				   POWER_UP_FILENAME,
				   GAME_OVER_FILENAME,
				   CLIPBOARD_TEST_PRINT,
				   DEBUG_LEVEL_TILESHEET,
				   DEBUG_LEVEL;

//This function optimizes the image for the surface it is being blit on.
SDL_Surface *load_image(std::string filename); 

//This function blits the image to its destination surface.
void apply_surface(int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip);

//This checks collision between two collision boxes.
bool checkCollision(CollisionBox A, CollisionBox B);

//This checks to make sure that the boxes are able to slide around each other but not go through each other.
bool checkMovingCollision(CollisionBox A, CollisionBox B);

double distance(CollisionBox A, CollisionBox B);

//This checks which side is being collided, where A is the object which we are finding the side for.
Side checkSide(CollisionBox A, CollisionBox B);

#endif