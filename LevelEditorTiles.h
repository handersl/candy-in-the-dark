#pragma once
#ifndef LEVELEDITORTILES_H
#define LEVELEDITORTILES_H

#include <SDL.h>
#include <string>

//This is the level editor tile that we will use as the basic building block
//for our level editor.  Basically it is an icon that stores information
//depending on its type.  More comments in "LevelEditorTile.cpp"

enum levelEditorTileType
{
	ground, mora, monster, item, NATileType
};

enum monsterType
{
	Bbaby, NAMonsterType
};

enum itemType
{
	reader, powerUp, dialogue, NAItemType
};

enum readerTileType
{
	clipboardReaderType, NAReaderType
};

class LevelEditorTiles
{
public:
	LevelEditorTiles();
	LevelEditorTiles(levelEditorTileType tileType);
	void render(SDL_Surface* dest);
	void cleanup();
	void setPosition(double x, double y)
	{ xPos = x; yPos = y; }
	void setGroundTile(std::string groundT);
	void setMonsterType(monsterType monType);
	void setItemType(itemType ittype);
	void setReaderTileType(readerTileType readT);
	void setReaderPrintString(std::string readerPS);
	void setNextLevelString(std::string nlString);
	void setDialogueString(std::string dString);
	void setDialogueIconString(std::string icoString);
	void setDialogueCharacterString(std::string charString);
	double getXPosition()
	{ return xPos; }
	double getYPosition()
	{ return yPos; }
	levelEditorTileType getTileType()
	{ return thisTileType; }
	std::string getGroundTile()
	{ return groundTile; }
	monsterType getMonsterType()
	{ return monsterTileType; }
	itemType getItemType()
	{ return itemTileType; }
	readerTileType getReaderTileType()
	{ return readType; }
	std::string getReaderPrintString()
	{ return readerPrintString; }
	std::string getNextLevelString()
	{ return nextLevelString; }
	std::string getDialogueString()
	{ return dialogueString; }
	std::string getIconString()
	{ return iconString; }
	std::string getCharacterString()
	{ return characterString; }
private:
	SDL_Surface* icon;
	double xPos;
	double yPos;
	bool imageSet;
	levelEditorTileType thisTileType;
	std::string groundTile;
	monsterType monsterTileType;
	itemType itemTileType;
	readerTileType readType;
	std::string readerPrintString;
	std::string nextLevelString;
	std::string dialogueString;
	std::string iconString;
	std::string characterString;

	static std::string const DEBUG_LEVEL_TILE00;
	static std::string const DEBUG_LEVEL_TILE01;
	static std::string const DEBUG_LEVEL_TILE02;
	static std::string const DEBUG_LEVEL_TILE03;
	static std::string const DEBUG_LEVEL_TILE04;
	static std::string const DEBUG_LEVEL_TILE05;
	static std::string const DEBUG_LEVEL_TILE06;
	static std::string const DEBUG_LEVEL_TILE07;
	static std::string const DEBUG_LEVEL_TILE08;
	static std::string const DEBUG_LEVEL_TILE09;
	static std::string const DEBUG_LEVEL_TILE10;

	static std::string const TUT_BUNNY_FILENAME;
};

#endif