#include "Engine.h"
#include "Global.h"
#include <vector>
#include <fstream>

Engine::Engine()
{
	screenBackground = NULL;
}

bool Engine::init()
{
	//Initialize everything.
	SDL_Init(SDL_INIT_EVERYTHING);
	//Setup the screen for fullscreen with the size of the game window.
	screenBackground = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE);

	//Init the music player and load the files.
	musicPlayer.init();
	musicPlayer.loadFiles();

	//Init. the menu.
	menu.init();
	menu.setMusicPlayer(&musicPlayer);

	gameOver = load_image(GAME_OVER_FILENAME);

	//The engine is running now but the game and editor are not.
	running = true;
	gameRunning = false;
	levelEditorRunning = false;
	currentButtonStatus = false;
	prevButtonStatus = false;

	return true;
}
void Engine::cleanup()
{
	//Clean up everything and quit.
	SDL_FreeSurface(screenBackground);
	SDL_FreeSurface(gameOver);
	if(gameRunning)
	{
		currentLevel.cleanup();
	}
	if(levelEditorRunning)
	{
		levelEditor.cleanup();
	}
	musicPlayer.cleanup();
	menu.cleanup();
	SDL_Quit();
}
void Engine::processEvent(Uint8* keys)
{
	//If escaped is pressed we will exit.
	if(keys[SDLK_ESCAPE])
	{
		notRunning();
	}
}

void Engine::update(double delta)
{
	prevButtonStatus = currentButtonStatus;
	currentButtonStatus = false;
	//Update the game.
	menu.update();
	if(menu.getStatus() == MENU_START)
	{
		screenBackground = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE);

		//Find the level to load.
		std::string firstLevel = "";
		std::ifstream levelMaster(LEVEL_MASTER_TEXT);
		if (levelMaster.is_open())
		{
			while (levelMaster.good())
			{
				std::getline(levelMaster, firstLevel);
			}
		}
		
		//Add Mora to the level.
		Mora mora(DEFAULT_ZERO, DEFAULT_ZERO);
		mora.setMusicPlayer(&musicPlayer);
		mora.init();
		currentLevel.addMora(mora);
		//Load the current level using the level loader.
		levelLoader.loadLevel(firstLevel, &currentLevel, &musicPlayer);
		//Initialize the level and load the assests where they need to be.
		currentLevel.init(DEBUG_LEVEL_TILESHEET);
		gameRunning = true;
		menu.pauseMenu();
	}
	else if(menu.getStatus() == MENU_LEVEL_EDITOR)
	{
		screenBackground = SDL_SetVideoMode(LEVEL_EDITOR_SCREEN_WIDTH, LEVEL_EDITOR_SCREEN_HEIGHT, SCREEN_BPP, SDL_SWSURFACE);
		//Initialize the level editor.
		levelEditor.init();
		levelEditorRunning = true;
		menu.pauseMenu();
	}
	else if(menu.getStatus() == MENU_QUIT)
	{
		notRunning();
	}

	if(levelEditorRunning)
	{
		levelEditor.update(delta);
	}
	else if(gameRunning)
	{
		if(!currentLevel.getMora().getDead())
		{
			//If there is a next level to be loaded.
			if(currentLevel.getNextLevelString() != "")
			{
				//Clean up the old level.
				currentLevel.softCleanup();
				//Reset the level so a new level can be loaded.
				currentLevel.reset();
				//Load the new level.
				levelLoader.loadLevel(currentLevel.getNextLevelString(), &currentLevel, &musicPlayer);
				currentLevel.clearNextLevelName();
				//Initialize the new loaded level.
				currentLevel.init(DEBUG_LEVEL_TILESHEET);
			}
			//Update the level.
			currentLevel.update(delta);
		}
	}
	while( SDL_PollEvent( &event ) )
    {
		if(gameRunning)
		{
			if(currentLevel.getMora().getDead())
			{
				if(event.type == SDL_KEYDOWN)
				{
					gameRunning = false;
					//Clean up the old level.
					currentLevel.cleanup();
					//Reset the level so a new level can be loaded.
					currentLevel.reset();
					menu.unpauseMenu();
				}
			}
		}

		//Taken from LazyFoo tutorials.
		//If the user has Xed out the window
        if( event.type == SDL_QUIT )
        {
            //Quit the program
            notRunning();
        }
	}
}

void Engine::render()
{
	//Blit the images on the screen.
	if(levelEditorRunning)
	{
		levelEditor.render(screenBackground);
	}
	else if(gameRunning)
	{
		currentLevel.render(screenBackground);
	}
	else
	{
		menu.render(screenBackground);
	}

	if(gameRunning)
	{
		if(currentLevel.getMora().getDead())
		{
			apply_surface(DEFAULT_ZERO, DEFAULT_ZERO, gameOver, screenBackground, NULL);
		}
	}

	//Update the screen with the new images.
	SDL_Flip(screenBackground);
}

void Engine::notRunning()
{
	//turn everything off.
	running = false;
	levelEditorRunning = false;
	gameRunning = false;
}