#include "Level.h"

#include "EnemyBBaby.h"
#include "LevelLoader.h"

Level::Level()
{
}

bool Level::init(std::string tileSheet)
{
	//Create the level.

	//If this string stops being "" we will load the level it points to.
	nextLevelName = "";

	//Set the tilesheet for the map.
	levelMap.setTileSheet(tileSheet);
		
	//Create more effecient walls.
	levelMap.getConsolidatedWalls();
	listOfWalls = levelMap.getLargeWalls();
	//Create the map for the monsters.
	makeMapForMonster(listOfWalls, SCREEN_WIDTH, SCREEN_HEIGHT);

	firstRun = true;

	blackoutSpriteSheet = load_image(BLACK_OUT_BOX_FILENAME);

	return true;
}

void Level::makeMapForMonster(std::vector<Collidable> listOfCollidables, int widthLevel, int heightLevel)
{
	for each(Monsters* mon in listOfMonsters)
	{
		mon->makeMap(widthLevel, heightLevel, listOfCollidables);
	}
}

void Level::cleanup()
{
	//Cleans up the level.
	levelMap.cleanup();

	for each(Monsters* mon in listOfMonsters)
	{
		if(!mon->getDead())
		{
			mon->cleanup();
			delete mon;
		}
	}

	for each(Items* it in listOfItems)
	{
		it->cleanup();
		delete it;
	}

	mora.cleanup();
}

void Level::softCleanup()
{
	//Cleans up the level but Mora.
	levelMap.cleanup();

	for each(Monsters* mon in listOfMonsters)
	{
		if(!mon->getDead())
		{
			mon->cleanup();
			delete mon;
		}
	}

	for each(Items* it in listOfItems)
	{
		it->cleanup();
		delete it;
	}
}

void Level::reset()
{
	//Resets the level for another level to be loaded into it.
	listOfMonsters.clear();
	listOfWalls.clear();
	listOfItems.clear();
	listOfNextLevels.clear();
	firstRun = true;
}

void Level::clearNextLevelName()
{
	nextLevelName = "";
}

void Level::update(double delta)
{
	//Update all the entities in the game.  Check for collisions.  Game logic goes here.
	mora.update(delta);

	for each(NextLevelTile nlt in listOfNextLevels)
	{
		if(checkCollision(nlt.nextLevelBox, mora.getCharacterBox()))
		{
			nextLevelName += nlt.nextLevelString;
			//Move Mora to the correct location after a new level is selected.
			getMoraEntryBox(nlt);
		}
	}

	for each(Items* it in listOfItems)
	{
		it->moraChanges(&mora);
		it->monsterChanges(listOfMonsters);
	}

	for each(Monsters* mon in listOfMonsters)
	{
		mon->update(delta, mora);
	}

	for each(Items* it in listOfItems)
	{
		it->update(delta, mora);
		if(it->isFinished())
		{
			firstRun = true;
		}
	}

	if(!listOfWalls.empty() || !listOfMonsters.empty())
	{
		for each(Collidable colli in listOfWalls)
		{
			if(checkCollision(mora.getFutureCharacterBox(), colli.getCollisionBox()))
			{
				mora.noStep();
			}
			else
			{
				mora.step();
			}
		}

		for each(Monsters* mon in listOfMonsters)
		{
			if(checkCollision(mora.getFutureCharacterBox(), mon->getMonsterBox()) && !mon->getDead())
			{
				if(checkMovingCollision(mora.getFutureCharacterBox(), mon->getMonsterBox()) && !mon->getDead())
				{
					mora.step();
				}
				else
				{
					mora.noStep();
				}
			}
			else
			{
				mora.step();
			}
		}
	}
	else
	{
		mora.step();
	}

	for each(Monsters* mon in listOfMonsters)
	{
		mora.fearViewer(mon->getMonsterBox(), mon->getFearDamage());
	}

	if(mora.getAttackActivated())
	{
		for each(Monsters* mon in listOfMonsters)
		{
			if(checkCollision(mora.getAttackBox(), mon->getMonsterBox()))
			{
				mon->applyDamage(mora.getDamage());
			}
		}
	}

	for each(Monsters* mon in listOfMonsters)
	{
		if(mon->getAttackActivated())
		{
			if(checkCollision(mon->getAttackbox(), mora.getCharacterBox()))
			{
				mora.applyDamage(mon->getDamage());
			}
		}
	}
}

void Level::render(SDL_Surface* dest)
{
	//Draw the entities and erase the past drawing if we need to by drawing the background
	//over it.
	if(firstRun)
	{
		levelMap.drawAllTiles(dest);
		firstRun = false;
	}

	std::vector<CollisionBox> listOfCollis;

	//These clean up the background but we are not using them yet.
	//CollisionBox healthBarCover;
	//healthBarCover.x = DEFAULT_ZERO;
	//healthBarCover.y = DEFAULT_ZERO;
	//healthBarCover.w = 150;
	//healthBarCover.h = 150;
	//listOfCollis.push_back(healthBarCover);
	//healthBarCover.y += healthBarCover.h;
	//listOfCollis.push_back(healthBarCover);
	//healthBarCover.y += healthBarCover.h;
	//listOfCollis.push_back(healthBarCover);

	CollisionBox moraBackgroundCover;
	moraBackgroundCover.x = (int)mora.getCharacterBox().x - 150;
	moraBackgroundCover.y = (int)mora.getCharacterBox().y - 150;
	moraBackgroundCover.w = (int)mora.getCharacterBox().w + 300;
	moraBackgroundCover.h = (int)mora.getCharacterBox().h + 300;
	listOfCollis.push_back(moraBackgroundCover);
	
	//These clean up the background but we are not using them yet.
	//moraBackgroundCover.x = (int)mora.getPreviousCharacterBox().x - 150;
	//moraBackgroundCover.y = (int)mora.getPreviousCharacterBox().y - 150;
	//moraBackgroundCover.w = (int)mora.getPreviousCharacterBox().w + 300;
	//moraBackgroundCover.h = (int)mora.getPreviousCharacterBox().h + 300;
	//listOfCollis.push_back(moraBackgroundCover);
	//
	//moraBackgroundCover.x = (int)mora.getTeleportSpotBox().x - 10;
	//moraBackgroundCover.y = (int)mora.getTeleportSpotBox().y - 10;
	//moraBackgroundCover.w = (int)mora.getTeleportSpotBox().w + 20;
	//moraBackgroundCover.h = (int)mora.getTeleportSpotBox().h + 20;
	//listOfCollis.push_back(moraBackgroundCover);
	//
	//moraBackgroundCover.x = (int)mora.getPreviousTeleportSpotBox().x - 10;
	//moraBackgroundCover.y = (int)mora.getPreviousTeleportSpotBox().y - 10;
	//moraBackgroundCover.w = (int)mora.getPreviousTeleportSpotBox().w + 20;
	//moraBackgroundCover.h = (int)mora.getPreviousTeleportSpotBox().h + 20;
	//listOfCollis.push_back(moraBackgroundCover);

	//moraBackgroundCover.x = (int)mora.getGhostAreaX() - 10;
	//moraBackgroundCover.y = (int)mora.getGhostAreaY() - 10;
	//moraBackgroundCover.w = (int)mora.getPreviousTeleportSpotBox().w + 60;
	//moraBackgroundCover.h = (int)mora.getPreviousTeleportSpotBox().h + 40;
	//listOfCollis.push_back(moraBackgroundCover);

	/*for each(Monsters* mon in listOfMonsters)
	{
		CollisionBox bbabyBackgroundCover;
		bbabyBackgroundCover.x = (int)mon->getMonsterBox().x - 200;
		bbabyBackgroundCover.y = (int)mon->getMonsterBox().y - 40;
		bbabyBackgroundCover.w = (int)mon->getMonsterBox().w + 400;
		bbabyBackgroundCover.h = (int)mon->getMonsterBox().h + 80;
		listOfCollis.push_back(bbabyBackgroundCover);
	}*/

	levelMap.renderNewTiles(dest, listOfCollis);

	for each(Items* it in listOfItems)
	{
		it->render(dest);
	}
	
	for each(Monsters* mon in listOfMonsters)
	{
		if(!mon->getDead())
		{
			mon->render(dest);
		}
	}
	
	mora.render(dest);

	apply_surface(mora.getCharacterBox().x - SCREEN_WIDTH + mora.getCharacterBox().w/2, mora.getCharacterBox().y - SCREEN_HEIGHT + mora.getCharacterBox().h/2, blackoutSpriteSheet, dest, NULL);
	for each(Items* it in listOfItems)
	{
		it->renderTop(dest);
	}
	if(!mora.getPause())
	{
		mora.drawMoraUI(dest);
	}
}

void Level::addMora(Mora mo)
{
	mora = mo;
}

void Level::addMonster(Monsters* mon)
{
	listOfMonsters.push_back(mon);
}

void Level::addCollidable(Collidable colli)
{
	listOfWalls.push_back(colli);
}

void Level::addTileToMap(Tile* t)
{
	levelMap.addTile(t);
}

void Level::addItems(Items* newItem)
{
	listOfItems.push_back(newItem);
}

void Level::addNextLevelTile(NextLevelTile nlt)
{
	listOfNextLevels.push_back(nlt);
}

void Level::setMoraPosition(double x, double y)
{
	mora.setPosition(x,y);
}

CollisionBox Level::getMoraEntryBox(NextLevelTile nlt)
{
		if(nlt.nextLevelBox.x == DEFAULT_ZERO)
		{
			CollisionBox moraNextPosition;
			moraNextPosition.x = SCREEN_WIDTH - TILE_SIZE - TILE_SIZE;
			moraNextPosition.y = nlt.nextLevelBox.y;
			moraNextPosition.w = nlt.nextLevelBox.w;
			moraNextPosition.h = nlt.nextLevelBox.h;
			mora.setPosition(moraNextPosition.x, moraNextPosition.y);
			return moraNextPosition;
		}
		else if(nlt.nextLevelBox.x == SCREEN_WIDTH - TILE_SIZE)
		{
			CollisionBox moraNextPosition;
			moraNextPosition.x = TILE_SIZE;
			moraNextPosition.y = nlt.nextLevelBox.y;
			moraNextPosition.w = nlt.nextLevelBox.w;
			moraNextPosition.h = nlt.nextLevelBox.h;
			mora.setPosition(moraNextPosition.x, moraNextPosition.y);
			return moraNextPosition;
		}
		else if(nlt.nextLevelBox.y == DEFAULT_ZERO)
		{
			CollisionBox moraNextPosition;
			moraNextPosition.x = nlt.nextLevelBox.x;
			moraNextPosition.y = SCREEN_HEIGHT - TILE_SIZE - TILE_SIZE;
			moraNextPosition.w = nlt.nextLevelBox.w;
			moraNextPosition.h = nlt.nextLevelBox.h;
			mora.setPosition(moraNextPosition.x, moraNextPosition.y);
			return moraNextPosition;
		}
		else if(nlt.nextLevelBox.y == SCREEN_HEIGHT - TILE_SIZE)
		{
			CollisionBox moraNextPosition;
			moraNextPosition.x = nlt.nextLevelBox.x;
			moraNextPosition.y = TILE_SIZE;
			moraNextPosition.w = nlt.nextLevelBox.w;
			moraNextPosition.h = nlt.nextLevelBox.h;mora.setPosition(moraNextPosition.x, moraNextPosition.y);
			return moraNextPosition;
		}
		else
		{
			CollisionBox moraNextPosition;
			moraNextPosition.x = nlt.nextLevelBox.x + TILE_SIZE;
			moraNextPosition.y = nlt.nextLevelBox.y;
			moraNextPosition.w = nlt.nextLevelBox.w;
			moraNextPosition.h = nlt.nextLevelBox.h;
			return moraNextPosition;
		}
}