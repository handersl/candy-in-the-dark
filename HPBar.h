#pragma once
#ifndef HPBAR_H
#define HPBAR_H

#include <SDL.h>
#include <SDL_image.h>
#include <string>

class HPBar
{
public:
	HPBar();
	//This creates an hp bar at position xPos and yPos.  It also
	//sets the time it shows.
	HPBar(double xPos, double yPos, int max, int min, int timeToShow);
	//Init. the health bar.
	bool init();
	//If we want a different looking healthbar we pass the strings to
	//the container and bar spritesheets.
	bool initOther(std::string hpContainer, std::string hpBar);
	//If we want an icon we can add it using this and passing in
	//the string of the icon sprite sheet.
	bool addIcon(std::string);
	void cleanup();
	//This update function takes in a position because
	//we need to know where to draw the hp bar if it is moving.
	void update(double delta, int xPos, int yPos, int valueNow);
	//Update also takes in a value so it can update the var accordingly.
	void update(double delta, int valueNow);
	void render(SDL_Surface* dest);
	//This shows the hp bar for the time to show.
	void showForTime()
	{ show = true; }
	//This hides the hp bar.
	void hide()
	{ show = false; alwaysShow = false;}
	//This makes the hp baralways show.
	void alwaysShowHP()
	{ alwaysShow = true; }
	//Return values.
	int getHPBarHeight()
	{ return HP_CONTAINER_HEIGHT; }
	int getHPBarWidth()
	{ return HP_CONTAINER_WIDTH; }

private:
	//These two are the sprites that make up the hp bar.
	SDL_Surface* hpContainerSpriteSheet;
	SDL_Surface* hpBarSpriteSheet;
	SDL_Surface* iconSpriteSheet;
	//The current frame
	int currentFrame;
	//The position of the hp bar.
	int xPosition;
	int yPosition;
	//The max and min of the bar.
	int maximum;
	int minimum;
	//This variable will store the previous value.  This will tell
	//the class if it is taking damage or being healed.
	int previousValue;
	//The show timer is how long the hp bar will show.
	int showTimer;
	//The xOffset to draw the bar at the tight place.
	int hpBarXOffset;
	//This bool will draw the sprite if true.
	bool show;
	bool alwaysShow;
	//The constants of the spritesheets.
	static const int HP_CONTAINER_WIDTH = 100;
	static const int HP_CONTAINER_HEIGHT = 25;
	static const int HP_BAR_WIDTH = 97;
	static const int HP_BAR_HEIGHT = 7;
	static const int HP_CONTAINER_ROW = 1;
	static const int HP_CONTAINER_COLUMN = 1;
	static const int HP_BAR_ROW  = 1;
	static const int HP_BAR_COLUMN  = 1;
	static const int HP_BAR_YOFFSET  = 15;
	static const int ICON_XOFFSET  = 1;
	static const int ICON_YOFFSET  = 1;
	static const int ICON_WIDTH  = 11;
	static const int ICON_HEIGHT  = 10;
	//The cips for the sprite sheets;
	SDL_Rect hpContainerClips[HP_CONTAINER_ROW * HP_CONTAINER_COLUMN];
	SDL_Rect hpBarClips[HP_BAR_ROW * HP_BAR_COLUMN];
	SDL_Rect iconClips[1];
};

#endif