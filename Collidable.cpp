#include "Collidable.h"

//Default will make the box basically useless.
Collidable::Collidable()
{
	collisionBox.x = 0;
	collisionBox.y = 0;
	collisionBox.w = 0;
	collisionBox.h = 0;
}

//Create a new collidable at (x,y) with width w and height h.
Collidable::Collidable(double x, double y, double w, double h)
{
	collisionBox.x = x;
	collisionBox.y = y;
	collisionBox.w = w;
	collisionBox.h = h;
}

void Collidable::resizeWidthBy(double widthToChange)
{
	collisionBox.w += widthToChange;
}

void Collidable::resizeHeightBy(double heightToChange)
{
	collisionBox.h += heightToChange;
}

void Collidable::setLocation(double newXPos, double newYPos)
{
	collisionBox.x = newXPos;
	collisionBox.y = newYPos;
}