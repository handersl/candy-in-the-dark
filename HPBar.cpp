#include "HPBar.h"
#include "Global.h"

HPBar::HPBar()
{
	//Create a bar at the top left od the screen and set it to not show.
	currentFrame = DEFAULT_ZERO;
	xPosition = DEFAULT_ZERO;
	yPosition = DEFAULT_ZERO;
	minimum = DEFAULT_ZERO;
	maximum = DEFAULT_ZERO;
	showTimer = DEFAULT_ZERO;
	show = false;
	alwaysShow = false;
	hpBarXOffset = HP_BAR_WIDTH;
	previousValue = DEFAULT_ZERO;
	//Cut the clips for the spritesheets.
	hpContainerClips[DEFAULT_ZERO].x = DEFAULT_ZERO;
	hpContainerClips[DEFAULT_ZERO].y = DEFAULT_ZERO;
	hpContainerClips[DEFAULT_ZERO].w = HP_CONTAINER_WIDTH;
	hpContainerClips[DEFAULT_ZERO].h = HP_CONTAINER_HEIGHT;
	hpBarClips[DEFAULT_ZERO].x = DEFAULT_ZERO;
	hpBarClips[DEFAULT_ZERO].y = DEFAULT_ZERO;
	hpBarClips[DEFAULT_ZERO].w = HP_BAR_WIDTH;
	hpBarClips[DEFAULT_ZERO].h = HP_BAR_HEIGHT;
	iconClips[DEFAULT_ZERO].x = DEFAULT_ZERO;
	iconClips[DEFAULT_ZERO].y = DEFAULT_ZERO;
	iconClips[DEFAULT_ZERO].w =ICON_WIDTH;
	iconClips[DEFAULT_ZERO].h = ICON_HEIGHT;
	iconSpriteSheet = NULL;
}

HPBar::HPBar(double xPos, double yPos, int max, int min, int timeToShow)
{
	//Create the hp bar with the specs given.
	currentFrame = DEFAULT_ZERO;
	xPosition = xPos;
	yPosition = yPos;
	minimum = min;
	maximum = max;
	showTimer = timeToShow;
	show = false;
	alwaysShow = false;
	hpBarXOffset = HP_BAR_WIDTH;
	previousValue = max;
	hpContainerClips[DEFAULT_ZERO].x = DEFAULT_ZERO;
	hpContainerClips[DEFAULT_ZERO].y = DEFAULT_ZERO;
	hpContainerClips[DEFAULT_ZERO].w = HP_CONTAINER_WIDTH;
	hpContainerClips[DEFAULT_ZERO].h = HP_CONTAINER_HEIGHT;
	hpBarClips[DEFAULT_ZERO].x = DEFAULT_ZERO;
	hpBarClips[DEFAULT_ZERO].y = DEFAULT_ZERO;
	hpBarClips[DEFAULT_ZERO].w = HP_BAR_WIDTH;
	hpBarClips[DEFAULT_ZERO].h = HP_BAR_HEIGHT;
	iconClips[DEFAULT_ZERO].x = DEFAULT_ZERO;
	iconClips[DEFAULT_ZERO].y = DEFAULT_ZERO;
	iconClips[DEFAULT_ZERO].w =ICON_WIDTH;
	iconClips[DEFAULT_ZERO].h = ICON_HEIGHT;
	iconSpriteSheet = NULL;
}

bool HPBar::init()
{
	//Load up the spritesheets.
	hpContainerSpriteSheet = load_image(HP_CONTAINER_FILENAME);
	hpBarSpriteSheet = load_image(HP_BAR_FILENAME);
	return true;
}

bool HPBar::initOther(std::string hpContainer, std::string hpBar)
{
	//Load up the spritesheets.
	hpContainerSpriteSheet = load_image(hpContainer);
	hpBarSpriteSheet = load_image(hpBar);
	return true;
}

bool HPBar::addIcon(std::string icon)
{
	//Load the icon.
	iconSpriteSheet = load_image(icon);
	return true;
}

void HPBar::cleanup()
{
	//Free up the spritesheets when we are done.
	SDL_FreeSurface(hpContainerSpriteSheet);
	SDL_FreeSurface(hpBarSpriteSheet);
	if(iconSpriteSheet != NULL)
	{
		SDL_FreeSurface(iconSpriteSheet);
	}
}

void HPBar::update(double delta, int xPos, int yPos, int valueNow)
{
	//We change the position of the bar.
	xPosition = xPos;
	yPosition = yPos;

	//If there is a change in the value.  We calculate the percent of change
	//and find out the right amount to change the offset.
	if(previousValue != valueNow)
	{
		if(previousValue < valueNow)
		{
			hpBarXOffset += (((double)(valueNow - previousValue)/maximum) * HP_BAR_WIDTH);
			if(hpBarXOffset >= HP_BAR_WIDTH)
			{
				hpBarXOffset = HP_BAR_WIDTH;
			}
		}
		else if(previousValue > valueNow)
		{
			hpBarXOffset -= (((double)(previousValue - valueNow)/maximum) * HP_BAR_WIDTH);
			if(hpBarXOffset <= DEFAULT_ZERO)
			{
				hpBarXOffset = DEFAULT_ZERO;
			}
		}
		if(valueNow >= maximum)
		{
			hpBarXOffset = HP_BAR_WIDTH;
		}
	}
	previousValue = valueNow;
}

void HPBar::update(double delta, int valueNow)
{
	//If there is a change in the value.  We calculate the percent of change
	//and find out the right amount to change the offset.
	if(previousValue != valueNow)
	{
		if(previousValue < valueNow)
		{
			hpBarXOffset += (((double)(valueNow - previousValue)/maximum) * HP_BAR_WIDTH);
			if(hpBarXOffset >= HP_BAR_WIDTH)
			{
				hpBarXOffset = HP_BAR_WIDTH;
			}
		}
		else if(previousValue > valueNow)
		{
			hpBarXOffset -= (((double)(previousValue - valueNow)/maximum) * HP_BAR_WIDTH);
			if(hpBarXOffset <= DEFAULT_ZERO)
			{
				hpBarXOffset = DEFAULT_ZERO;
			}
		}
		if(valueNow >= maximum)
		{
			hpBarXOffset = HP_BAR_WIDTH;
		}
	}
	previousValue = valueNow;
}

void HPBar::render(SDL_Surface* dest)
{
	//If it is time to show the bar, we will draw it.
	if(show || alwaysShow)
	{
		hpBarClips[currentFrame].w = hpBarXOffset;
		apply_surface(xPosition + (HP_CONTAINER_WIDTH - HP_BAR_WIDTH), yPosition + HP_BAR_YOFFSET, hpBarSpriteSheet, dest, &hpBarClips[currentFrame]);
		apply_surface(xPosition, yPosition, hpContainerSpriteSheet, dest, &hpContainerClips[currentFrame]);
		if(iconSpriteSheet != NULL)
		{
			apply_surface(xPosition + ICON_XOFFSET, yPosition + ICON_YOFFSET, iconSpriteSheet, dest, &iconClips[DEFAULT_ZERO]);
		}
	}
}