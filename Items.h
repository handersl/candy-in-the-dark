#pragma once
#ifndef ITEMS_H
#define ITEMS_H

#include <SDL.h>
#include <vector>
#include "Mora.h"
#include "Monsters.h"
#include "Collidable.h"

//Items and item like objects will use this class.
//Though items may inherit they may be too complicated
//and require more functions to work.

class Items
{
public:
	//Init. the item.  Loading assets setting variables etc.
	virtual bool init() = 0;
	//Clean up the memory in use.
	virtual void cleanup() = 0;
	//Update depending on if the item is activated or not.
	virtual void update(double delta, Mora mora) = 0;
	//Render the item on the ground.
	virtual void render(SDL_Surface* dest) = 0;
	//this renders the items that need to be overlayed at the top.
	virtual void renderTop(SDL_Surface* dest) = 0;
	//The changes we will do to Mora.
	virtual void moraChanges(Mora* mora) = 0;
	//The changes we will do to the monsters in the level.
	virtual void monsterChanges(std::vector<Monsters*> mons) = 0;
	//The collision box of the item.
	virtual CollisionBox getItemCollisionBox() = 0;
	//Is the item done.
	virtual bool isFinished() = 0;
};

#endif