#include "LevelEditorTiles.h"

#include "Global.h"

//This is the Level Editor Tile.  It is what is clicked on to select the tile in the level editor
//and it is also what is placed on the level in the level editor.
//The level editor tile was created to encapsulate every different tile needed for the game.
//Setting the type will set the tile.

std::string const LevelEditorTiles::DEBUG_LEVEL_TILE00 = "Art/Tile00.png";
std::string const LevelEditorTiles::DEBUG_LEVEL_TILE01 = "Art/Tile01.png";
std::string const LevelEditorTiles::DEBUG_LEVEL_TILE02 = "Art/Tile02.png";
std::string const LevelEditorTiles::DEBUG_LEVEL_TILE03 = "Art/Tile03.png";
std::string const LevelEditorTiles::DEBUG_LEVEL_TILE04 = "Art/Tile04.png";
std::string const LevelEditorTiles::DEBUG_LEVEL_TILE05 = "Art/Tile05.png";
std::string const LevelEditorTiles::DEBUG_LEVEL_TILE06 = "Art/Tile06.png";
std::string const LevelEditorTiles::DEBUG_LEVEL_TILE07 = "Art/Tile07.png";
std::string const LevelEditorTiles::DEBUG_LEVEL_TILE08 = "Art/Tile08.png";
std::string const LevelEditorTiles::DEBUG_LEVEL_TILE09 = "Art/Tile09.png";
std::string const LevelEditorTiles::DEBUG_LEVEL_TILE10 = "Art/Tile10.png";

std::string const LevelEditorTiles::TUT_BUNNY_FILENAME = "Art/TutDialogueIconSpriteSheet.png";

//Creates a deafult no-type tile.
LevelEditorTiles::LevelEditorTiles()
{
	icon = nullptr;
	imageSet = false;
	xPos = DEFAULT_ZERO;
	yPos = DEFAULT_ZERO;
	thisTileType = NATileType;
	groundTile = "";
	monsterTileType = NAMonsterType;
	itemTileType = NAItemType;
}

//Create the tile of specific type.
LevelEditorTiles::LevelEditorTiles(levelEditorTileType tileType)
{
	icon = nullptr;
	imageSet = false;
	xPos = DEFAULT_ZERO;
	yPos = DEFAULT_ZERO;
	thisTileType = tileType;
	groundTile = "";
	monsterTileType = NAMonsterType;
	itemTileType = NAItemType;
	if(tileType == mora)
	{
		icon = load_image(MORA_ICON_FILENAME);
		imageSet = true;
	}
}

//Set the tile to be a gorund type.  The string indicates which tile it is representing.
void LevelEditorTiles::setGroundTile(std::string groundT)
{ 
	groundTile = groundT;
	if(thisTileType == ground && groundTile != "")
	{
		if(groundTile == "00")
		{
			icon = load_image(DEBUG_LEVEL_TILE00);
			imageSet = true;
		}
		else if(groundTile == "01")
		{
			icon = load_image(DEBUG_LEVEL_TILE01);
			imageSet = true;
		}
		else if(groundTile == "02")
		{
			icon = load_image(DEBUG_LEVEL_TILE02);
			imageSet = true;
		}
		else if(groundTile == "03")
		{
			icon = load_image(DEBUG_LEVEL_TILE03);
			imageSet = true;
		}
		else if(groundTile == "04")
		{
			icon = load_image(DEBUG_LEVEL_TILE04);
			imageSet = true;
		}
		else if(groundTile == "05")
		{
			icon = load_image(DEBUG_LEVEL_TILE05);
			imageSet = true;
		}
		else if(groundTile == "06")
		{
			icon = load_image(DEBUG_LEVEL_TILE06);
			imageSet = true;
		}
		else if(groundTile == "07")
		{
			icon = load_image(DEBUG_LEVEL_TILE07);
			imageSet = true;
		}
		else if(groundTile == "08")
		{
			icon = load_image(DEBUG_LEVEL_TILE08);
			imageSet = true;
		}
		else if(groundTile == "09")
		{
			icon = load_image(DEBUG_LEVEL_TILE09);
			imageSet = true;
		}
		else if(groundTile == "10")
		{
			icon = load_image(DEBUG_LEVEL_TILE10);
			imageSet = true;
		}
	}
}

//Set this tile to a monster type.  Set it to represent a certian monster.
void LevelEditorTiles::setMonsterType(monsterType monType)
{
	monsterTileType = monType;
	if(thisTileType == monster)
	{
		if(monsterTileType == Bbaby)
		{
			icon = load_image(BBABY_ICON_FILENAME);
		}
		imageSet = true;
	}
}

//Set the tile to be a powerup type.
void LevelEditorTiles::setItemType(itemType ittype)
{
	itemTileType = ittype;
	if(itemTileType == powerUp)
	{
		icon = load_image(POWER_UP_FILENAME);
	}
	else if(itemTileType == dialogue)
	{
		icon = load_image(TUT_BUNNY_FILENAME);
	}
	imageSet = true;
}

//Set the tile to be a reader item type.
void LevelEditorTiles::setReaderTileType(readerTileType readT)
{
	readType = readT;
	if(thisTileType == item && itemTileType == reader)
	{
		if(readType == clipboardReaderType)
		{
			icon = load_image(CLIPBOARD_ITEM_THIS_IS_A_TEST_ICON_FILENAME);
		}
		imageSet = true;
	}
}

//If the tile is a next level tile then we will want to enter the string
//of the next level.
void LevelEditorTiles::setNextLevelString(std::string nlString)
{
	nextLevelString += nlString;
}

//Clean up the tile.
void LevelEditorTiles::cleanup()
{
	if(imageSet && icon != nullptr)
	{
		SDL_FreeSurface(icon);
		icon = nullptr;
	}
}

//Draw the tile.
void LevelEditorTiles::render(SDL_Surface* dest)
{
	apply_surface(xPos, yPos, icon, dest, NULL);
}

//Set the print string for a reader type tile.
void LevelEditorTiles::setReaderPrintString(std::string str)
{
	readerPrintString = str;
}

void LevelEditorTiles::setDialogueString(std::string dString)
{
	dialogueString = dString;
}

void LevelEditorTiles::setDialogueIconString(std::string icoString)
{
	iconString = icoString;
}

void LevelEditorTiles::setDialogueCharacterString(std::string charString)
{
	characterString = charString;
}