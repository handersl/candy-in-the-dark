#pragma once
#ifndef MENU_H
#define MENU_H

#include <SDL.h>
#include <string>

#include "MusicPlayer.h"

static const int MENU_START = 0;
static const int MENU_LEVEL_EDITOR = 1;
static const int MENU_QUIT = 2;

//This is the main menu for the game.
class Menu
{
public:
	//Create the menu.  Load all needed images.
	Menu();
	bool init();
	//Set the music player.
	void setMusicPlayer(MusicPlayer* musPlay)
	{ menuMusicPlayer = musPlay; }
	//Update according to the users keypresses.
	void update();
	void updateCurrentPointerStatus(Uint8* keys);
	//Draw everything.
	void render(SDL_Surface* dest);
	//CLean up the used memory.
	void cleanup();
	int getStatus();
	void pauseMenu()
	{ pause = true; }
	void unpauseMenu()
	{ pause = false; }

private:
	//The images for the menu's background and selector.
	SDL_Surface* menuBackgroundSpriteSheet;
	SDL_Surface* menuPointerSpriteSheet;
	//Position of the selector.
	int buttonX;
	int buttonY;
	//The current status of the pointer.
	int currentPointerStatus;
	//The selected status of the pointer.
	int pointerStatusToBeReturned;
	Uint8* keystates;
	
	//This makes sure that only one key press will register. 
	bool currentButtonState;
	bool prevButtonState;

	//We only need the main menu sometimes.
	//This bools pauses it until we need it.
	bool pause;

	//We set the state of the menu for one frame.  Afterwards
	//we reset it.  This bool keeps track of resetting.
	bool reset;
	
	MusicPlayer* menuMusicPlayer;

	static const int START_BUTTON_X = 492;
	static const int START_BUTTON_Y = 48;
	static const int LEVELEDITOR_BUTTON_X = 492;
	static const int LEVELEDITOR_BUTTON_Y = 294;
	static const int QUIT_BUTTON_X = 492;
	static const int QUIT_BUTTON_Y = 548;

	static const std::string MENU_BACKGROUND_SPRITESHEET;
	static const std::string MENU_POINTER_SPRITESHEET;
};

#endif