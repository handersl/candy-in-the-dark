#include "Mora.h"

#include <stdlib.h>
#include <time.h>

//This is the movement speed across the screen.
double const Mora::MOVE_SPEED = .25;
//This is the modifier that will be multiplied by the 
//move speed to determind run speed.
double const Mora::RUN_MODIFIER = 2;
//This will be used to find when to play the next frame.
//The MAX_FRAME_INDEX will be divided by this variable to quicken
//the speed at which the attack animation will play.
double const Mora::ATTACK_MODIFIER = 2.5;
//The rates at which to
double const Mora::FOOT_STEP_WALK_RATE = .5;
double const Mora::FOOT_STEP_RUN_RATE = .25;
double const Mora::MORA_WALK_FRAMERATE_TIMER = .20;
double const Mora::MORA_RUN_FRAMERATE_TIMER = .05;
double const Mora::MORA_ABILITY_FRAMERATE_TIMER = .05;
//This is the amount of time that we show the fear indicator;
double const Mora::SHOW_DAMAGE_INDICATOR_TIMER = .025;

std::string const Mora::MORA_FILENAME = "Art/MoraSpriteSheet.png";
std::string const Mora::MORA_POWER_UP_FILENAME = "Art/MoraPowerUpSpriteSheet.png";
std::string const Mora::GHOST_CHANGER_ARROW_FILENAME = "Art/GhostChangerArrowSpriteSheet.png";
std::string const Mora::GHOST_CHANGER_ATTACK_FILENAME = "Art/GhostChangerAttackAngelSpriteSheet.png";
std::string const Mora::GHOST_CHANGER_TELEPORT_FILENAME = "Art/GhostChangerTeleportAngelSpriteSheet.png";
std::string const Mora::ATTACK_ANGEL_FILENAME = "Art/AttackAngelSpriteSheet.png";

std::string const Mora::MORA_LEFT_EYESIGHT_ICON_FILENAME = "Art/EyesightLeftSpriteSheet.png";
std::string const Mora::MORA_RIGHT_EYESIGHT_ICON_FILENAME = "Art/EyesightRightSpriteSheet.png";
std::string const Mora::MORA_UP_EYESIGHT_ICON_FILENAME = "Art/EyesightUpSpriteSheet.png";
std::string const Mora::MORA_DOWN_EYESIGHT_ICON_FILENAME = "Art/EyesightDownSpriteSheet.png";
std::string const Mora::ATTACK_ANGEL_ICON_FILENAME = "Art/AttackAngelIconSpriteSheet.png";
std::string const Mora::TELEPORT_ANGEL_ICON_FILENAME = "Art/TeleportAngelIconSpriteSheet.png";
std::string const Mora::TELEPORT_CURSOR_FILENAME = "Art/TeleportCursorSpriteSheet.png";
std::string const Mora::TELEPORT_ANGEL_FILENAME = "Art/TeleportAngelSpriteSheet.png";
std::string const Mora::TELEPORT_ANGEL_BLANKET_FILENAME = "Art/TeleportAngelBlanketSpriteSheet.png";

Mora::Mora()
{
}

//This function will create Mora at a specify position.
Mora::Mora(double xPos, double yPos)
{
	//xOff and yOff will be used to help create the array of rectangels
	//that makes up the different frames on the animation.  These rectangels
	//cut up the sprite sheet into rectangels representing the frames of the
	//animation.
	playFootstepTimer = DEFAULT_ZERO;
	playHeartbeatTimer = DEFAULT_ZERO;
	mpBarTimer = DEFAULT_ZERO;
	timeSinceLastFear = MIN_TIME_IN_BETWEEN_FEARS;
	updateFrameTimer = DEFAULT_ZERO;
	showDamageIndicatorTimer = DEFAULT_ZERO;

	int xOff = xPos;
	int yOff = yPos;
	//This for loop loops through and creates the cells.
	for(int i = 0; i < MORA_CELLS; i++)
	{
		if(i != DEFAULT_ZERO && i%MORA_COLUMN == DEFAULT_ZERO)
		{
			xOff = DEFAULT_ZERO;
			yOff += MORA_HEIGHT;
		}
		moraClip[i].x = xOff;
		moraClip[i].y = yOff;
		moraClip[i].w = MORA_WIDTH;
		moraClip[i].h = MORA_HEIGHT;
		xOff += MORA_WIDTH;
	}
	//Reset the offsets to be used again.
	xOff = DEFAULT_ZERO;
	yOff = DEFAULT_ZERO;
	for(int i = 0; i < ATTACK_ANGEL_CELLS; i++)
	{
		if(i != DEFAULT_ZERO && i%ATTACK_ANGEL_COLUMN == DEFAULT_ZERO)
		{
			yOff += ATTACK_ANGEL_HEIGHT;
			xOff = DEFAULT_ZERO;
		}
		attackAngelClip[i].x = xOff;
		attackAngelClip[i].y = yOff;
		attackAngelClip[i].w = ATTACK_ANGEL_WIDTH;
		attackAngelClip[i].h = ATTACK_ANGEL_HEIGHT;
		xOff += ATTACK_ANGEL_WIDTH;
	}
	//Another reset.
	xOff = DEFAULT_ZERO;
	yOff = DEFAULT_ZERO;
	for(int i = 0; i < TELEPORT_ANGEL_CELLS; i++)
	{
		if(i != DEFAULT_ZERO && i%TELEPORT_ANGEL_COLUMN == DEFAULT_ZERO)
		{
			yOff += TELEPORT_ANGEL_HEIGHT;
			xOff = DEFAULT_ZERO;
		}
		teleportAngelClip[i].x = xOff;
		teleportAngelClip[i].y = yOff;
		teleportAngelClip[i].w = TELEPORT_ANGEL_WIDTH;
		teleportAngelClip[i].h = TELEPORT_ANGEL_HEIGHT;
		xOff += TELEPORT_ANGEL_WIDTH;
	}
	//The x and y position.
	xPosition = xPos;
	yPosition = yPos;
	//This sets up the health
	healthNow = MORA_HEALTH;
	manaOneNow = MORA_HEALTH;
	manaTwoNow = MORA_HEALTH;
	sanityNow = MORA_HEALTH;
	healthBar = HPBar(xPosition, yPosition, MORA_HEALTH, DEFAULT_ZERO, DEFAULT_ZERO);
	healthBar.alwaysShowHP();
	fearBar = HPBar(xPosition + healthBar.getHPBarWidth(), yPosition, MORA_HEALTH, DEFAULT_ZERO, DEFAULT_ZERO);
	fearBar.alwaysShowHP();
	manaBarOne = HPBar(xPosition, yPosition + manaBarOne.getHPBarHeight(), MORA_HEALTH, DEFAULT_ZERO, DEFAULT_ZERO);
	manaBarOne.alwaysShowHP();
	manaBarTwo = HPBar(xPosition, yPosition + manaBarTwo.getHPBarHeight() * 2, MORA_HEALTH, DEFAULT_ZERO, DEFAULT_ZERO);
	manaBarTwo.alwaysShowHP();
	//Set running and attacking to false.
	running = false;
	attacking = false;
	attackActivated = false;
	teleporting = false;
	calmButtonPressed = false;
	poweredUp = false;
	ghostChangerActivated = false;
	damaged = false;
	startled = false;
	dead = false;

	//Set Mora's status to standing idle.
	currentStatus = idle;
	//Set Mora's fear status to none.
	fearStatus = fearNA;
	//The current frame is zero.
	currentFrame = DEFAULT_ZERO;
	//Set the moves for Mora.
	currentAttack1 = attackAngel;
	currentAttack2 = teleportAngel;
	//We will use an invalid value to tell when the attackFrame has 
	//not yet been set.  This will notify the code to change the
	//value to the correct one depending on which side Mora is facing.
	attackFrame = NA_VALUE;
	//The frame index runs up to FRAME_INDEX_MAX and will reset back 
	//to DEFAULT_ZERO.  When it reaches FRAME_INDEX_MAX it will play 
	//the next animation.
	frameIndex = DEFAULT_ZERO;
	//These set the collision box for where Mora is.  The sprite is 
	//also drawn at this box's position.
	characterBox.x = xPosition;
	characterBox.y = yPosition;
	characterBox.w = MORA_WIDTH;
	characterBox.h = MORA_HEIGHT;
	//This is a temporary collision box to check if Mora can go
	//where she wants to go.  If she can, her characterBox will take
	//on the same value as the futureCharacterBox; if not her futureCharacterBox
	//will take on the value of the characterBox.
	futureCharacterBox.x = xPosition;
	futureCharacterBox.y = yPosition;
	futureCharacterBox.w = MORA_WIDTH;
	futureCharacterBox.h = MORA_HEIGHT;
	previousCharacterBox.w = MORA_WIDTH;
	previousCharacterBox.h = MORA_HEIGHT;
	//This sets the Mora's attack collision box.
	attackBox.x = DEFAULT_ZERO;
	attackBox.y = DEFAULT_ZERO;
	attackBox.w = ATTACK_ANGEL_VERTICAL_ATTCK_WIDTH;
	attackBox.h = ATTACK_ANGEL_VERTICAL_ATTCK_HEIGHT;
	//Sets the teleporter spot in front of Mora.
	teleportSpotBox.x = xPosition;
	teleportSpotBox.y = yPosition + MORA_HEIGHT + MORA_HEIGHT;
	teleportSpotBox.w = MORA_WIDTH;
	teleportSpotBox.h = MORA_HEIGHT;
	//Sets the eyesight spot in front of Mora.
	eyesightBox.x = xPosition;
	eyesightBox.y = yPosition;
	eyesightBox.w = MORA_WIDTH;
	eyesightBox.h = MORA_HEIGHT + MORA_HEIGHT + MORA_HEIGHT + MORA_HEIGHT;
	//This does the setup for the ghost changer.
	ghostArrowBox.x = NA_VALUE;
	ghostArrowBox.y = GHOST_CHANGER_ARROW_Y;
	ghostArrowBox.w = TILE_SIZE;
	ghostArrowBox.h = TILE_SIZE;
	attackAngelGhostBox.x = GHOST_CHANGER_ICON_START_X;
	attackAngelGhostBox.y = GHOST_CHANGER_ICON_Y;
	attackAngelGhostBox.w = TILE_SIZE;
	attackAngelGhostBox.h = TILE_SIZE;
	teleportAngelGhostBox.x = GHOST_CHANGER_ICON_START_X + GHOST_X_CHANGE;
	teleportAngelGhostBox.y = GHOST_CHANGER_ICON_Y;
	teleportAngelGhostBox.w = TILE_SIZE;
	teleportAngelGhostBox.h = TILE_SIZE;
}

void Mora::setPosition(double x, double y)
{
	futureCharacterBox.x = x;
	futureCharacterBox.y = y;
	step();
}

bool Mora::init()
{	
	std::srand(time(NULL));
	//This loads Mora's sprite sheet and all her attacks.
	//We are not paused.
	pause = false;
	//Load the sprite sheets.
	moraSpriteSheet = load_image(MORA_FILENAME);
	moraPoweredUpSpriteSheet = load_image(MORA_POWER_UP_FILENAME);
	attackAngelSpriteSheet = load_image(ATTACK_ANGEL_FILENAME);
	teleportAngelSpriteSheet = load_image(TELEPORT_ANGEL_FILENAME);
	teleportAngelBlanketSpriteSheet = load_image(TELEPORT_ANGEL_BLANKET_FILENAME);
	teleportCursorSpirteSheet = load_image(TELEPORT_CURSOR_FILENAME);
	eyesightSpriteSheet = load_image(MORA_DOWN_EYESIGHT_ICON_FILENAME);
	ghostChangerArrow = load_image(GHOST_CHANGER_ARROW_FILENAME);
	ghostChangerAttackIconSpriteSheet = load_image(GHOST_CHANGER_ATTACK_FILENAME);
	ghostChangerTeleportIconSpriteSheet = load_image(GHOST_CHANGER_TELEPORT_FILENAME);
	damageIndicatorSpriteSheet = load_image(DAMAGE_INDICATOR_FILENAME);
	//Initalize the healthbars and add icons where needed.
	healthBar.init();
	fearBar.initOther(FEAR_CONTAINER_FILENAME, FEAR_BAR_FILENAME);
	fearBar.addIcon(FEAR_ICON_FILENAME);
	manaBarOne.initOther(MP_CONTAINER_FILENAME, MP_BAR_FILENAME);
	manaBarOne.addIcon(ATTACK_ANGEL_ICON_FILENAME);
	manaBarTwo.initOther(MP_CONTAINER_FILENAME, MP_BAR_FILENAME);
	manaBarTwo.addIcon(TELEPORT_ANGEL_ICON_FILENAME);
	
	//Initalize the music player and load its files.
	moraMusic->playCandyInTheDark();
	return true;
}

void Mora::cleanup()
{
	//This cleans the memory of the sprite sheets.
	SDL_FreeSurface(moraSpriteSheet);
	SDL_FreeSurface(moraPoweredUpSpriteSheet);
	SDL_FreeSurface(attackAngelSpriteSheet);
	SDL_FreeSurface(teleportAngelSpriteSheet);
	SDL_FreeSurface(teleportAngelBlanketSpriteSheet);
	SDL_FreeSurface(teleportCursorSpirteSheet);
	healthBar.cleanup();
	fearBar.cleanup();
	manaBarOne.cleanup();
	manaBarTwo.cleanup();
}

void Mora::processEvent(Uint8* keys, double delta)
{
	//This function tests for key presses.  It sets the status.
	
	//This key clams down Mora which makes her easier to move. 
	if(keys[SDLK_s])
	{
		if(!calmButtonPressed)
		{
			sanityNow += AMOUNT_OF_CALMNESS;
			if(sanityNow > MORA_HEALTH)
			{
				sanityNow = MORA_HEALTH;
			}
			calmButtonPressed = true;
		}

	}
	else
	{
		calmButtonPressed = false;
	}

	//The less sanity Mora has the less control over her the player has.
	//The player regains sanity by pressing the "s" key.
	int randNumber = rand() % RANDOM_FEAR_NUMBER + DEFAULT_ZERO;
	if(sanityNow == MORA_HEALTH || randNumber <= sanityNow)
	{
		if(keys[SDLK_z])
		{
			running = true;
		}
		else
		{
			running = false;
		}

		//To start attacking we make sure our last attack
		//is fully one and that the x button is pressed.
		if(keys[SDLK_x] )
		{
			if(currentAttack1 == attackAngel && attackActivated == false && !attacking && !teleporting && manaOneNow >= ATTACK_ANGEL_COST)
			{
				manaOneNow -= ATTACK_ANGEL_COST;
				attacking = true;
			}
			else if(currentAttack1 == teleportAngel && !teleporting && !attacking && manaOneNow >= TELEPORT_ANGEL_COST)
			{
				manaOneNow -= TELEPORT_ANGEL_COST;
				teleporting = true;
			}
		}
		//The secondary attack button which functions a lot like the last one.
		if(keys[SDLK_c] )
		{
			if(currentAttack2 == attackAngel && attackActivated == false && !attacking && !teleporting && manaTwoNow >= ATTACK_ANGEL_COST)
			{
				manaTwoNow -= ATTACK_ANGEL_COST;
				attacking = true;
			}
			else if(currentAttack2 == teleportAngel && !teleporting && !attacking && manaTwoNow >= TELEPORT_ANGEL_COST)
			{
				manaTwoNow -= TELEPORT_ANGEL_COST;
				teleporting = true;
			}
		}

		if(!attacking && !teleporting)
		{
			if(keys[SDLK_UP])
			{
				currentStatus = up;
				if(running)
				{
					futureCharacterBox.y -= MOVE_SPEED * RUN_MODIFIER * delta;
				}
				else
				{
					futureCharacterBox.y -= MOVE_SPEED * delta;
				}
			}
			if(keys[SDLK_DOWN])
			{
				currentStatus = down;
				if(running)
				{
					futureCharacterBox.y += MOVE_SPEED * RUN_MODIFIER * delta;
				}
				else
				{
					futureCharacterBox.y += MOVE_SPEED * delta;
				}
			}
			if(keys[SDLK_LEFT])
			{
				currentStatus = left;
				if(running)
				{
					futureCharacterBox.x -= MOVE_SPEED * RUN_MODIFIER * delta;
				}
				else
				{
					futureCharacterBox.x -= MOVE_SPEED * delta;
				}
			}
			if(keys[SDLK_RIGHT])
			{
				currentStatus = right;
				if(running)
				{
					futureCharacterBox.x += MOVE_SPEED * RUN_MODIFIER * delta;
				}
				else
				{
					futureCharacterBox.x += MOVE_SPEED * delta;
				}
			}
			if(!keys[SDLK_UP] && !keys[SDLK_DOWN] && !keys[SDLK_LEFT] && !keys[SDLK_RIGHT] && !attacking && !teleporting)
			{
				currentStatus = idle;
			}
		}
	}
	//If the player loses control of Mora we will randomly choose not to move.
	else
	{
		//We still want to change direction so that Mora will always facing the one that Mora
		//is facing.
		if(!attacking && !teleporting)
		{
			if(keys[SDLK_UP])
			{
				currentStatus = up;
			}
			if(keys[SDLK_DOWN])
			{
				currentStatus = down;
			}
			if(keys[SDLK_LEFT])
			{
				currentStatus = left;
			}
			if(keys[SDLK_RIGHT])
			{
				currentStatus = right;
			}
			if(!keys[SDLK_UP] && !keys[SDLK_DOWN] && !keys[SDLK_LEFT] && !keys[SDLK_RIGHT] && !attacking && !teleporting)
			{
				currentStatus = idle;
			}
		}
		if(!attacking && !teleporting)
		{
			noStep();
		}
	}
}

//This is how Mora will change her two moves.  As of now
//there are only two attacks.
void Mora::ghostChanger(Uint8* keystates)
{
	//Not yet functional or nessesary.

	////Checks to see if weapons need to change.
	//if(keystates[SDLK_f] && ghostButtons)
	//{
	//	if(!ghostChangerActivated)
	//	{
	//		ghostChangerActivated = true;
	//		canChangeGhost = false;
	//		ghostButtons = false;
	//	}
	//	ghostAttack = &currentAttack1;
	//}
	//if(keystates[SDLK_g] && ghostButtons)
	//{
	//	if(!ghostChangerActivated)
	//	{
	//		ghostChangerActivated = true;
	//		canChangeGhost = false;
	//		ghostButtons = false;
	//	}
	//	ghostAttack = &currentAttack2;
	//}
	//if(!keystates[SDLK_f] && !keystates[SDLK_g])
	//{
	//	ghostButtons = true;
	//}

	//if(ghostChangerActivated)
	//{
	//	if(*ghostAttack == attackAngel && ghostArrowBox.x == NA_VALUE)
	//	{
	//		ghostArrowBox.x = GHOST_CHANGER_ICON_START_X;
	//	}
	//	else if(*ghostAttack == teleportAngel && ghostArrowBox.x == NA_VALUE)
	//	{
	//		ghostArrowBox.x = GHOST_CHANGER_ICON_START_X + GHOST_X_CHANGE;
	//	}

	//	if(keystates[SDLK_LEFT] && ghostArrowBox.x > GHOST_CHANGER_ICON_START_X && ghostButtons)
	//	{
	//		ghostArrowBox.x -= GHOST_X_CHANGE;
	//		ghostButtons = false;
	//	}
	//	if(keystates[SDLK_RIGHT] && ghostArrowBox.x < GHOST_CHANGER_ICON_END_X && ghostButtons)
	//	{
	//		ghostArrowBox.x += GHOST_X_CHANGE;
	//		ghostButtons = false;
	//	}
	//	
	//	if(checkCollision(ghostArrowBox, attackAngelGhostBox) && (keystates[SDLK_f] || keystates[SDLK_g]) && canChangeGhost && ghostButtons)
	//	{
	//		*ghostAttack = attackAngel;
	//		ghostChangerActivated = false;
	//		ghostArrowBox.x = NA_VALUE;
	//		ghostButtons = false;
	//	}
	//	else if(checkCollision(ghostArrowBox, teleportAngelGhostBox) && (keystates[SDLK_f] || keystates[SDLK_g]) && canChangeGhost && ghostButtons)
	//	{
	//		*ghostAttack = teleportAngel;
	//		ghostChangerActivated = false;
	//		ghostArrowBox.x = NA_VALUE;
	//		ghostButtons = false;
	//	}
	//	canChangeGhost = true;
	//}
}

void Mora::update(double delta)
{	
	//Pump and update the Mora based on the keys.
	SDL_PumpEvents();
	keystates = SDL_GetKeyState(NULL);
	ghostChanger(keystates);
	if(!pause && !dead)
	{
		//Update Mora's status.
		if(currentStatus != idle)
		{
			previousStatus = currentStatus;
		}
		
		if(showDamageIndicatorTimer > SHOW_DAMAGE_INDICATOR_TIMER)
		{
			damaged = false;
			startled = false;
			showDamageIndicatorTimer = DEFAULT_ZERO;
		}

		//Update the timers,
		playFootstepTimer += delta * .001;
		playHeartbeatTimer += delta * .001;
		mpBarTimer += delta * .001;
		timeSinceLastFear += delta * .001;
		updateFrameTimer += delta * .001;
		if(damaged || startled)
		{
			showDamageIndicatorTimer += delta * .001;
		}
		
		//Update the previous location of the collision boxes.
		previousTeleportSpotBox = teleportSpotBox;
		previousCharacterBox.x = xPosition;
		previousCharacterBox.y = yPosition;
		
		//These three functions get and process the inputs.
		SDL_PumpEvents();
		keystates = SDL_GetKeyState(NULL);
		processEvent(keystates, delta);
		
		//We check if we are attacking/finish attacking.  If
		//we are done attacking we will not activate the
		//attack box.
		if(!attacking)
		{
			attackActivated = false;
		}
		
		//We increment the frameIndex everyframe and check if we are able to play
		//the next frame in the animation.
		//Based on our status we will play the different animations
		updateFrames(updateFrameTimer);
		findFacingSide();
		resetFacingDirection();
		
		//Play the heartbeat based on the sanity of Mora.
		if(playHeartbeatTimer > sanityNow/MAX_HEARTBEAT_MULTIPLIER)
		{
			moraMusic->playHearbeat();
			playHeartbeatTimer = DEFAULT_ZERO;
		}

		//Update the UI bars to display the right amount.
		if(mpBarTimer >= 1)
		{
			if(manaOneNow < MORA_HEALTH)
			{
				manaOneNow += 10;
				if(manaOneNow > MORA_HEALTH)
				{
					manaOneNow = MORA_HEALTH;
				}
			}
			if(manaTwoNow < MORA_HEALTH)
			{
				manaTwoNow += 10;
				if(manaTwoNow > MORA_HEALTH)
				{
					manaTwoNow = MORA_HEALTH;
				}
			}
			mpBarTimer -= 1;
		}
		healthBar.update(delta, healthNow);
		fearBar.update(delta, sanityNow);
		manaBarOne.update(delta, manaOneNow);
		manaBarTwo.update(delta, manaTwoNow);

		if(healthNow < DEFAULT_ZERO)
		{
			dead = true;
		}
	}
}

void Mora::resetFacingDirection()
{
	//We make sure that the currentFrame stays in the range of the right animation.
	if(currentFrame >= WALK_UP_START_FRAME && currentFrame <= WALK_UP_END_FRAME)
	{
		//If we are outside the range we want to put ourselves back into it.
		//We also update the UI to reflect the new direction.
		teleportSpotBox.x = xPosition;
		teleportSpotBox.y = yPosition - MORA_HEIGHT - MORA_HEIGHT;
		eyesightBox.x = xPosition;
		eyesightBox.y = yPosition - MORA_HEIGHT - MORA_HEIGHT - MORA_HEIGHT;
		eyesightBox.w = MORA_WIDTH;
		eyesightBox.h = MORA_HEIGHT + MORA_HEIGHT + MORA_HEIGHT + MORA_HEIGHT;
		eyesightSpriteSheet = load_image(MORA_UP_EYESIGHT_ICON_FILENAME);
	}
	if(currentFrame >= WALK_DOWN_START_FRAME && currentFrame <= WALK_DOWN_END_FRAME)
	{
		teleportSpotBox.x = xPosition;
		teleportSpotBox.y = yPosition + MORA_HEIGHT + MORA_HEIGHT;
		eyesightBox.x = xPosition;
		eyesightBox.y = yPosition;
		eyesightBox.w = MORA_WIDTH;
		eyesightBox.h = MORA_HEIGHT + MORA_HEIGHT + MORA_HEIGHT + MORA_HEIGHT;
		eyesightSpriteSheet = load_image(MORA_DOWN_EYESIGHT_ICON_FILENAME);
	}
	if(currentFrame >= WALK_LEFT_START_FRAME && currentFrame <= WALK_LEFT_END_FRAME)
	{
		teleportSpotBox.x = xPosition - MORA_WIDTH - MORA_WIDTH;
		teleportSpotBox.y = yPosition;
		eyesightBox.x = xPosition - MORA_WIDTH - MORA_WIDTH - MORA_WIDTH;
		eyesightBox.y = yPosition;
		eyesightBox.w = MORA_WIDTH + MORA_WIDTH + MORA_WIDTH + MORA_WIDTH;
		eyesightBox.h = MORA_HEIGHT;
		eyesightSpriteSheet = load_image(MORA_LEFT_EYESIGHT_ICON_FILENAME);
	}
	if(currentFrame >= WALK_RIGHT_START_FRAME && currentFrame <= WALK_RIGHT_END_FRAME)
	{
		teleportSpotBox.x = xPosition + MORA_WIDTH + MORA_WIDTH;
		teleportSpotBox.y = yPosition;
		eyesightBox.x = xPosition;
		eyesightBox.y = yPosition;
		eyesightBox.w = MORA_WIDTH + MORA_WIDTH + MORA_WIDTH + MORA_WIDTH;
		eyesightBox.h = MORA_HEIGHT;
		eyesightSpriteSheet = load_image(MORA_RIGHT_EYESIGHT_ICON_FILENAME);
	}
}

void Mora::findFacingSide()
{
	if(currentStatus != idle)
	{
		if(currentFrame >= WALK_UP_START_FRAME && currentFrame <= WALK_UP_END_FRAME)
		{	
			currentStatus = up;
		}
		else if(currentFrame >= WALK_DOWN_START_FRAME && currentFrame <= WALK_DOWN_END_FRAME)
		{
			currentStatus = down;
		}
		else if(currentFrame >= WALK_LEFT_START_FRAME && currentFrame <= WALK_LEFT_END_FRAME)
		{
			currentStatus = left;
		}
		else if(currentFrame >= WALK_RIGHT_START_FRAME && currentFrame <= WALK_RIGHT_END_FRAME)
		{
			currentStatus = right;
		}
	}
}

void Mora::updateFrames(double framerateTimer)
{
	//Increment the frame index.  We only advance to the next frame when the frame index reaches
	//its MAX_FRAME_INDEX or we reach another condition.
	if(framerateTimer > MORA_ABILITY_FRAMERATE_TIMER && attacking && !teleporting)
	{
		if(attackFrame == NA_VALUE)
		{
			//If we are attacking we make sure to stay within the correct frames.
			if(currentFrame >= WALK_LEFT_START_FRAME && currentFrame <= WALK_LEFT_END_FRAME)
			{
				attackFrame = ATTACK_ANGEL_LEFT_START_FRAME;
			}
			else if(currentFrame >= WALK_RIGHT_START_FRAME && currentFrame <= WALK_RIGHT_END_FRAME)
			{
				attackFrame = ATTACK_ANGEL_RIGHT_START_FRAME;
			}
			else if(currentFrame >= WALK_UP_START_FRAME && currentFrame <= WALK_UP_END_FRAME)
			{
				attackFrame = ATTACK_ANGEL_UP_START_FRAME;
			}
			else if(currentFrame >= WALK_DOWN_START_FRAME && currentFrame <= WALK_DOWN_END_FRAME)
			{
				attackFrame = ATTACK_ANGEL_DOWN_START_FRAME;
			}
		}
		else if(attackFrame == ATTACK_ANGEL_LEFT_END_FRAME || attackFrame == ATTACK_ANGEL_RIGHT_END_FRAME || attackFrame == ATTACK_ANGEL_DOWN_END_FRAME || attackFrame == ATTACK_ANGEL_UP_END_FRAME || attackFrame > ATTACK_ANGEL_ROW * ATTACK_ANGEL_COLUMN)
		{
			//If we are in the last frame of the animation we will
			//place the attack box in the right area.
			if(attackFrame == ATTACK_ANGEL_UP_END_FRAME)
			{
				attackBox.x = characterBox.x;
				attackBox.y = characterBox.y - MORA_HEIGHT;
				attackBox.w = ATTACK_ANGEL_VERTICAL_ATTCK_WIDTH;
				attackBox.h = ATTACK_ANGEL_VERTICAL_ATTCK_HEIGHT;
			}
			else if(attackFrame == ATTACK_ANGEL_DOWN_END_FRAME)
			{
				attackBox.x = characterBox.x;
				attackBox.y = characterBox.y;
				attackBox.w = ATTACK_ANGEL_VERTICAL_ATTCK_WIDTH;
				attackBox.h = ATTACK_ANGEL_VERTICAL_ATTCK_HEIGHT;
			}
			else if(attackFrame == ATTACK_ANGEL_LEFT_END_FRAME)
			{
				attackBox.x = characterBox.x - MORA_HEIGHT;
				attackBox.y = characterBox.y;
				attackBox.w = ATTACK_ANGEL_HORIZONTAL_ATTCK_WIDTH;
				attackBox.h = ATTACK_ANGEL_HORIZONTAL_ATTCK_HEIGHT;
			}
			else if(attackFrame == ATTACK_ANGEL_RIGHT_END_FRAME)
			{
				attackBox.x = characterBox.x;
				attackBox.y = characterBox.y;
				attackBox.w = ATTACK_ANGEL_HORIZONTAL_ATTCK_WIDTH;
				attackBox.h = ATTACK_ANGEL_HORIZONTAL_ATTCK_HEIGHT;
			}
			//We are done attacking so we reset the way it attacks.
			attackFrame = NA_VALUE;
			attacking = false;
			//We activate the attack box for use.  In the next update/frame
			//it will deactivate because we only want it for one frame.
			attackActivated = true;
		}
		else
		{
			attackFrame++;
		}
		updateFrameTimer = DEFAULT_ZERO;
	}
	else if(framerateTimer > MORA_ABILITY_FRAMERATE_TIMER && teleporting && !attacking)
	{
		if(attackFrame == NA_VALUE || attackFrame < TELEPORT_ANGEL_START_FRAME || attackFrame > TELEPORT_ANGEL_END_FRAME)
		{
			attackFrame = TELEPORT_ANGEL_START_FRAME;
		}
		else
		{
			//If we are in the last frame of the animation we will
			//place the attack box in the right area.
			if(attackFrame == TELEPORT_ANGEL_END_FRAME)
			{
				//Teleport Mora if possible.
				//Want to check for collisions with walls first.
				futureCharacterBox.x = teleportSpotBox.x;
				futureCharacterBox.y = teleportSpotBox.y;
				//We are done attacking so we reset the way it attacks.
				attackFrame = NA_VALUE;
				teleporting = false;
				moraMusic->playTeleport();
			}
			else
			{
				attackFrame++;
			}
		}
		updateFrameTimer = DEFAULT_ZERO;
	}
	else if(framerateTimer > MORA_WALK_FRAMERATE_TIMER)
	{
		if(currentStatus == up)
		{
			currentFrame++;
			if(currentFrame < WALK_DOWN_END_FRAME || currentFrame > WALK_UP_END_FRAME)
			{	
				currentFrame = WALK_UP_START_FRAME;
			}
			updateFrameTimer = DEFAULT_ZERO;
		}
		if(currentStatus == down)
		{
			currentFrame++;
			if(currentFrame < WALK_RIGHT_END_FRAME || currentFrame > WALK_DOWN_END_FRAME)
			{
				currentFrame = WALK_DOWN_START_FRAME;
			}
			updateFrameTimer = DEFAULT_ZERO;
		}
		if(currentStatus == left)
		{
			currentFrame++;
			if(currentFrame < WALK_LEFT_START_FRAME || currentFrame > WALK_LEFT_END_FRAME)
			{
				currentFrame = WALK_LEFT_START_FRAME;
			}
			updateFrameTimer = DEFAULT_ZERO;
		}
		if(currentStatus == right)
		{
			currentFrame++;
			if(currentFrame < WALK_LEFT_END_FRAME || currentFrame > WALK_RIGHT_END_FRAME)
			{
				currentFrame = WALK_RIGHT_START_FRAME;
			}
			updateFrameTimer = DEFAULT_ZERO;
		}
	}
}

void Mora::render(SDL_Surface* dest)
{
	//This draws all the sprites.
	if(teleporting && attackFrame != NA_VALUE)
	{
		apply_surface((int)xPosition, (int)yPosition - MORA_HEIGHT, teleportAngelSpriteSheet, dest, &teleportAngelClip[attackFrame]);
	}
	if(attacking && attackFrame != NA_VALUE && currentFrame >= WALK_LEFT_START_FRAME && currentFrame <= WALK_LEFT_END_FRAME)
	{
		apply_surface((int)xPosition - MORA_WIDTH, (int)yPosition - MORA_HEIGHT, attackAngelSpriteSheet, dest, &attackAngelClip[attackFrame]);
	}
	else if(attacking && attackFrame != NA_VALUE && currentFrame >= WALK_RIGHT_START_FRAME && currentFrame <= WALK_RIGHT_END_FRAME)
	{
		apply_surface((int)xPosition, (int)yPosition - MORA_HEIGHT, attackAngelSpriteSheet, dest, &attackAngelClip[attackFrame]);
	}
	else if(attacking && attackFrame != NA_VALUE && currentFrame >= WALK_DOWN_START_FRAME && currentFrame <= WALK_DOWN_END_FRAME)
	{
		apply_surface((int)xPosition - MORA_WIDTH/2, (int)yPosition - MORA_HEIGHT, attackAngelSpriteSheet, dest, &attackAngelClip[attackFrame]);
	}
	if(!poweredUp)
	{
		apply_surface((int)xPosition, (int)yPosition, moraSpriteSheet, dest, &moraClip[currentFrame]);
	}
	else
	{
		apply_surface((int)xPosition, (int)yPosition, moraPoweredUpSpriteSheet, dest, &moraClip[currentFrame]);
	}
	if(attacking && attackFrame != NA_VALUE && currentFrame >= WALK_UP_START_FRAME && currentFrame <= WALK_UP_END_FRAME)
	{
		apply_surface((int)xPosition - MORA_WIDTH/2, (int)yPosition - MORA_HEIGHT, attackAngelSpriteSheet, dest, &attackAngelClip[attackFrame]);
	}
	if(teleporting && attackFrame != NA_VALUE)
	{
		apply_surface((int)xPosition, (int)yPosition - MORA_HEIGHT, teleportAngelBlanketSpriteSheet, dest, &teleportAngelClip[attackFrame]);
	}
}

void Mora::drawMoraUI(SDL_Surface* dest)
{
	//Displays the UI only.
	apply_surface((int)teleportSpotBox.x, (int)teleportSpotBox.y, teleportCursorSpirteSheet, dest, NULL);

	if(currentStatus == up || (currentStatus == idle && previousStatus == up))
	{
		apply_surface((int)eyesightBox.x, (int)eyesightBox.y, eyesightSpriteSheet, dest, NULL);
	}
	else if(currentStatus == down || (currentStatus == idle && previousStatus == down))
	{
		apply_surface((int)eyesightBox.x, (int)(eyesightBox.y + eyesightBox.h - MORA_HEIGHT), eyesightSpriteSheet, dest, NULL);
	}
	else if(currentStatus == left || (currentStatus == idle && previousStatus == left))
	{
		apply_surface((int)eyesightBox.x, (int)eyesightBox.y, eyesightSpriteSheet, dest, NULL);
	}
	else if(currentStatus == right || (currentStatus == idle && previousStatus == right))
	{
		apply_surface((int)eyesightBox.x + eyesightBox.w - MORA_WIDTH, (int)eyesightBox.y, eyesightSpriteSheet, dest, NULL);
	}

	if(ghostChangerActivated)
	{
		apply_surface((int)ghostArrowBox.x, (int)ghostArrowBox.y, ghostChangerArrow, dest, NULL);
		apply_surface((int)attackAngelGhostBox.x, (int)attackAngelGhostBox.y, ghostChangerAttackIconSpriteSheet, dest, NULL);
		apply_surface((int)teleportAngelGhostBox.x, (int)teleportAngelGhostBox.y, ghostChangerTeleportIconSpriteSheet, dest, NULL);
	}

	if(damaged)
	{
		apply_surface(DEFAULT_ZERO, DEFAULT_ZERO, damageIndicatorSpriteSheet, dest, NULL);
	}

	if(startled)
	{
		apply_surface(DEFAULT_ZERO, DEFAULT_ZERO, damageIndicatorSpriteSheet, dest, NULL);
	}
	healthBar.render(dest);
	manaBarOne.render(dest);
	manaBarTwo.render(dest);
	fearBar.render(dest);
}

void Mora::step()
{
	if(!(futureCharacterBox.x == xPosition && futureCharacterBox.y == yPosition))
	{
		//We do a check to make sure Mora can not walk off screen.
		if((futureCharacterBox.x <= SCREEN_WIDTH - MORA_WIDTH && futureCharacterBox.x >= 0) &&
			(futureCharacterBox.y <= SCREEN_HEIGHT - MORA_HEIGHT && futureCharacterBox.y >= 0))
		{
			if(playFootstepTimer > FOOT_STEP_WALK_RATE)
			{
				moraMusic->playFootSteps();
				playFootstepTimer = DEFAULT_ZERO;
			}
			else if(playFootstepTimer > FOOT_STEP_RUN_RATE && running)
			{
				moraMusic->playFootSteps();
				playFootstepTimer = DEFAULT_ZERO;
			}

			//If her next position is walkable then we let her characterBox
			//take on the value of her futureCharacterBox.
			xPosition = futureCharacterBox.x;
			yPosition = futureCharacterBox.y;
			characterBox.x = xPosition;
			characterBox.y = yPosition;
		}
		else
		{
			noStep();
		}
	}
}

void Mora::noStep()
{
	//If Mora can not go to where she wants to go then her 
	//futureCharacterBox will take on the value of her characterBox; this
	//means that her position would not have changed.
	futureCharacterBox.x = previousCharacterBox.x;
	futureCharacterBox.y = previousCharacterBox.y;
	step();
}

void Mora::applyDamage(int dmg)
{
	healthNow -= dmg;
	damaged = true;
}

int Mora::getDamage()
{
	if(currentAttack1 == attackAngel)
	{
		moraMusic->playStab();
		return ATTACK_ANGEL_DAMAGE;
	}
	return DEFAULT_ZERO;
}

void Mora::fearViewer(CollisionBox colli, int fearDamage)
{
	//If Mora's eveysight catches a monster she will lose sanity.
	if(checkCollision(eyesightBox, colli) && timeSinceLastFear >= MIN_TIME_IN_BETWEEN_FEARS)
	{
		sanityNow -= fearDamage;
		timeSinceLastFear = DEFAULT_ZERO;
		startled = true;
		moraMusic->playPanic();
	}
}
//Add in the acutal power up.
void Mora::powerUp()
{
	poweredUp = true;
}

void Mora::powerDown()
{
	poweredUp = false;
}