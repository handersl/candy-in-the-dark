#include "LevelLoader.h"

#include <iostream>
#include <fstream>
#include "Tile.h"
#include "Mora.h"
#include "EnemyBBaby.h"
#include "Readable.h"
#include "Global.h"
#include "PowerUp.h"
#include "Dialogue.h"

void LevelLoader::loadLevel(std::string levelString, Level* lev, MusicPlayer* musPlay)
{
	levelText += levelString;
	std::ifstream levelFile (levelText);
	if (levelFile.is_open())
	{
		while (levelFile.good())
		{
			//Read in the tiles and create and fill the map with them.
			int yPos = DEFAULT_ZERO;
			std::string mapString;
			std::getline(levelFile, mapString);
			while(!mapString.empty())
			{
				int parserHelperInt = DEFAULT_ZERO;
				for(int i = DEFAULT_ZERO; i < TILES_IN_WIDTH; i++)
				{
					//Grab the tiles and add it to the map accordingly.
					int tileNumber;
					std::string subString;
					subString = mapString.substr(parserHelperInt, mapString.find(" ", parserHelperInt));
					parserHelperInt = mapString.find(" ", parserHelperInt);
					parserHelperInt++;
					tileNumber = std::atoi(subString.c_str());
					int tileSpriteX;
					int tileSpriteY;
					if(tileNumber != DEFAULT_ZERO)
					{
						tileSpriteX = ((int)((tileNumber%TILES_IN_WIDTH_SPRITE))) * TILE_SIZE;
						tileSpriteY = (int)(tileNumber/TILES_IN_WIDTH_SPRITE) * TILE_SIZE;
					}
					else
					{
						tileSpriteX = DEFAULT_ZERO;
						tileSpriteY = DEFAULT_ZERO;
					}
					if(tileNumber < UNWALKABLE_TILE_BORDER)
					{
						Tile* nextTile = new Tile(i * TILE_SIZE, yPos, tileSpriteX, tileSpriteY, unwalkable);
						lev->addTileToMap(nextTile);
					}
					else
					{
						Tile* nextTile = new Tile(i * TILE_SIZE, yPos, tileSpriteX, tileSpriteY, walkable);
						lev->addTileToMap(nextTile);
					}
				}
				yPos += TILE_SIZE;
				std::getline(levelFile, mapString);
			}
			
			//Create the next level tiles and add it to the level.
			bool noNextLevel = true;
			std::string nextLevelsString;
			std::getline(levelFile, nextLevelsString);
			while(!nextLevelsString.empty())
			{
				int xPos;
				int yPos;
				int parserHelperInt = 0;
				std::string subString;
				
				//XPosition.
				subString = nextLevelsString.substr(parserHelperInt, nextLevelsString.find(" ", parserHelperInt));
				parserHelperInt = nextLevelsString.find(" ", parserHelperInt);
				xPos = std::atoi(subString.c_str());

				//YPosition.
				parserHelperInt++;
				subString = nextLevelsString.substr(parserHelperInt, nextLevelsString.find(" ", parserHelperInt));
				parserHelperInt = nextLevelsString.find(" ", parserHelperInt);
				yPos = std::atoi(subString.c_str());

				//Grab the string that leads to the next level text.
				std::getline(levelFile, nextLevelsString);
				NextLevelTile newNextLevel;
				newNextLevel.nextLevelString = nextLevelsString;
				newNextLevel.nextLevelBox.x = xPos;
				newNextLevel.nextLevelBox.y = yPos;
				newNextLevel.nextLevelBox.w = TILE_SIZE;
				newNextLevel.nextLevelBox.h = TILE_SIZE;
				lev->addNextLevelTile(newNextLevel);

				noNextLevel = false;

				std::getline(levelFile, nextLevelsString);
			}

			if(noNextLevel)
			{
				std::getline(levelFile, nextLevelsString);
			}

			//Add in Mora at the specified location.
			std::string moraString = "";
			std::getline(levelFile, moraString);
			while(!moraString.empty())
			{
				int xPos;
				int yPos;
				int parserHelperInt = 0;
				std::string subString;
				
				//XPosition.
				subString = moraString.substr(parserHelperInt, moraString.find(" ", parserHelperInt));
				parserHelperInt = moraString.find(" ", parserHelperInt);
				xPos = std::atoi(subString.c_str());

				//YPosition.
				parserHelperInt++;
				subString = moraString.substr(parserHelperInt, moraString.find(" ", parserHelperInt));
				parserHelperInt = moraString.find(" ", parserHelperInt);
				yPos = std::atoi(subString.c_str());

				if(lev->getMora().getCharacterBox().x == DEFAULT_ZERO && lev->getMora().getCharacterBox().y == DEFAULT_ZERO)
				{
					lev->setMoraPosition(xPos, yPos);
				}
				std::getline(levelFile, moraString);
			}

			//Add in the monsters as specified.
			bool noMonsters = true;
			std::string monstersString;
			std::getline(levelFile, monstersString);
			while(!monstersString.empty())
			{
				//Bbaby monster.
				if(std::atoi(monstersString.c_str()) == BBABY_MONSTER_NUMBER)
				{
					int xPos;
					int yPos;
					int parserHelperInt = 0;
					std::string subString;

					//XPosition.
					std::getline(levelFile, monstersString);
					subString = monstersString.substr(parserHelperInt, monstersString.find(" ", parserHelperInt));
					parserHelperInt = monstersString.find(" ", parserHelperInt);
					xPos = std::atoi(subString.c_str());
					
					//YPosition.
					parserHelperInt++;
					subString = monstersString.substr(parserHelperInt, monstersString.find(" ", parserHelperInt));
					yPos = std::atoi(subString.c_str());
					
					EnemyBBaby* bbaby = new EnemyBBaby(xPos, yPos);
					bbaby->setMusicPlayer(musPlay);
					bbaby->init();

					lev->addMonster(bbaby);
				}

				noMonsters = false;

				std::getline(levelFile, monstersString);
			}

			if(noMonsters)
			{
				std::getline(levelFile, monstersString);
			}

			//Add in the items.
			bool noItems = true;
			std::string itemsString;
			std::getline(levelFile, itemsString);
			while(!itemsString.empty())
			{
				//Reader items.
				if(std::atoi(itemsString.c_str()) == READER_ITEM_NUMBER)
				{
					std::getline(levelFile, itemsString);

					if(std::atoi(itemsString.c_str()) == READER_CLIPBOARD_ITEM_NUMBER)
					{
						int xPos;
						int yPos;
						int parserHelperInt = 0;
						std::string subString;

						//XPosition.
						std::getline(levelFile, itemsString);
						subString = itemsString.substr(parserHelperInt, itemsString.find(" ", parserHelperInt));
						parserHelperInt = itemsString.find(" ", parserHelperInt);
						xPos = std::atoi(subString.c_str());
					
						//YPosition.
						parserHelperInt++;
						subString = itemsString.substr(parserHelperInt, itemsString.find(" ", parserHelperInt));
						parserHelperInt = itemsString.find(" ", parserHelperInt);
						yPos = std::atoi(subString.c_str());

						//Grab the line with the clipboard print.
						std::getline(levelFile, itemsString);

						Readable* read = new Readable();
						read->load(xPos, yPos, clipboard, itemsString);
						lev->addItems(read);
					}
				}
				//Power up items.
				else if(std::atoi(itemsString.c_str()) == POWER_UP_ITEM_NUMBER)
				{
						int xPos;
						int yPos;
						int parserHelperInt = 0;
						std::string subString;

						//XPosition of the item.
						std::getline(levelFile, itemsString);
						subString = itemsString.substr(parserHelperInt, itemsString.find(" ", parserHelperInt));
						parserHelperInt = itemsString.find(" ", parserHelperInt);
						xPos = std::atoi(subString.c_str());
						
						//YPosition of the item.
						parserHelperInt++;
						subString = itemsString.substr(parserHelperInt, itemsString.find(" ", parserHelperInt));
						parserHelperInt = itemsString.find(" ", parserHelperInt);
						yPos = std::atoi(subString.c_str());

						PowerUp* pu = new PowerUp();
						pu->init(xPos, yPos);
						lev->addItems(pu);
				}
				else if(std::atoi(itemsString.c_str()) == DIALOGUE_ITEM_NUMBER)
				{
						int xPos;
						int yPos;
						int parserHelperInt = 0;
						std::string subString;
						std::string dialogueString;
						std::string characterIconString;
						std::string characterString= "";

						//XPosition.
						std::getline(levelFile, itemsString);
						subString = itemsString.substr(parserHelperInt, itemsString.find(" ", parserHelperInt));
						parserHelperInt = itemsString.find(" ", parserHelperInt);
						xPos = std::atoi(subString.c_str());
						
						//YPosition.
						parserHelperInt++;
						subString = itemsString.substr(parserHelperInt, itemsString.find(" ", parserHelperInt));
						parserHelperInt = itemsString.find(" ", parserHelperInt);
						yPos = std::atoi(subString.c_str());

						//Grab the dialogue string.
						std::getline(levelFile, itemsString);
						dialogueString = itemsString;

						//Grab the iconString.
						std::getline(levelFile, itemsString);
						characterIconString = itemsString;

						//Grab the characterString.  Should be "" if there is not one.
						std::getline(levelFile, itemsString);
						if(itemsString == "NONE")
						{
							characterString = "";
						}
						else
						{
							characterString = itemsString;
						}

						Dialogue* newD = new Dialogue(xPos, yPos, dialogueString, characterIconString, characterString); 
						lev->addItems(newD);
				}
				std::getline(levelFile, itemsString);
				noItems = false;
			}

			if(noItems)
			{
				std::getline(levelFile, itemsString);
			}
		}
		levelFile.close();
		levelText = "";
	}
}