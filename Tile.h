#pragma once
#ifndef TILE_H
#define TILE_H

enum tileType
{
	walkable, unwalkable
};

class Tile
{
public:
	//This creates a tile that will be placed at (xPos, yPos) and
	//comes from the sprite sheet position (sXPos, sYPos) with a
	//tileType of tType.  (All tiles have the dimension of one tile).
	Tile(int xPos, int yPos, int sXPos, int sYPos, tileType tType)
		: xPosition(xPos), yPosition(yPos), spriteXPosition(sXPos), spriteYPosition(sYPos), type(tType) {}
	int getXPosition()
	{ return xPosition; }
	int getYPosition()
	{ return yPosition; }
	int getSpriteXPosition()
	{ return spriteXPosition; }
	int getSpriteYPosition()
	{ return spriteYPosition; }
	tileType getTileType()
	{ return type; }
private:
	int xPosition;
	int yPosition;
	int spriteXPosition;
	int spriteYPosition;
	tileType type;
};

#endif