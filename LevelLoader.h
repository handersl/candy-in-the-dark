#pragma once
#ifndef LEVELLOADER_H
#define LEVELLOADER_H

#include <string>
#include "Level.h"
#include "MusicPlayer.h"

//Parses the txt file and fills the level as it is told to.

class LevelLoader
{
public:
	void loadLevel(std::string levelString, Level* lev, MusicPlayer* musPlay);
private:
	std::string levelText;
	static int const TILES_IN_WIDTH_SPRITE = 4;
	static int const TILES_IN_HEIGHT_SPRITE = 3;
	static int const UNWALKABLE_TILE_BORDER = 9;
};

#endif