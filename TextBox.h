#pragma once
#ifndef TEXTBOX_H
#define TEXTBOX_H

#include "Global.h"
#include <SDL_ttf.h>

class TextBox
{
public:
	//Create a text box at the xP and yP.
	bool init(int xP, int yP);
	//Update the text box.  This takes in inputs from
	//the keyboard.
	void update(double delta);
	//Draw the text box and text.
	void render(SDL_Surface* dest);
	//Clean up the textbox.
	void cleanup();
	//Enable the textbox for use.
	void enable();
	//Disable the textbox so that we
	//can not use it.
	void disable();
	//Clear the previous input.
	void cleanText();
	//Set the instruction string;
	void setInstructionString(std::string instruction);
	//Clean the instruction String.
	void cleanInstructionString();
	//Get the current inputted string.
	std::string getText();
	bool isEnabled()
	{ return enabled; }

private:
	int xPosition;
	int yPosition;
	SDL_Event keyEvent;
	TTF_Font* font;
	TTF_Font* instructionFont;
	SDL_Color textColor;
	SDL_Surface* inputBox;
	SDL_Surface* textToRender;
	SDL_Surface* instructionToRender;
	Uint8* keystates;
	std::string text;
	std::string instructionString;
	bool enabled;

	static int const FONT_SIZE = 50;
	static int const INSTRUCTION_FONT_SIZE = 24;
	static int const TEXT_X_OFFSET = 25;
	static int const TEXT_Y_OFFSET = 85;
	static int const INSTRUCTION_X_OFFSET = 25;
	static int const INSTRUCTION_Y_OFFSET = 15;

	static std::string const TEXT_BOX_FILENAME;
	static std::string const ASMAN_FONT_FILENAME;
	static std::string const INTRIQUE_FONT_FILENAME;
};

#endif