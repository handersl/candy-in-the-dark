#pragma once
#ifndef COLLIDABLE_H
#define COLLIDABLE_H

#include <SDL.h>

//This is basically a SDL_Rect with doubles for coordinates and dimensions.
typedef struct
{
	double x, y;
	double w, h;
} CollisionBox;

//The collidable object holds the collision box.  I have not used it but
//it may be useful for giving properties to the tiles later on.
class Collidable
{
public:
	//Default constructor that makes a 0 x 0 collision box at (0,0).
	Collidable();
	//Makes a w x h collision box at (x,y). 
	Collidable(double x, double y, double w, double h);
	//Returns the collision box.
	CollisionBox getCollisionBox()
	{return collisionBox;}
	//Resize the dimensions.
	void resizeWidthBy(double widthToChange);
	void resizeHeightBy(double heightToChange);
	//Set a new location for the box.
	void setLocation(double newXPos, double newYPos);
private:
	CollisionBox collisionBox;
};

#endif