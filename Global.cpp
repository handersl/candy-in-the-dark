#include "Global.h"

#include <cmath>

int SCREEN_WIDTH = 999;
int SCREEN_HEIGHT = 740;
int LEVEL_EDITOR_SCREEN_WIDTH = 1280;
int LEVEL_EDITOR_SCREEN_HEIGHT = 768;
int TILES_IN_WIDTH = 27;
int TILES_IN_HEIGHT= 20;
int SCREEN_BPP = 32;
int TILE_SIZE = 37;
int MAX_FRAME_INDEX = 70;
int WLAKABLE_PIXEL_AREA = 1;
int BBABY_MONSTER_NUMBER = 0;
int READER_ITEM_NUMBER = 0;
int POWER_UP_ITEM_NUMBER = 1;
int READERS_Y_LEVEL = 0;
int POWER_UPS_Y_LEVEL = 1;
int READER_CLIPBOARD_ITEM_NUMBER = 0;
int DIALOGUE_ITEM_NUMBER = 2;
int DOOR_TILE_NUMBER = 10;
int NA_VALUE = 9999;
int NNA_VALUE = -9999;
int DEFAULT_ZERO = 0;

std::string LEVEL_MASTER_TEXT = "Levels/Level Master.txt";
std::string HP_CONTAINER_FILENAME = "Art/HPContainerSpriteSheet.png";
std::string HP_BAR_FILENAME = "Art/HPBarSpriteSheet.png";
std::string MP_CONTAINER_FILENAME = "Art/MPContainerSpriteSheet.png";
std::string MP_BAR_FILENAME = "Art/MPBarSpriteSheet.png";
std::string FEAR_CONTAINER_FILENAME = "Art/FearContainerSpriteSheet.png";
std::string FEAR_BAR_FILENAME = "Art/FearBarSpriteSheet.png";
std::string FEAR_ICON_FILENAME = "Art/FearIconSpriteSheet.png";
std::string BLACK_OUT_BOX_FILENAME = "Art/BlackOutSpriteSheet.png";
std::string DAMAGE_INDICATOR_FILENAME = "Art/DamageIndicatorSpriteSheet.png";
std::string POWER_UP_FILENAME = "Art/PowerUpItemSpriteSheet.png";
std::string GAME_OVER_FILENAME = "Art/CandyInTheDarkDeathSpriteSheet.png";
std::string CLIPBOARD_TEST_PRINT = "Art/TestPrintSpriteSheet.png";
std::string DEBUG_LEVEL_TILESHEET = "Art/BasicWoodTileSet.png";
std::string DEBUG_LEVEL = "Levels/DebugLevel.txt";
std::string MORA_ICON_FILENAME = "Art/MoraIconSpriteSheet.png";
std::string BBABY_ICON_FILENAME = "Art/BbabyIconSpriteSheet.png";
std::string CLIPBOARD_ITEM_THIS_IS_A_TEST_ICON_FILENAME = "Art/ClipboardItemTestIconSpriteSheet.png";

//Taken from Lazy Foo's tutorial.
SDL_Surface *load_image( std::string filename ) 
{
	//The image that's loaded
    SDL_Surface* loadedImage = NULL;
    
    //The optimized image that will be used
    SDL_Surface* optimizedImage = NULL;
    
    //Load the image using SDL_image
    loadedImage = IMG_Load( filename.c_str() );
    
    //If the image loaded
    if( loadedImage != NULL )
    {
        //Create an optimized image
        optimizedImage = SDL_DisplayFormatAlpha( loadedImage );
        
        //Free the old image
        SDL_FreeSurface( loadedImage );
    }
    
    //Return the optimized image
    return optimizedImage;
}

//Taken from Lazy Foo's tutorial.
void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip = NULL )
{
    //Holds offsets
    SDL_Rect offset;
    
    //Get offsets
    offset.x = x;
    offset.y = y;
    
    //Blit
    SDL_BlitSurface( source, clip, destination, &offset );
}

//Taken from Lazy Foo's tutorial(Slightly varied).
bool checkCollision(CollisionBox A, CollisionBox B)
{
    //The sides of the rectangles
    double leftA, leftB;
    double rightA, rightB;
    double topA, topB;
    double bottomA, bottomB;

    //Calculate the sides of rect A
    leftA = A.x;
    rightA = A.x + A.w;
    topA = A.y;
    bottomA = A.y + A.h;
        
    //Calculate the sides of rect B
    leftB = B.x;
    rightB = B.x + B.w;
    topB = B.y;
    bottomB = B.y + B.h;
	//If any of the sides from A are outside of B
    if( bottomA <= topB )
    {
        return false;
    }
    
    if( topA >= bottomB )
    {
        return false;
    }
    
    if( rightA <= leftB )
    {
        return false;
    }
    
    if( leftA >= rightB )
    {
        return false;
    }
    
    //If none of the sides from A are outside B
    return true;
}

//A varied method from Lazy Foo's tutorials.
bool checkMovingCollision(CollisionBox A, CollisionBox B)
{
    //The sides of the rectangles
    double leftA, leftB;
    double rightA, rightB;
    double topA, topB;
    double bottomA, bottomB;

    //Calculate the sides of rect A
    leftA = A.x;
    rightA = A.x + A.w;
    topA = A.y;
    bottomA = A.y + A.h;
        
    //Calculate the sides of rect B
    leftB = B.x;
    rightB = B.x + B.w;
    topB = B.y;
    bottomB = B.y + B.h;
	//If there we are one pixel inside the monster we can still slide
	//across it.  This called along with the other collidable check will allow objects
	//to move aorund each other but not walk through each other.
    if( abs(bottomA - topB) < WLAKABLE_PIXEL_AREA )
    {
        return true;
    }
    
    if( abs(topA - bottomB) < WLAKABLE_PIXEL_AREA )
    {
        return true;
    }
    
    if( abs(rightA - leftB) < WLAKABLE_PIXEL_AREA )
    {
        return true;
    }
    
    if( abs(leftA - rightB) < WLAKABLE_PIXEL_AREA )
    {
        return true;
    }
    
    //If none of the sides from A are outside B
    return false;
}

double distance(CollisionBox A, CollisionBox B)
{
	return sqrt(pow((A.x - B.x),2) + pow((A.y - B.y),2));
}

//Taken from Lazy Foo's tutorial(Slightly varied).
Side checkSide(CollisionBox A, CollisionBox B)
{
	    //The sides of the rectangles
    double leftA, leftB;
    double rightA, rightB;
    double topA, topB;
    double bottomA, bottomB;

    //Calculate the sides of rect A
    leftA = A.x;
    rightA = A.x + A.w;
    topA = A.y;
    bottomA = A.y + A.h;
        
    //Calculate the sides of rect B
    leftB = B.x;
    rightB = B.x + B.w;
    topB = B.y;
    bottomB = B.y + B.h;
	//If any of the sides from A are outside of B
	if( bottomA >= topB  && bottomA < bottomB)
    {
        return DOWN;
    }
    
    if( topA <= bottomB && topA > topB)
    {
        return UP;
    }
    
	if( rightA >= leftB && rightA < rightB)
    {
        return RIGHT;
    }
    
    if( leftA <= rightB && leftA >leftB)
    {
        return LEFT;
    }
    
    //If none of the sides from A are colliding with B
    return NA;
}