#include "PathFinding.h"
#include <iterator>
#include "Global.h"

//This is the collision function that will check if
//a tile in walkable or not.
bool checkIfWalkable(CollisionBox A, CollisionBox B)
{
    //The sides of the rectangles
    double leftA, leftB;
    double rightA, rightB;
    double topA, topB;
    double bottomA, bottomB;

    //Calculate the sides of rect A
    leftA = A.x;
    rightA = A.x + A.w;
    topA = A.y;
    bottomA = A.y + A.h;
        
    //Calculate the sides of rect B
    leftB = B.x;
    rightB = B.x + B.w;
    topB = B.y;
    bottomB = B.y + B.h;
	//If any of the sides from A are outside of B
    if( bottomA <= topB )
    {
        return false;
    }
    
    if( topA >= bottomB )
    {
        return false;
    }
    
    if( rightA <= leftB )
    {
        return false;
    }
    
    if( leftA >= rightB )
    {
        return false;
    }
    
    //If none of the sides from A are outside B
    return true;
}

PathFinding::PathFinding()
{

}

//For more information about this pathfinding see http://www.raywenderlich.com/4946/introduction-to-a-pathfinding for the concept.
//Also see http://xnatd.blogspot.com/2011/06/pathfinding-tutorial-part-1.html for the pseudocode.
void PathFinding::createMap(int widthLevel, int heightLevel, double xSize, double ySize, std::vector<Collidable> listOfCollidables)
{	
	map.resize(widthLevel/xSize);
	for(int i = 0; i < map.size(); i++)
	{
		map[i].resize(heightLevel/ySize);
	}

	for(int x = 0; x < widthLevel/xSize; x++)
	{
		for(int y = 0; y < heightLevel/ySize; y++)
		{
			Node* node = new Node();
			node->setLocation(x * xSize, y * ySize);
			node->setWalkable(true);

			Collidable temp(node->getLocation().x, node->getLocation().y, xSize, ySize);
			for each(Collidable c in listOfCollidables)
			{
				if(checkIfWalkable(temp.getCollisionBox(), c.getCollisionBox()))
				{
					node->setWalkable(false);
				}
			}
			for(int i = 0; i < 8; i++)
			{
				Node* n = new Node();
				n->setFScore(0);
				n->setGScore(0);
				n->setHScore(0);
				n->setLocation(0, 0);
				n->setParentNode(NULL);
				n->setWalkable(false);
				node->setNeighbour(i, n);
			}

			map[x][y] = node;
		}
	}

	for(int x = 0; x < widthLevel/xSize ; x++)
	{
		for(int y = 0; y < heightLevel/ySize ; y++)
		{
			Node* node = map[x][y];

			std::vector<SDL_Rect> neighbour;
				
			SDL_Rect topLeft;
			topLeft.x = ((x - 1) * xSize);
			topLeft.y = ((y - 1) * ySize);
			topLeft.h = 0;
			topLeft.w = 0;
			neighbour.push_back(topLeft);

			SDL_Rect topMid;
			topMid.x = x * xSize;
			topMid.y = ((y - 1) * ySize);
			topMid.h = 0;
			topMid.w = 0;
			neighbour.push_back(topMid);

			SDL_Rect topRight;
			topRight.x = ((x + 1) * xSize);
			topRight.y = ((y - 1) * ySize);
			topRight.h = 0;
			topRight.w = 0;
			neighbour.push_back(topRight);

			SDL_Rect midLeft;
			midLeft.x = ((x - 1) * xSize);
			midLeft.y = y * ySize;
			midLeft.h = 0;
			midLeft.w = 0;
			neighbour.push_back(midLeft);

			SDL_Rect midRight;
			midRight.x = ((x + 1) * xSize);
			midRight.y = y * ySize;
			midRight.h = 0;
			midRight.w = 0;
			neighbour.push_back(midRight);

			SDL_Rect botRight;
			botRight.x = ((x - 1) * xSize);
			botRight.y = ((y + 1) * ySize);
			botRight.h = 0;
			botRight.w = 0;
			neighbour.push_back(botRight);

			SDL_Rect botMid;
			botMid.x = x * xSize;
			botMid.y = ((y + 1) * ySize);
			botMid.h = 0;
			botMid.w = 0;
			neighbour.push_back(botMid);

			SDL_Rect botLeft;
			botLeft.x = ((x + 1) * xSize);
			botLeft.y = ((y + 1) * ySize);
			botLeft.h = 0;
			botLeft.w = 0;
			neighbour.push_back(botLeft);

			for(unsigned int i = 0; i < neighbour.size(); i++)
			{
				SDL_Rect r = neighbour[i];
				if((r.x >= 0 && r.x <= widthLevel) && (r.y >= 0 && r.y <= heightLevel))
				{
					if(r.x/xSize < widthLevel/xSize && r.y/ySize < heightLevel/ySize)
					{
						Node* neigh = map[r.x/xSize][r.y/ySize];
						
						node->setNeighbour(i, neigh);
					}
					else
					{
						node->setNeighbour(i, NULL);
					}
				}
				else
				{
					node->setNeighbour(i, NULL);
				}
			}
		}
	}
}

float calculateHScore(Node startingNode, Node endNode, double xSize, double ySize)
{
	float xMove = 0;
	float yMove = 0;

	if(startingNode.getLocation().x < endNode.getLocation().x)
	{
		while(startingNode.getLocation().x + (xMove * xSize) != endNode.getLocation().x)
		{
			xMove++;
		}

		if(startingNode.getLocation().y < endNode.getLocation().y)
		{
			while(startingNode.getLocation().y + (yMove * ySize) != endNode.getLocation().y)
			{
				yMove++;
			}
		}
		else
		{
			while(startingNode.getLocation().y + (yMove * ySize) != endNode.getLocation().y)
			{
				yMove--;
			}
			yMove *= -1;
		}
	}
	else
	{
		while(startingNode.getLocation().x + (xMove * xSize) != endNode.getLocation().x)
		{
			xMove--;
		}
		xMove *= -1;

		if(startingNode.getLocation().y < endNode.getLocation().y)
		{
			while(startingNode.getLocation().y + (yMove * ySize) != endNode.getLocation().y)
			{
				yMove++;
			}
		}
		else
		{
			while(startingNode.getLocation().y + (yMove * ySize) != endNode.getLocation().y)
			{
				yMove--;
			}
			yMove *= -1;
		}
	}

	return xMove + yMove;
}


std::vector<Node*> PathFinding::findPath(SDL_Rect start, SDL_Rect end, double xSize, double ySize)
{
	for each(std::vector<Node*> vn in map)
	{
		for each(Node* n in vn)
		{
			n->setFScore(0);
			n->setGScore(0);
			n->setHScore(0);
			n->setParentNode(NULL);

		}
	}

	if(!openList.empty())
	{
		openList.clear();
	}

	if(!closedList.empty())
	{
		closedList.clear();
	}

	Node* startNode;
	Node* endNode;

	int xPos = 0;
	int yPos = 0;

	xPos = start.x/xSize;
	if(start.x%(int)xSize > xSize/2)
	{
		xPos++;
	}

	yPos = start.y/ySize;
	if(start.y%(int)ySize > ySize/2)
	{
		yPos++;
	}

	for each(std::vector<Node*> vn in map)
	{
		for each(Node* n in vn)
		{
			if(n->getLocation().x == xPos * (int)xSize && n->getLocation().y == yPos * (int)ySize)
			{
				startNode = n;
			}
		}
	}

	xPos = end.x/xSize;
	if(end.x%(int)xSize > xSize/2)
	{
		xPos++;
	}

	yPos = end.y/ySize;
	if(end.y%(int)ySize > ySize/2)
	{
		yPos++;
	}

	for each(std::vector<Node*> vn in map)
	{
		for each(Node* n in vn)
		{
			if(n->getLocation().x == xPos*xSize && n->getLocation().y == yPos*ySize)
			{
				endNode = n;
			}
		}
	}

	startNode->setHScore(calculateHScore(*startNode, *endNode, xSize, ySize));
	openList.push_back(startNode);

	while(!openList.empty())
	{
		Node* activeNode;

		float minFScore = 9999;
		for each(Node* n in openList)
		{
			if(n != nullptr && n->getFScore() < minFScore && n->getWalkable())
			{
				minFScore = n->getFScore();
				activeNode = n;
			}
		}

		if(activeNode->getLocation().x == endNode->getLocation().x && activeNode->getLocation().y == endNode->getLocation().y)
		{
			std::vector<Node*> returnList;
			Node* nodeToAdd = activeNode;
			while(nodeToAdd != NULL)
			{
				returnList.push_back(nodeToAdd);
				nodeToAdd = nodeToAdd->getParentNode();
			}
			return returnList;
		}
		else
		{
			for(unsigned int i = 0; i < activeNode->getNeighbour().size() - 1; i++)
			{
				Node* n = activeNode->getNeighbour()[i];
				float newGValue = 0;
				if(n != NULL)
				{
					if(n->getWalkable())
					{
						if(n != nullptr && (n->getLocation().x - xSize == activeNode->getLocation().x && n->getLocation().y == activeNode->getLocation().y) || 
							(n->getLocation().x + xSize == activeNode->getLocation().x && n->getLocation().y == activeNode->getLocation().y) ||
								(n->getLocation().x == activeNode->getLocation().x && n->getLocation().y - ySize == activeNode->getLocation().y) ||
									(n->getLocation().x == activeNode->getLocation().x && n->getLocation().y + ySize == activeNode->getLocation().y))
						{
							newGValue = activeNode->getGScore() + 1;
						}
						else
						{
							newGValue = activeNode->getGScore() + 1.2f;
						}
					}

					bool inAList = false;
					for each(Node* an in openList)
					{
						if(an == n)
						{
							inAList = true;
						}
					}
					for each(Node* an in closedList)
					{
						if(an == n)
						{
							inAList = true;
						}
					}

					if(!inAList)
					{
						n->setGScore(newGValue);
						n->setFScore(newGValue + calculateHScore(*n, *endNode, xSize, ySize));
						n->setParentNode(activeNode);
						openList.push_back(n);
					}
					else
					{
						if(n->getGScore() > newGValue)
						{
							n->setGScore(newGValue);
							n->setFScore(newGValue + calculateHScore(*n, *endNode, xSize, ySize));
							n->setParentNode(activeNode);
						}
					}
				}
			}
		}
		closedList.push_back(activeNode);
		std::vector<Node*>::iterator it = openList.begin();
		while((*it)->getLocation().x != activeNode->getLocation().x &&
				(*it)->getLocation().y != activeNode->getLocation().y)
		{
			it++;
		}
		openList.erase(it);
	}
	std::vector<Node*> returnList;
	return returnList;
}

void PathFinding::cleanup()
{
	for each(std::vector<Node*> vn in map)
	{
		for each(Node* n in vn)
		{
			delete n;
			n = NULL;
		}
	}
}