#pragma once
#ifndef NODE_H
#define NODE_H

#include <SDL.h>
#include <vector>

//These nodes are used for the pathfinding and store information fo
//each tile.

class Node
{
public:
	Node()
	{
		//Create the array for this nodes neighbors.
		neighbors.resize(8);
		//We do not know its location yet.
		location.x = 0;
		location.y = 0;
		location.w = 0;
		location.h = 0;
		//we do not know if it is walkable ot not.
		walkable = false;
		//We do not know its scroe.
		fScore = 0;
		gScore = 0;
		hScore = 0;
		//We do not know its parent.
		parentNode = NULL;
	}
	
	inline void setLocation(int x, int y)
	{ location.x =x; location.y =y; }
	
	inline void setWalkable(bool walk)
	{ walkable = walk; }
	
	inline void setFScore(float fs)
	{ fScore = fs; }
	
	inline void setGScore(float gs)
	{ gScore = gs; }
	
	inline void setHScore(float hs)
	{ hScore = hs; }
	
	inline void setParentNode(Node* n)
	{ parentNode = n; }
	
	inline SDL_Rect getLocation()
	{ return location; }
	
	inline bool getWalkable()
	{ return walkable; }
	
	inline float getFScore()
	{ return fScore; }
	
	inline float getGScore()
	{ return gScore; }
	
	inline float getHScore()
	{ return hScore; }
	
	inline std::vector<Node*> getNeighbour()
	{ return neighbors; }

	inline void setNeighbour(int location, Node* n)
	{neighbors[location] = n;}
	
	inline Node* getParentNode()
	{ return parentNode; }
	
	//Might need to make this more clear.
	inline bool operator==(Node n)
	{ return (location.x == n.getLocation().x) && (location.y == n.getLocation().y); }

	inline bool operator!=(Node n)
	{ return (location.x != n.getLocation().x) && (location.y != n.getLocation().y); }

	inline void cleanup()
	{
		if(parentNode != NULL)
		{
			delete parentNode;
			parentNode = NULL;
		}
		for each(Node* n in neighbors)
		{
			if(n != NULL)
			{
				delete n;
				n = NULL;
			}
		}
	}

private:
	SDL_Rect location;
	bool walkable;
	float fScore;
	float gScore;
	float hScore;
	std::vector<Node*> neighbors;
	Node *parentNode;
};

#endif