#include <SDL.h>
#include "Engine.h"

int main(int argc, char* args[])
{
	//The engine for the game.  This takes care of the
	//updating and drawing.
	Engine engine;
	
	//Keeps tracks of keypresses.
	Uint8* event;
	
	//I only used this one to check if the user pressed
	//the 'X' to close the game.
	SDL_Event xEvent;

	//Initialize the engine.
	engine.init();

	//Set the window title.
	SDL_WM_SetCaption("Candy in the Dark", NULL);
 
	//Get the ticks between frames.
	unsigned long lastFrameTime = SDL_GetTicks();
	
	//As the engine is running.
	while(engine.isRunning()) 
	{
		//Pump the events to fill the keypress array.
		SDL_PumpEvents();
		//Grab the key states after we pump them through.
		event = SDL_GetKeyState(NULL);
		//Process the keys according to the engine.
		engine.processEvent(event);
		//Calculate the elapsed time since last frame.
		unsigned long time = SDL_GetTicks();
		double delta = time - lastFrameTime;

		//All update functions take in the delta between
		//the last frame and the current one.  This helps
		//make the game framerate independent.
		engine.update(delta);
		//Render the images onto the screen.
		engine.render();					

		lastFrameTime = time;
    }
	//If the engine stopped running we will do the clean up
	engine.cleanup();
    return 0; 
}