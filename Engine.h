#ifndef ENGINE_H
#define ENGINE_H

#include <SDL.h>
#include "Mora.h"
#include "EnemyBBaby.h"
#include "Collidable.h"
#include "Level.h"
#include "LevelLoader.h"
#include "LevelEditor.h"
#include "Menu.h"
#include "MusicPlayer.h"

class Engine
{
public:
		//This creates the engine.  It will call the
		//needed constructors to make the game run. 
		Engine();
		//This calles init on all the objects constructed.
		bool init();
		//This cleans up the entire game.
		void cleanup();
		//This processes key events for the escaping of the game.
		void processEvent(Uint8* keystates);
		//This updates the objects.
		void update(double delta);
		//This renders the level.
		void render();
		//This will tell the engine not to run anymore.
		void notRunning();
		//This is to check if the engine is running.
		bool isRunning() { return running; }
		//Removing the fog of war.
		void removeFogOfWar(double x, double y); 
private:
	//The menu for the game
	Menu menu;
	//Bools for the F1 button changes.
	bool currentButtonStatus;
	bool prevButtonStatus;
	//The surfaces that make up the underlying screen.
	SDL_Surface* screenBackground;
	SDL_Surface* gameOver;
	
	//Bools for the engine.
	bool running;
	bool gameRunning;
	bool levelEditorRunning;

	//The level loader.
	LevelLoader levelLoader;
	//The level object we will always use.
	Level currentLevel;
	//The level editor.
	LevelEditor levelEditor;
	
	SDL_Event event;

	//The music player for the game.
	MusicPlayer musicPlayer;

	static const int ONE_SECOND = 1;
};

#endif