#include "EnemyBBaby.h"
#include "Global.h"

#include <cmath>

//The movement speed.
double const EnemyBBaby::moveSpeed = .075;
double const EnemyBBaby::BBABY_FRAMERATE_TIMER = .5;

std::string const EnemyBBaby::BBABY_FILENAME = "Art/EnemyBBabySpriteSheet.png";

EnemyBBaby::EnemyBBaby()
{
	//BBaby starts not attacking.
	attacking = false;
	attackActivated = false;
	dead = false;
	//This creates the pathfinding for the Bbaby.
	pathFinder = PathFinding();
	//Creates the Bbaby at (0,0).
	xPosition = DEFAULT_ZERO;
	yPosition = DEFAULT_ZERO;
	//Set the frame update timer to zero.
	frameUpdateTimer = DEFAULT_ZERO;
	//Set attack timer to zero.
	attackTimer = DEFAULT_ZERO;
	//The frame to play.
	currentFrame = DEFAULT_ZERO;
	//This limits the frame of the animation to make sure
	//frames play at the right speed.
	frameIndex = DEFAULT_ZERO;
	//The Bbaby's status.
	currentStatus = mIdle;
	//The offsets to help cut Bbaby's spritesheet into frames.
	int xOff = DEFAULT_ZERO;
	int yOff = DEFAULT_ZERO;
	for(int i = DEFAULT_ZERO; i < BBABY_ROW * BBABY_COLUMN; i++)
	{
		if(i !=DEFAULT_ZERO && i%BBABY_COLUMN == DEFAULT_ZERO)
		{
			xOff = DEFAULT_ZERO;
			yOff += BBABY_HEIGHT;
		}
		clip[i].x = xOff;
		clip[i].y = yOff;
		clip[i].w = BBABY_WIDTH;
		clip[i].h = BBABY_HEIGHT;
		xOff += BBABY_WIDTH;
	}
	//This is the collision box of BBaby.
	monsterBox.x = xPosition;
	monsterBox.y = yPosition;
	monsterBox.w = BBABY_WIDTH;
	monsterBox.h = BBABY_HEIGHT;
	//This where BBaby will want to go.  BBaby will use this to
	//make sure where it wants to go is valid.
	futureMonsterBox.x = xPosition;
	futureMonsterBox.y = yPosition;
	futureMonsterBox.w = BBABY_WIDTH;
	futureMonsterBox.h = BBABY_HEIGHT;
	//This is the collision box that represents the attack range.
	//If Mora is within this range BBaby will try to attack her.
	attackRange.x = xPosition - 4;
	attackRange.y = yPosition - 4;
	attackRange.w = BBABY_WIDTH + 8;
	attackRange.h = BBABY_HEIGHT + 8;
	//This is the collision box that repreents the attack.
	attackBox.x = NA_VALUE;
	attackBox.y = NA_VALUE;
	attackBox.w = BBABY_WIDTH;
	attackBox.h = BBABY_HEIGHT;
	//The health bar of the monster.  It sits on top of the monster.
	healthNow = MAX_HEALTH;
	healthBar = HPBar(xPosition, yPosition, MAX_HEALTH, DEFAULT_ZERO, HPBAR_SHOW_TIME);
}

EnemyBBaby::EnemyBBaby(double xPos, double yPos)
{
	//BBaby starts not attacking.
	attacking = false;
	attackActivated = false;
	dead = false;
	//This creates the pathfinding for the Bbaby.
	pathFinder = PathFinding();
	//Creates the Bbaby at (xPos,yPos).
	xPosition = xPos;
	yPosition = yPos;
	//Set the update timer to zero.
	frameUpdateTimer = DEFAULT_ZERO;
	//Set attack timer to zero.
	attackTimer = DEFAULT_ZERO;
	//The frame to play.
	currentFrame = DEFAULT_ZERO;
	//This limits the frame of the animation to make sure
	//frames play at the right speed.
	frameIndex = DEFAULT_ZERO;
	//The Bbaby's status.
	currentStatus = mIdle;
	//The offsets to help cut Bbaby's spritesheet into frames.
	int xOff = DEFAULT_ZERO;
	int yOff = DEFAULT_ZERO;
	for(int i = DEFAULT_ZERO; i < BBABY_ROW * BBABY_COLUMN; i++)
	{
		if(i !=DEFAULT_ZERO && i%BBABY_COLUMN == DEFAULT_ZERO)
		{
			xOff = DEFAULT_ZERO;
			yOff += BBABY_HEIGHT;
		}
		clip[i].x = xOff;
		clip[i].y = yOff;
		clip[i].w = BBABY_WIDTH;
		clip[i].h = BBABY_HEIGHT;
		xOff += BBABY_WIDTH;
	}
	//This is the collision box of BBaby.
	monsterBox.x = xPosition;
	monsterBox.y = yPosition;
	monsterBox.w = BBABY_WIDTH;
	monsterBox.h = BBABY_HEIGHT;
	//This where BBaby will want to go.  BBaby will use this to
	//make sure where it wants to go is valid.
	futureMonsterBox.x = xPosition;
	futureMonsterBox.y = yPosition;
	futureMonsterBox.w = BBABY_WIDTH;
	futureMonsterBox.h = BBABY_HEIGHT;
	//This is the collision box that represents the attack range.
	//If Mora is within this range BBaby will try to attack her.
	attackRange.x = xPosition - 4;
	attackRange.y = yPosition - 4;
	attackRange.w = BBABY_WIDTH + 8;
	attackRange.h = BBABY_HEIGHT + 8;
	//This is the collision box that repreents the attack.
	attackBox.x = NA_VALUE;
	attackBox.y = NA_VALUE;
	attackBox.w = BBABY_WIDTH;
	attackBox.h = BBABY_HEIGHT;
	healthNow = MAX_HEALTH;
	healthBar = HPBar(xPosition, yPosition, MAX_HEALTH, DEFAULT_ZERO, HPBAR_SHOW_TIME);
}

bool EnemyBBaby::init()
{
	//Load the spritesheet for the charcter.
	pause = false;
	bbabySpriteSheet = load_image(BBABY_FILENAME);
	healthBar.init();

	return true;
}

void EnemyBBaby::cleanup()
{
	//This frees the image.
	SDL_FreeSurface(bbabySpriteSheet);
	pathFinder.cleanup();
	healthBar.cleanup();
}

void EnemyBBaby::update(double delta, Mora m)
{
	if(!dead && !pause)
	{
		frameUpdateTimer += delta * .001;
		attackTimer += delta * .001;

		//Reset attacking.
		if(!attacking)
		{
			attackActivated = false;
			attackBox.x = NA_VALUE;
			attackBox.y = NA_VALUE;
			if(attackTimer < DEFAULT_ZERO)
			{
				attackTimer = DEFAULT_ZERO;
			}
		}

		healthBar.update(delta, xPosition, yPosition - healthBar.getHPBarHeight(), healthNow);

		//If BBaby are colliding with Mora, find the side BBaby is colliding with and play the right attack animation.
		if(attackTimer >= TIME_BETWEEN_ATTACKS && checkCollision(attackRange, m.getCharacterBox()))
		{
			//If BBaby are attacking it does not move.
			noStep();
			//Set BBaby attack to melee type.
			currentAttack = melee;
			//BBaby is attacking.
			attacking = true;
			attackTimer = NNA_VALUE;
		}
		else
		{
			if(checkCollision(monsterBox, m.getCharacterBox()))
			{
				noStep();
			}
			else
			{
				//BBaby take a step if he is not attacking.
				monsterMusicPlayer->playWoodCreak(distance(monsterBox, m.getCharacterBox()));
				step();
			}
		}

		if(attacking && (currentFrame == BBABY_ATTACK_UP_END_FRAME || currentFrame == BBABY_ATTACK_DOWN_END_FRAME || currentFrame == BBABY_ATTACK_LEFT_END_FRAME || currentFrame == BBABY_ATTACK_RIGHT_END_FRAME))
		{
			if(currentFrame == BBABY_ATTACK_UP_END_FRAME)
			{
				//Move the box to the right place.
				attackBox.x = xPosition;
				attackBox.y = yPosition - BBABY_HEIGHT;
			}
			else if(currentFrame == BBABY_ATTACK_DOWN_END_FRAME)
			{
				//Move the box to the right place.
				attackBox.x = xPosition;
				attackBox.y = yPosition + BBABY_HEIGHT;
			}
			else if(currentFrame == BBABY_ATTACK_LEFT_END_FRAME)
			{
				//Move the box to the right place.
				attackBox.x = xPosition - BBABY_WIDTH;
				attackBox.y = yPosition;
			}
			else if(currentFrame == BBABY_ATTACK_RIGHT_END_FRAME)
			{
				//Move the box to the right place.
				attackBox.x = xPosition + BBABY_WIDTH;
				attackBox.y = yPosition;
			}

			attacking = false;
			attackActivated = true;
		}

		updatePathfinding(delta, m);

		if(frameUpdateTimer >= BBABY_FRAMERATE_TIMER)
		{
			updateFrames(m);
		}

		if(healthNow <= 0)
		{
			dead = true;
			cleanup();
		}
	}
}

void EnemyBBaby::updateFrames(Mora m)
{
	//If BBaby are colliding with Mora, find the side BBaby is colliding with and play the right attack animation.
	if(attacking)
	{
		if(checkSide(attackRange, m.getCharacterBox()) == UP)
		{
			currentFrame++;
			if(currentFrame < BBABY_ATTACK_DOWN_END_FRAME || currentFrame > BBABY_ATTACK_UP_END_FRAME)
			{	
				currentFrame = BBABY_ATTACK_UP_START_FRAME;
			}
		}
		else if(checkSide(attackRange, m.getCharacterBox()) == DOWN)
		{
			currentFrame++;
			if(currentFrame < BBABY_DOWN_END_FRAME || currentFrame > BBABY_ATTACK_DOWN_END_FRAME)
			{	
				currentFrame = BBABY_ATTACK_DOWN_START_FRAME;
			}
		}
		else if(checkSide(attackRange, m.getCharacterBox()) == LEFT)
		{
			currentFrame++;
			if(currentFrame < BBABY_ATTACK_RIGHT_END_FRAME || currentFrame > BBABY_ATTACK_LEFT_END_FRAME)
			{	
				currentFrame = BBABY_ATTACK_LEFT_START_FRAME;
			}
		}
		else if(checkSide(attackRange, m.getCharacterBox()) == RIGHT)
		{
			currentFrame++;
			if(currentFrame < BBABY_ATTACK_DOWN_END_FRAME || currentFrame > BBABY_ATTACK_RIGHT_END_FRAME)
			{	
				currentFrame = BBABY_ATTACK_RIGHT_START_FRAME;
			}
		}
	}
	else
	{
		//If it is time to play the next frame, find the right frame based
		//on status.
		if(currentStatus == mUp)
		{
			currentFrame++;
			if(currentFrame < BBABY_RIGHT_END_FRAME || currentFrame > BBABY_UP_END_FRAME)
			{	
				currentFrame = BBABY_UP_START_FRAME;
			}
		}
		else if(currentStatus == mDown)
		{
			currentFrame++;
			if(currentFrame < BBABY_UP_END_FRAME || currentFrame > BBABY_DOWN_END_FRAME)
			{	
				currentFrame = BBABY_DOWN_START_FRAME;
			}
		}
		else if(currentStatus == mLeft)
		{
			currentFrame++;
			if(currentFrame < BBABY_LEFT_START_FRAME || currentFrame > BBABY_LEFT_END_FRAME)
			{	
				currentFrame = BBABY_LEFT_START_FRAME;
			}
		}
		else if(currentStatus == mRight)
		{
			currentFrame++;
			if(currentFrame < BBABY_LEFT_END_FRAME || currentFrame > BBABY_RIGHT_END_FRAME)
			{	
				currentFrame = BBABY_RIGHT_START_FRAME;
			}
		}
	}
	frameUpdateTimer = DEFAULT_ZERO;
}

void EnemyBBaby::updatePathfinding(double delta, Mora m)
{
	//BBaby will always want to find the next path to get it closer to Mora
	//so it will tell the path finder to find the next path.
	if(path.empty())
	{
		if(checkCollision(attackRange, m.getCharacterBox()))
		{
			monsterMusicPlayer->playScream();
		}
		//Clean the path.
		path.clear();
		//These rectangles are close reps of where BBaby and Mora are.
		SDL_Rect monsterRect;
		SDL_Rect moraRect;
		monsterRect.x = (int)monsterBox.x;
		monsterRect.y = (int)monsterBox.y;
		monsterRect.w = (int)monsterBox.w;
		monsterRect.h = (int)monsterBox.h;
		moraRect.x = (int)m.getCharacterBox().x;
		moraRect.y = (int)m.getCharacterBox().y;
		moraRect.w = (int)m.getCharacterBox().w;
		moraRect.h = (int)m.getCharacterBox().h;
		//Call findPath to find the path.
		path = pathFinder.findPath(monsterRect, moraRect, TILE_SIZE, TILE_SIZE);
	}
	if(!path.empty())
	{
		//When BBaby is close to the current point on the list of points
		//to travel to BBaby will pop the point and move to the next one.
		//There are a lot of magic numbers here but I will leave it
		//for now.
		if(monsterBox.x - 3 <= path[path.size() -1]->getLocation().x 
			&& monsterBox.x + 3 >= path[path.size() -1]->getLocation().x 
				&& monsterBox.y - 3 <= path[path.size() -1]->getLocation().y 
					&& monsterBox.y + 3 >= path[path.size() -1]->getLocation().y)
		{
			path.pop_back();
		}
		//If the path is not empty BBaby will want to do some math
		//to find the vector to get to the next point.
		if(!path.empty())
		{
			//Find the vector to move the monster to the next point in pathfinding.
			CollisionBox pathingVector;
			pathingVector.x = path[path.size() -1]->getLocation().x - monsterBox.x;
			pathingVector.y = path[path.size() -1]->getLocation().y - monsterBox.y;
			pathingVector.x = (pathingVector.x/sqrt(pathingVector.x * pathingVector.x + pathingVector.y * pathingVector.y));
			pathingVector.y = (pathingVector.y/sqrt(pathingVector.x * pathingVector.x + pathingVector.y * pathingVector.y));
			futureMonsterBox.x += pathingVector.x * moveSpeed * delta;
			futureMonsterBox.y += pathingVector.y * moveSpeed * delta;
			//Based on how BBaby are moving it will have a different status;
			if(pathingVector.y * moveSpeed > 0)
			{
				currentStatus = mDown;
			}
			else if(pathingVector.y * moveSpeed < 0)
			{
				currentStatus = mUp;
			}
			if(pathingVector.x * moveSpeed > .03)
			{
				currentStatus = mRight;
			}
			else if(pathingVector.x * moveSpeed < -.03)
			{
				currentStatus = mLeft;
			}
		}
	}
}

void EnemyBBaby::render(SDL_Surface* dest)
{
	if(!dead)
	{
		apply_surface((int)xPosition, (int)yPosition, bbabySpriteSheet, dest, &clip[currentFrame]);
		healthBar.render(dest);
	}
}

void EnemyBBaby::step()
{
	//If BBaby are moving offscreen BBaby will not take a step.
	if((futureMonsterBox.x <= SCREEN_WIDTH - BBABY_WIDTH && futureMonsterBox.x >= 0) &&
		(futureMonsterBox.y <= SCREEN_HEIGHT - BBABY_HEIGHT && futureMonsterBox.y >= 0))
	{
		//BBaby's monster box will take on the value of the future box. 
		xPosition = futureMonsterBox.x;
		yPosition = futureMonsterBox.y;
		monsterBox.x = xPosition;
		monsterBox.y = yPosition;
		attackRange.x = xPosition - 4;
		attackRange.y = yPosition - 4;
	}
	else
	{
		noStep();
	}
}

void EnemyBBaby::noStep()
{
	//If BBaby can not move to where it wants to go he revert himself
	//back to a previous location.
	futureMonsterBox.x = monsterBox.x;
	futureMonsterBox.y = monsterBox.y;
}

void EnemyBBaby::makeMap(int widthLevel, int heightLevel, std::vector<Collidable> listOfCollidables)
{
	//Make the map for the pathfinder for the current level.
	pathFinder.createMap(widthLevel, heightLevel, TILE_SIZE, TILE_SIZE, listOfCollidables);
}

void EnemyBBaby::applyDamage(int dmg)
{
	healthBar.showForTime();
	healthNow -= dmg;
}

int EnemyBBaby::getDamage()
{
	if(currentAttack == melee)
	{
		monsterMusicPlayer->playTeethChatter();
		return MELEE_DAMAGE;
	}
	return DEFAULT_ZERO;
}