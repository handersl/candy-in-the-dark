#pragma once
#ifndef CLICKABLE_H
#define CLICKABLE_H

#include <SDL.h>
#include <string>
#include "LevelEditorTiles.h"

//Clickable types hold level editor tiles and tells the level editor how to use it when clicked.

//A selectable tile can not be deleted but selected.  A tiled tile is one that is placed 
//and can be deleted.
enum clickableType
{
	tiled, selectable
};

class Clickable
{
public:
	Clickable();
	//Create a tile at the position with the level editor tile.
	Clickable(int xPos, int yPos, int width, int height, LevelEditorTiles* let, clickableType clickType);
	//Give it an icon if the level editor tile does not have one.
	void setClickableIcon(std::string clickableIconFilename);
	//Draw it.
	void render(SDL_Surface* dest);
	//Clean up the used data.
	void cleanup();
	//Clean up the tile.
	void cleanTile();
	//The box representing the clickable object,
	SDL_Rect getBox();
	//Return the level editor tile.
	LevelEditorTiles getTile();
	bool isTileNull();
	//Set the strings for the level editor tile if needed.
	void setTileReaderString(std::string readerString);
	void setTilesNextLevelString(std::string nlString);
	void setTileDialogueString(std::string diaString);
	void setTileDialogueIcon(std::string iconStr);
	void setTileDialogueCharacter(std::string diaCharString);
private:
	SDL_Rect tileBox;
	SDL_Surface* clickableIcon;
	LevelEditorTiles* tile;
	clickableType type;
};

#endif