#pragma once
#ifndef LEVELEDITOR_H
#define LEVELEDITOR_H

#include <SDL.h>
#include <vector>
#include <string>
#include "Clickable.h"
#include "LevelEditorTiles.h"
#include "TextBox.h"

//More comments in "LevelEditor.cpp"

enum quads
{
	gridQuad, moraQuad, monsterQuad, itemQuad, groundQuad, saveQuad, NAQuad
};

class LevelEditor
{
public:
	LevelEditor();
	void init();
	void update(double delta);
	void render(SDL_Surface* dest);
	void cleanup();
	void mouseQuad(double xPos, double yPos);
	void placer();
	void changeCurrentMouse();
	void monsterArrows();
	void itemArrows();
	LevelEditorTiles pickGroundTile(double xPos, double yPos);
	void updateMouseState();
	void deletion();
	void createLevelFile(std::string  fileNameAndLocation);
	bool checkIfOn(double xPos, double yPos, Clickable cli);
	bool checkIfLeftClickedOn(double xPos, double yPos, Clickable cli);
	bool checkIfRightClickedOn(double xPos, double yPos, Clickable cli);
	bool checkIfLeftClickedOnOnce(double xPos, double yPos, Clickable cli);
private:
	int mouseXPosition;
	int mouseYPosition;
	bool leftMouseNow;
	bool leftMousePast;
	quads currentMouseQuad;
	LevelEditorTiles currentMouse;

	Clickable gridArea;
	Clickable moraArea;
	Clickable monsterArea;
	Clickable itemArea;
	Clickable groundTileArea;
	Clickable saveArea;

	Clickable moraIcon;
	
	Clickable monsterLeft;
	Clickable monsterRight;
	std::vector<Clickable> monsterIcon;
	int currentMonster;
	
	Clickable itemUp;
	Clickable itemDown;
	Clickable itemLeft;
	Clickable itemRight;
	std::vector<Clickable> itemIcon;
	int currentItemX;
	int currentItemY;

	Clickable saveIcon;
	
	std::vector<Clickable> levelSpriteSheet;
	
	Clickable moraTile;
	std::vector<Clickable> groundTiles;
	std::vector<Clickable> monsterTiles;
	std::vector<Clickable> itemTiles;

	bool redraw;
	bool currentlySaving;
	bool currentlyCreatingReaderItem;
	bool currentlyCreatingDoor;
	bool currentlyCreatingDialogue;

	//Which stage of saving are we on when creating dialogue.
	int dialogueCreationStage;

	TextBox levelEditorTextBox;

	//The max number of stages(base zero).
	static const int DIALOGUE_CREATION_MAX = 2;
	//The total number of items base zero.
	static const int ITEM_MAX = 2;
	//The start and end in the array of the first type of items: readable items such as dialogue.
	static const int ITEM_ZERO_START = 0;
	static const int ITEM_ZERO_END = 1;
	//The start and end in the array of the second type of items: powerups for Mora.
	static const int ITEM_ONE_START = 2;
	static const int ITEM_ONE_END = 2;
	
	static const int MORA_AREA_WIDTH = 280;
	static const int MORA_AREA_HEIGHT = 111;
	static const int MORA_ICON_X = 121;
	static const int MORA_ICON_Y = 37;

	static const int MONSTER_AREA_WIDTH = 280;
	static const int MONSTER_AREA_HEIGHT = 111;
	static const int MONSTER_LEFT_X = 84;
	static const int MONSTER_LEFT_Y = 37 + MORA_AREA_HEIGHT;
	static const int MONSTER_RIGHT_X = 158;
	static const int MONSTER_RIGHT_Y = 37 + MORA_AREA_HEIGHT;
	static const int MONSTER_ICON_X = 121;
	static const int MONSTER_ICON_Y = 37 + MORA_AREA_HEIGHT;

	static const int ITEM_AREA_WIDTH = 280;
	static const int ITEM_AREA_HEIGHT = 111;
	static const int ITEM_UP_X = 121;
	static const int ITEM_UP_Y = MORA_AREA_HEIGHT + MONSTER_AREA_HEIGHT;
	static const int ITEM_DOWN_X = 121;
	static const int ITEM_DOWN_Y = 74 + MORA_AREA_HEIGHT + MONSTER_AREA_HEIGHT;
	static const int ITEM_LEFT_X = 84;
	static const int ITEM_LEFT_Y = 37 + MORA_AREA_HEIGHT + MONSTER_AREA_HEIGHT;
	static const int ITEM_RIGHT_X = 158;
	static const int ITEM_RIGHT_Y = 37 + MORA_AREA_HEIGHT + MONSTER_AREA_HEIGHT;
	static const int ITEM_ICON_X = 121;
	static const int ITEM_ICON_Y = 37 + MORA_AREA_HEIGHT + MONSTER_AREA_HEIGHT;

	static const int GROUND_TILE_AREA_WIDTH = 280;
	static const int GROUND_TILE_AREA_HEIGHT = 185;
	static const int GROUND_TILE_ROW_ONE_Y = MORA_AREA_HEIGHT + MONSTER_AREA_HEIGHT + ITEM_AREA_HEIGHT;
	static const int GROUND_TILE_ROW_ONE_X = 74;
	static const int GROUND_TILE_ROW_TWO_Y = 37 + MORA_AREA_HEIGHT + MONSTER_AREA_HEIGHT + ITEM_AREA_HEIGHT;
	static const int GROUND_TILE_ROW_TWO_X = 74;
	static const int GROUND_TILE_ROW_THREE_Y = 74 + MORA_AREA_HEIGHT + MONSTER_AREA_HEIGHT + ITEM_AREA_HEIGHT;
	static const int GROUND_TILE_ROW_THREE_X = 74;

	static const int SAVE_AREA_WIDTH = 280;
	static const int SAVE_AREA_HEIGHT = 111;
	static const int SAVE_ICON_X = 121;
	static const int SAVE_ICON_Y = 37 + MORA_AREA_HEIGHT + MONSTER_AREA_HEIGHT + ITEM_AREA_HEIGHT + GROUND_TILE_AREA_HEIGHT;

	static const int TEXT_BOX_X = (1024/2 - 250);
	static const int TEXT_BOX_Y = (768/2 - 100);
	
	static std::string const UP_ARROW_ICON_FILENAME;
	static std::string const DOWN_ARROW_ICON_FILENAME;
	static std::string const LEFT_ARROW_ICON_FILENAME;
	static std::string const RIGHT_ARROW_ICON_FILENAME;
	static std::string const EMPTY_TILE_FILENAME;
	static std::string const SAVE_ICON_FILENAME;
};

#endif