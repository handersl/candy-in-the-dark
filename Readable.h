#pragma once
#ifndef READABLE_H
#define READABLE_H

#include "Items.h"

//This is a readable item.  it is represented by a clipboard on the gound.
//When the player walks to it and presses a button a large scrollable clipboard with
//text fills the screen.  It also pauses the game giving the player time to read.

enum readerType
{
	clipboard
};

class Readable : public Items
{
public:
	//Create the readable.
	virtual bool init();
	//Create the readable at (xPos, yPos) with a readType and the string to the
	//image containing the text.
	void load(double xPos, double yPos, readerType readType, std::string print);
	//Clean up the item.
	virtual void cleanup();
	//Update according to Mora.
	virtual void update(double delta, Mora mora);
	//Draw the clipboard.
	virtual void render(SDL_Surface* dest);
	//Renders on the top of the black overlay.
	virtual void renderTop(SDL_Surface* dest);
	void isReading()
	{ reading = true; }
	void notReading()
	{ reading = false; justFinished = true; }
	void notJustFinished()
	{ justFinished = false; }
	bool getReading()
	{ return reading; }
	bool getJustFinished()
	{ return justFinished; }
	//Changes done to Mora by the item.
	virtual void moraChanges(Mora* mora);
	//Changes done to monsters by the item.
	virtual void monsterChanges(std::vector<Monsters*> mons);
	virtual CollisionBox getItemCollisionBox()
	{ return itemBox; }
	virtual bool isFinished();
private:
	SDL_Surface* itemSprite;
	SDL_Surface* readerSprite;
	SDL_Surface* printSprite;
	
	CollisionBox itemBox;

	Uint8* keystates;

	bool reading;
	bool justFinished;

	bool currentButtonStatus;
	bool prevButtonStatus;

	double itemXPosition;
	double itemYPosition;
	double readerXPosition;
	double readerYPosition;
	double printXPosition;
	double printYPosition;
	double readerMaxX;
	double readerMaxY;
	double printYOffset;
	
	static const int CLIPBOARD_READER_X = 74;
	static const int CLIPBOARD_READER_Y = 92;
	static const int CLIPBOARD_PRINT_X = 128;
	static const int CLIPBOARD_PRINT_Y = 200;
	static const int CLIPBOARD_MAX_X = 740;
	static const int CLIPBOARD_MAX_Y = 444;

	static const std::string CLIPBOARD_ITEM_SPRITE;
	static const std::string CLIPBOARD_READER_SPRITE;
};

#endif
