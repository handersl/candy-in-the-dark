#pragma once
#ifndef MUSICPLAYER_H
#define MUSICPLAYER_H

#include <SDL.h>
#include <SDL_mixer.h>
#include "Global.h"

class MusicPlayer
{
public:
	//Inits the music player.
	bool init();
	//Load all the music files.	
	bool loadFiles();
	//Clean the memory.
	void cleanup();
	//Play the effects.
	void playFootSteps();
	void playStab();
	void playTeethChatter();
	void playTeleport();
	void playSelect();
	void playScream();
	//This sound plays at a variable vol. depending on a distance.
	void playWoodCreak(int distance);
	void playPanic();
	void playHearbeat();
	//Play the song until it is told to stop.
	void playCandyInTheDark();
	void stopCandyInTheDark();
	
private:
	Mix_Chunk* footSteps;
	Mix_Chunk* stab;
	Mix_Chunk* chatter;
	Mix_Chunk* teleport;
	Mix_Chunk* select;
	Mix_Chunk* monsterScream;
	Mix_Chunk* woodCreak;
	Mix_Chunk* panic;
	Mix_Chunk* heartbeat;
	Mix_Music* candyInTheDark;

	//Number of channels.
	static int const NUM_SOUND_CHANNELS = 64;

	//Different channels for different songs/sounds. -1 lets the library play
	//the sound on the next avalible channel.
	static int const FOOTSTEP_SOUNDEFFECT_CHANNEL = 1;
	static int const CANDYINTHEDARK_MUSIC_CHANNEL = 2;
	static int const STAB_SOUNDEFFECT_CHANNEL = 3;
	static int const CHATTER_SOUNDEFFECT_CHANNEL = 4;
	static int const TELEPORT_SOUNDEFFECT_CHANNEL = 5;
	static int const SELECT_SOUNDEFFECT_CHANNEL = 6;
	static int const MONSTER_SCREAM_SOUNDEFFECT_CHANNEL = 7;
	static int const WOODCREAK_SOUNDEFFECT_CHANNEL = 8;
	static int const PANIC_SOUND_CHANNEL = 9;
	static int const HEART_BEAT_CHANNEL = 10;

	//The volumes.
	static int const FOOTSTEP_VOLUME = 32;
	static int const STAB_VOLUME = MIX_MAX_VOLUME;
	static int const CHATTER_VOLUME = MIX_MAX_VOLUME;
	static int const TELEPORT_VOLUME = 64;
	static int const SELECT_VOLUME = 64;
	static int const MONSTER_SCREAM_VOLUME = MIX_MAX_VOLUME;
	static int const WOODCREAK_VOLUME = MIX_MAX_VOLUME;
	static int const PANIC_VOLUME = MIX_MAX_VOLUME;
	static int const HEARTBEAT_VOLUME = 8;
	static int const MUSIC_VOLUME = 16;

	static std::string const FOOTSTEP_SOUND_FILENAME;
	static std::string const STAB_SOUND_FILENAME;
	static std::string const CHATTER_SOUND_FILENAME;
	static std::string const TELEPORT_SOUND_FILENAME;
	static std::string const SELECT_SOUND_FILENAME;
	static std::string const MONSTER_SCREAM_FILENAME;
	static std::string const WOODCREAK_FILENAME;
	static std::string const PANIC_SOUND_FILENAME;
	static std::string const HEART_BEAT_FILENAME;
	static std::string const CANDYINTHEDARK_SOUND_FILENAME;
};

#endif
