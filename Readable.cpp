#include "Readable.h"

//More comments in "Readable.h"

std::string const Readable::CLIPBOARD_ITEM_SPRITE = "Art/ClipboardItemSpriteSheet.png";
std::string const Readable::CLIPBOARD_READER_SPRITE = "Art/ClipboardReaderSpriteSheet.png";

bool Readable::init()
{
	return true;
}

void Readable::load(double xPos, double yPos, readerType readType, std::string print)
{
	reading = false;
	justFinished = false;
	currentButtonStatus = false;
	prevButtonStatus = false;

	if(readType == clipboard)
	{
		itemSprite = load_image(CLIPBOARD_ITEM_SPRITE);
		readerSprite = load_image(CLIPBOARD_READER_SPRITE);
		printSprite = load_image(print);

		itemBox.x = xPos;
		itemBox.y = yPos;
		itemBox.w = TILE_SIZE;
		itemBox.h = TILE_SIZE;

		itemXPosition = xPos;
		itemYPosition = yPos;
		readerXPosition = CLIPBOARD_READER_X;
		readerYPosition = CLIPBOARD_READER_Y;
		printXPosition = CLIPBOARD_PRINT_X;
		printYPosition = CLIPBOARD_PRINT_Y;
		readerMaxX = CLIPBOARD_MAX_X;
		readerMaxY = CLIPBOARD_MAX_Y;
		printYOffset = DEFAULT_ZERO;
	}
}

void Readable::cleanup()
{
	SDL_FreeSurface(itemSprite);
	SDL_FreeSurface(readerSprite);
	SDL_FreeSurface(printSprite);
}

void Readable::update(double delta, Mora mora)
{ 
	prevButtonStatus = currentButtonStatus;
	currentButtonStatus = false;
	justFinished = false;

	SDL_PumpEvents();
	keystates = SDL_GetKeyState(NULL);

	if(reading && keystates[SDLK_UP] && printYOffset > DEFAULT_ZERO)
	{
		printYOffset--;
	}
	else if(reading && keystates[SDLK_DOWN] && printSprite->h > readerMaxY + printYOffset)
	{
		printYOffset++;
	}
	if(keystates[SDLK_a])
	{
		if(reading && !prevButtonStatus)
		{
			reading = false;
			justFinished = true;
		}
		else if(!reading && checkCollision(itemBox, mora.getCharacterBox()) && !prevButtonStatus)
		{
			reading = true;
		}
		currentButtonStatus = true;
	}
}

void Readable::render(SDL_Surface* dest)
{
	apply_surface(itemXPosition, itemYPosition, itemSprite, dest, NULL);
}

void Readable::renderTop(SDL_Surface* dest)
{
	if(reading)
	{
		apply_surface(readerXPosition, readerYPosition, readerSprite, dest, NULL);
		SDL_Rect printClips[1];
		SDL_Rect printCutout;
		printCutout.w = readerMaxX;
		printCutout.h = readerMaxY;
		printCutout.x = DEFAULT_ZERO;
		printCutout.y = printYOffset;
		printClips[DEFAULT_ZERO] = printCutout;
		apply_surface(printXPosition, printYPosition, printSprite, dest, &printClips[DEFAULT_ZERO]);
	}
}

void Readable::moraChanges(Mora* mora)
{
	if(reading)
	{
		mora->pauseMora();
	}
	else if(justFinished)
	{
		mora->unpauseMora();
	}
}

void Readable::monsterChanges(std::vector<Monsters*> mons)
{
	if(reading)
	{
		for each(Monsters* mon in mons)
		{
			mon->pauseMonster();
		}
	}
	else if(justFinished)
	{
		for each(Monsters* mon in mons)
		{
			mon->unpauseMonster();
		}
	}
}

bool Readable::isFinished()
{
	return !reading;
}