#include "TextBox.h"

std::string const TextBox::TEXT_BOX_FILENAME = "Art/TextBoxSpriteSheet.png";
std::string const TextBox::ASMAN_FONT_FILENAME = "Fonts/ASMAN.TTF";
std::string const TextBox::INTRIQUE_FONT_FILENAME = "Fonts/Intrique.ttf";

bool TextBox::init(int xP, int yP)
{
	//Create the text box.  Loading assets and fonts.
	TTF_Init();
	
	xPosition = xP;
	yPosition = yP;
	
	inputBox = load_image(TEXT_BOX_FILENAME);
	textToRender = NULL;
	
	enabled = false;
	
	font = TTF_OpenFont(ASMAN_FONT_FILENAME.c_str(), FONT_SIZE);
	
	instructionFont = TTF_OpenFont(ASMAN_FONT_FILENAME.c_str(), INSTRUCTION_FONT_SIZE);
	
	textColor.b = 0;
	textColor.g = 0;
	textColor.r = 0;
	
	text = "";
	textToRender = TTF_RenderText_Solid(font, text.c_str(), textColor);
	instructionString = "";
	instructionToRender = TTF_RenderText_Solid(instructionFont, instructionString.c_str(), textColor);

	return true;
}

void TextBox::setInstructionString(std::string instruction)
{
	instructionString = instruction;
	instructionToRender = TTF_RenderText_Solid(instructionFont, instructionString.c_str(), textColor);
}

void TextBox::cleanInstructionString()
{
	instructionString = "";
}

void TextBox::update(double delta)
{
	if(enabled)
	{
		SDL_PumpEvents();
		keystates = SDL_GetKeyState(NULL);

		if(keystates[SDLK_RETURN])
		{
			disable();
		}

		//Based off Lazy Foo's tutorial.
		while(SDL_PollEvent(&keyEvent))
        {
			if(keyEvent.type == SDL_KEYDOWN)
			{
				std::string previousString = text;
				//If the key is a space
				if(keyEvent.key.keysym.unicode == (Uint16)' ')
				{
					//Append the character
					text += (char)keyEvent.key.keysym.unicode;    
				}
				//If the key is a number
				else if((keyEvent.key.keysym.unicode >= (Uint16)'0') && (keyEvent.key.keysym.unicode <= (Uint16)'9' ) )
				{
					//Append the character
					text += (char)keyEvent.key.keysym.unicode;
				}
				//If the key is a uppercase letter
				else if( ( keyEvent.key.keysym.unicode >= (Uint16)'A' ) && ( keyEvent.key.keysym.unicode <= (Uint16)'Z' ) )
				{
					//Append the character
					text += (char)keyEvent.key.keysym.unicode;
				}
				//If the key is a lowercase letter
				else if( ( keyEvent.key.keysym.unicode >= (Uint16)'a' ) && ( keyEvent.key.keysym.unicode <= (Uint16)'z' ) )
				{
					//Append the character
					text += (char)keyEvent.key.keysym.unicode;
				}
				else if(keyEvent.key.keysym.unicode == (Uint16)'/' || keyEvent.key.keysym.unicode == (Uint16)'"' || keyEvent.key.keysym.unicode == (Uint16)'.')
				{
					//Append the character
					text += (char)keyEvent.key.keysym.unicode;
				}

				if( ( keyEvent.key.keysym.sym == SDLK_BACKSPACE ) && ( text.length() != 0 ) )
				{
					//Remove a character from the end
					text.erase(text.length() - 1 );
				}

				if(text != previousString)
				{        
					//Render a new text surface
					textToRender = TTF_RenderText_Solid(font, text.c_str(), textColor);
				}
			}
		}
	}
}

void TextBox::render(SDL_Surface* dest)
{
	//Draw the box and text.
	if(enabled)
	{
		apply_surface(xPosition, yPosition, inputBox, dest, NULL);
		apply_surface(xPosition + INSTRUCTION_X_OFFSET, yPosition + INSTRUCTION_Y_OFFSET, instructionToRender, dest, NULL);
		apply_surface(xPosition + TEXT_X_OFFSET, yPosition + TEXT_Y_OFFSET, textToRender, dest, NULL);
	}
}

void TextBox::cleanup()
{
	//Clean up the used memory.
	SDL_FreeSurface(inputBox);
	if(textToRender != nullptr)
	{
		SDL_FreeSurface(textToRender);
	}
	if(instructionToRender != nullptr)
	{
		SDL_FreeSurface(instructionToRender);
	}
	TTF_CloseFont(font);
	TTF_CloseFont(instructionFont);
	SDL_EnableUNICODE(SDL_DISABLE); 
}

void TextBox::enable()
{
	enabled = true;
	SDL_EnableUNICODE(SDL_ENABLE); 
}

void TextBox::disable()
{
	enabled = false;
	SDL_EnableUNICODE(SDL_DISABLE); 
}

void TextBox::cleanText()
{
	text = "";
	SDL_FreeSurface(textToRender);
	instructionString = "";
	SDL_FreeSurface(instructionToRender);
	textToRender = nullptr;
	instructionToRender = nullptr;
}

std::string TextBox::getText()
{
	if(!enabled)
	{
		return text;
	}
	else return "";
}