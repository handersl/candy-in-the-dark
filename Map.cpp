#include "Map.h"

#include "Global.h"

Map::Map()
{
}

void Map::setTileSheet(std::string tileSetString)
{
	tileSheet = load_image(tileSetString);
}

void Map::cleanup()
{
	SDL_FreeSurface(tileSheet);
	listOfLargeWalls.clear();
	listOfTiles.clear();
}

std::vector<Tile*> Map::getRedrawTiles(CollisionBox cBox)
{
	std::vector<Tile*> brokenTiles;

	for each(Tile* t in listOfTiles)
	{
		CollisionBox tileColli;
		tileColli.x = t->getXPosition();
		tileColli.y = t->getYPosition();
		tileColli.w = TILE_SIZE;
		tileColli.h = TILE_SIZE;

		if(checkCollision(cBox, tileColli))
		{
			brokenTiles.push_back(t);
		}
	}

	return brokenTiles;
}

//This function loops through the collision walls and makes as many large collision boxes
//as possible.  it gets rid of the smaller boxes it is made from.
std::vector<Collidable> Map::getConsolidatedWalls()
{
	std::vector<Collidable> walls;
	bool newStart = true;
	int numberInARow = DEFAULT_ZERO;
	int currentYPosition = DEFAULT_ZERO;
	int currentXPosition = DEFAULT_ZERO;
	int currentXStartPosition = DEFAULT_ZERO;

	for each(Tile* t in listOfTiles)
	{
		if(t->getTileType() == unwalkable)
		{
			if(t->getXPosition() == 111 && t->getYPosition() == 407)
			{
				int temp = 0;
			}
			if(newStart)
			{
				currentXStartPosition = t->getXPosition();
				newStart = false;
				numberInARow++;
				currentXPosition = t->getXPosition();
				currentYPosition = t->getYPosition();
			}
			else
			{
				if(t->getYPosition() == currentYPosition)
				{
					numberInARow++;
					currentXPosition += TILE_SIZE;
				}
				else
				{
					Collidable colli(currentXStartPosition, currentYPosition, TILE_SIZE * numberInARow, TILE_SIZE);
					walls.push_back(colli);

					currentXStartPosition = DEFAULT_ZERO;
					currentXPosition = DEFAULT_ZERO;
					currentXPosition += TILE_SIZE;
					currentYPosition += TILE_SIZE;
					numberInARow = DEFAULT_ZERO;
					numberInARow++;
				}
			}
		}
		else
		{
			if(!newStart)
			{
				Collidable colli(currentXStartPosition, currentYPosition, TILE_SIZE * numberInARow, TILE_SIZE);
				walls.push_back(colli);
				
				newStart = true;
				numberInARow = DEFAULT_ZERO;
			}

			currentXPosition += TILE_SIZE;

			if(currentXPosition > SCREEN_WIDTH)
			{
				currentXPosition = DEFAULT_ZERO;
				currentXStartPosition = DEFAULT_ZERO;
				currentYPosition += TILE_SIZE;
			}
		}
	}

	bool noChanges = true;

	do
	{
		noChanges = true;
		std::vector<Collidable>::iterator wallsIteratorOne;
		for(wallsIteratorOne = walls.begin(); wallsIteratorOne != walls.end(); wallsIteratorOne++)
		{
			std::vector<Collidable>::iterator wallsIteratorTwo;
			for(wallsIteratorTwo = walls.begin(); wallsIteratorTwo != walls.end(); wallsIteratorTwo++)
			{
				if(noChanges)
				{
					if(wallsIteratorOne->getCollisionBox().x == wallsIteratorTwo->getCollisionBox().x && wallsIteratorOne->getCollisionBox().w == wallsIteratorTwo->getCollisionBox().w && wallsIteratorOne->getCollisionBox().y + wallsIteratorOne->getCollisionBox().h == wallsIteratorTwo->getCollisionBox().y)
					{
						noChanges = false;
					
						wallsIteratorOne->resizeHeightBy(wallsIteratorTwo->getCollisionBox().h);

						walls.erase(wallsIteratorTwo);
						wallsIteratorOne = walls.end() - 1;
						wallsIteratorTwo = walls.end() - 1;
					}
					else if(wallsIteratorOne->getCollisionBox().x == wallsIteratorTwo->getCollisionBox().x && wallsIteratorOne->getCollisionBox().w == wallsIteratorTwo->getCollisionBox().w && wallsIteratorTwo->getCollisionBox().y + wallsIteratorTwo->getCollisionBox().h == wallsIteratorOne->getCollisionBox().y)
					{
						noChanges = false;

						wallsIteratorTwo->resizeHeightBy(wallsIteratorOne->getCollisionBox().h);

						walls.erase(wallsIteratorOne);
						wallsIteratorOne = walls.end() - 1;
						wallsIteratorTwo = walls.end() - 1;
					}
				}
			}
		}
	}while(!noChanges);
	listOfLargeWalls = walls;
	return listOfLargeWalls;
}

void Map::renderNewTiles(SDL_Surface* dest, std::vector<CollisionBox> listOfCollis)
{
	std::vector<Tile*> tilesNeedingRedrawing;

	for each(CollisionBox c in listOfCollis)
	{
		std::vector<Tile*> subRedrawnTiles = getRedrawTiles(c);
		tilesNeedingRedrawing.insert(tilesNeedingRedrawing.end(), subRedrawnTiles.begin(), subRedrawnTiles.end());
	}

	for each(Tile* t in tilesNeedingRedrawing)
	{
		SDL_Rect tileClip[1];
		tileClip[DEFAULT_ZERO].w= TILE_SIZE;
		tileClip[DEFAULT_ZERO].h= TILE_SIZE;
		tileClip[DEFAULT_ZERO].x= t->getSpriteXPosition();
		tileClip[DEFAULT_ZERO].y= t->getSpriteYPosition();
		apply_surface(t->getXPosition(), t->getYPosition(), tileSheet, dest, &tileClip[DEFAULT_ZERO]);
	}
}

void Map::drawAllTiles(SDL_Surface* dest)
{
	for each(Tile* t in listOfTiles)
	{
		SDL_Rect tileClip[1];
		SDL_Rect spriteSheetCutout;
		spriteSheetCutout.w= TILE_SIZE;
		spriteSheetCutout.h= TILE_SIZE;
		spriteSheetCutout.x= t->getSpriteXPosition();
		spriteSheetCutout.y= t->getSpriteYPosition();
		tileClip[DEFAULT_ZERO] = spriteSheetCutout;
		apply_surface((int)t->getXPosition(), (int)t->getYPosition(), tileSheet, dest, &tileClip[DEFAULT_ZERO]);
	}
}