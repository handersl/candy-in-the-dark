#pragma once
#ifndef DIALOGUE_H
#define DIALOGUE_H

#include "Items.h"
#include <string>

class Dialogue : public Items
{
public:
	//Create the dialogue box with neccesary sprites.
	Dialogue(int xPos, int yPos, std::string dialogueSprites, std::string dialogueCharacterSprite, std::string characterSpriteSheet = "");
	virtual bool init();
	//Clean up the space used.
	virtual void cleanup();
	//Update the item and check for collisions with Mora.
	virtual void update(double delta, Mora mora);
	//Draw what needs to be drawn.
	virtual void render(SDL_Surface* dest);
	virtual void renderTop(SDL_Surface* dest);
	//Apply changes to Mora if the item is activated.
	virtual void moraChanges(Mora* mora);
	//Apply changes to all the monsters in the current level if needed.
	virtual void monsterChanges(std::vector<Monsters*> mons);
	//Grab the collision box of the item.
	virtual CollisionBox getItemCollisionBox();
	virtual bool isFinished();
private:
	//There may or may not be a character that delivers the dialogue.
	SDL_Surface* characterSprite;
	//The list of dialogue images to be render in order of the vector.
	SDL_Surface* listOfDialogue;
	//The character sprite that will go in the dialogue box to denote who is talking.
	SDL_Surface* iconSprite;
	//the sprite for the frame of the dialogue.
	SDL_Surface* dialogueFrame;
	//The box for the item.
	CollisionBox dialogueBox;
	//The dialogue will be cut up into different sections by height differences of 200.
	std::vector<SDL_Rect> dialogueClips;
	//The keystates that control this item.
	Uint8* keystates;
	//Is the dialouge currently activated.
	bool active;

	//All dialogue without characters will only be usable once.
	bool deactivated;

	//Are we finished.
	bool finished;

	//Keep track of the button presses.
	bool currentButtonPress;
	bool prevButtonPress;

	//This is the current section of the dialogue the player is on.
	int currentDialogueSection;

	//The box that the dialogue will be displayed in.
	static const int DIALOGUE_FRAME_W = 999;
	static const int DIALOUGE_FRAME_H = 200;
	static const int DIALOGUE_FRAME_X = 0;
	static const int DIALOUGE_FRAME_Y = 568;

	//ADD IN THE SPRITE SHEET.
	static const std::string DIALOGUE_FRAME_SPRITE_SHEET;
};

#endif