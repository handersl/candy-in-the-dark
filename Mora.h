#pragma once
#ifndef MORA_H
#define MORA_H

#include <SDL.h>
#include <SDL_image.h>
#include <string>
#include <vector>
#include "Collidable.h"
#include "HPBar.h"
#include "MusicPlayer.h"

//The status of Mora.  This is used to find out which animations to play.
enum moraStatus
{
	idle, left, right, up, down
};

//This defines the attacks Mora can do.
enum moraAttacks
{
	attackAngel, teleportAngel
};

//To find which direction Mora is feared in.
enum moraFearStatus
{
	fearUp, fearDown, fearLeft, fearRight, fearNA
};

class Mora
{
public:
	//Default constructor.  Makes Mora at(0,0).
	Mora();
	//Makes Mora at (xPos,yPos).
	Mora(double xPos, double yPos);
	//Init Mora with the sprite sheets, bools, position etc.
	void setPosition(double x, double y);
	bool init();
	//Set the music player.
	void setMusicPlayer(MusicPlayer* musPlay)
	{ moraMusic = musPlay; }
	//Clean up the memory after Mora is gone.
	void cleanup();
	//Process the keys.
	void processEvent(Uint8* keystates, double delta);
	//Updates Mora.  Her position and status are updated here.
	void update(double delta);
	//Draw what needs to be drawn.  Draws Mora and her attacks.
	void render(SDL_Surface* dest);
	//This will update the frames of the animation.
	void updateFrames(double framerateTimer);
	//This allows Mora to change ghosts/attack methods.
	void ghostChanger(Uint8* keystates);
	//This checks if Mora sees an enemy and should be scared.
	void fearViewer(CollisionBox colli, int fearDamage);
	double getXPosition()
	{ return xPosition; }
	double getYPosition()
	{ return yPosition; }
	//Returns if Mora is attacking.
	bool getAttacking()
	{ return attacking; }
	//Sees if Mora's attack is hitting.
	bool getAttackActivated()
	{ return attackActivated; }
	//Returns the attack box.
	CollisionBox getAttackBox()
	{ return attackBox; }
	//Change the value of health down.
	void applyDamage(int dmg);
	//This gets the damage for the current attack.
	int getDamage();
	//Get the collision box representing Mora.
	CollisionBox getCharacterBox()
	{ return characterBox; }
	//Get the collision box of where we want to be.
	CollisionBox getFutureCharacterBox()
	{ return futureCharacterBox; }
	//Get the previous collision box of mora.
	CollisionBox getPreviousCharacterBox()
	{ return previousCharacterBox; }
	//This is the box where Mora will go to.
	CollisionBox getTeleportSpotBox()
	{ return teleportSpotBox; }
	//The old spot of the box.  This is used to clean up any
	//left over sprites.
	CollisionBox getPreviousTeleportSpotBox()
	{ return previousTeleportSpotBox; }
	int getGhostAreaX()
	{ return  GHOST_CHANGER_ICON_START_X;}
	int getGhostAreaY()
	{ return GHOST_CHANGER_ARROW_Y; }
	//Take a step.  Moves Mora where she wants to go.
	void step();
	//If we can not move where Mora wants to move we move back.
	void noStep();
	//Draw the UI.
	void drawMoraUI(SDL_Surface* dest);
	void pauseMora()
	{ pause = true; }
	void unpauseMora()
	{ pause = false; }
	//If Mora is paused we do not call update.
	bool getPause()
	{ return pause; }
	//Find out if the ghost selecter menu is on.
	bool isGhostChangerActivated()
	{ return ghostChangerActivated; }
	//Checks if Mora is dead.
	bool getDead()
	{ return dead; }
	//Call this to power up Mora.  Improving her stats.
	void powerUp();
	//Call this when the power up is done.
	void powerDown();
	//This resets the gameplay UI's when changing directions.
	void resetFacingDirection();
	//This tells you which direction Mora is facing based on the currentFrame being played.  Should
	//be called after updating frames.
	void findFacingSide();
private:
	//The spritesheets for Mora and any other related
	//spritesheets.
	SDL_Surface* moraSpriteSheet;
	SDL_Surface* moraPoweredUpSpriteSheet;
	SDL_Surface* attackAngelSpriteSheet;
	SDL_Surface* teleportAngelSpriteSheet;
	SDL_Surface* teleportAngelBlanketSpriteSheet;
	SDL_Surface* teleportCursorSpirteSheet;
	SDL_Surface* eyesightSpriteSheet;
	SDL_Surface* ghostChangerAttackIconSpriteSheet;
	SDL_Surface* ghostChangerTeleportIconSpriteSheet;
	SDL_Surface* ghostChangerArrow;
	SDL_Surface* damageIndicatorSpriteSheet;
	//Music player for mora.  Right now there is no music.
	MusicPlayer* moraMusic;
	//The healthbar object.
	HPBar healthBar;
	//Tha mana bars.
	HPBar manaBarOne;
	HPBar manaBarTwo;
	//A fear bar.
	HPBar fearBar;
	//Collision boxes for Mora and her attacks/moves.
	CollisionBox characterBox;
	CollisionBox futureCharacterBox;
	CollisionBox attackBox;
	CollisionBox teleportSpotBox;
	CollisionBox previousTeleportSpotBox;
	CollisionBox previousCharacterBox;
	CollisionBox eyesightBox;
	CollisionBox ghostArrowBox;
	CollisionBox attackAngelGhostBox;
	CollisionBox teleportAngelGhostBox;
	//Key states.
	Uint8* keystates;
	//Position.
	double xPosition;
	double yPosition;
	//Timers for events to occur.
	double mpBarTimer;
	double playFootstepTimer;
	double timeSinceLastFear;
	double updateFrameTimer;
	double playHeartbeatTimer;
	double showDamageIndicatorTimer;
	//Health.
	int healthNow;
	//Mana.
	int manaOneNow;
	int manaTwoNow;
	//Amount of sanity.
	int sanityNow;
	//Frames for all animations.
	int currentFrame;
	int attackFrame;
	//Limiting the frames.
	int frameIndex;
	//Current status and the previous one.
	moraStatus currentStatus;
	moraStatus previousStatus;
	//The fear status of Mora.
	moraFearStatus fearStatus;
	//Current attack.
	moraAttacks currentAttack1;
	moraAttacks currentAttack2;
	//This is to keep track of the attack for the ghostChanger.
	moraAttacks* ghostAttack;
	//Do we update or not.
	bool pause;
	//Are we running or attacking.
	bool running;
	bool attacking;
	bool teleporting;
	//When this bool is true we can grab the
	//attack box.
	bool attackActivated;
	bool calmButtonPressed;
	//Is Mora powered up.
	bool poweredUp;
	//Is the ghost changer activated.
	bool ghostChangerActivated;
	bool canChangeGhost;
	bool ghostButtons;
	//Did Mora get damaged.
	bool damaged;
	//Did Mora lose sanity.
	bool startled;
	//Is Mora dead;
	bool dead;
	//Constant data.  This includes frame data, damage, movement speed, etc.
	static double const MOVE_SPEED;
	static double const RUN_MODIFIER;
	static double const ATTACK_MODIFIER;
	static double const FOOT_STEP_WALK_RATE;
	static double const FOOT_STEP_RUN_RATE;
	static double const MORA_WALK_FRAMERATE_TIMER;
	static double const MORA_RUN_FRAMERATE_TIMER;
	static double const MORA_ABILITY_FRAMERATE_TIMER;
	static double const SHOW_DAMAGE_INDICATOR_TIMER;
	static int const RANDOM_FEAR_NUMBER = 150;
	static int const RAND_UP_VALUE = 25;
	static int const RAND_DOWN_VALUE = 50;
	static int const RAND_LEFT_VALUE = 75;
	static int const RAND_RIGHT_VALUE = 100;
	static int const AMOUNT_OF_CALMNESS = 5;
	static int const MIN_TIME_IN_BETWEEN_FEARS = 10;
	static int const MAX_HEARTBEAT_MULTIPLIER = 20;
	static int const ATTACK_ANGEL_DAMAGE = 1;
	static int const MORA_HEALTH = 100;
	static int const MORA_HEIGHT = 37;
	static int const MORA_WIDTH = 37;
	static int const MORA_ROW = 3;
	static int const MORA_COLUMN = 8;
	static int const MORA_CELLS = 24;
	static int const WALK_LEFT_START_FRAME = 0;
	static int const WALK_RIGHT_START_FRAME = 8;
	static int const WALK_UP_START_FRAME = 20;
	static int const WALK_DOWN_START_FRAME = 16;
	static int const WALK_LEFT_END_FRAME = 7;
	static int const WALK_RIGHT_END_FRAME = 15;
	static int const WALK_UP_END_FRAME = 23;
	static int const WALK_DOWN_END_FRAME = 19;
	SDL_Rect moraClip[MORA_ROW * MORA_COLUMN];
	static int const ATTACK_ANGEL_HEIGHT = 74;
	static int const ATTACK_ANGEL_WIDTH = 74;
	static int const ATTACK_ANGEL_ROW = 4;
	static int const ATTACK_ANGEL_COLUMN = 9;
	static int const ATTACK_ANGEL_CELLS = 36;
	static int const ATTACK_ANGEL_LEFT_START_FRAME = 0;
	static int const ATTACK_ANGEL_LEFT_END_FRAME = 8;
	static int const ATTACK_ANGEL_RIGHT_START_FRAME = 9;
	static int const ATTACK_ANGEL_RIGHT_END_FRAME = 17;
	static int const ATTACK_ANGEL_DOWN_START_FRAME = 18;
	static int const ATTACK_ANGEL_DOWN_END_FRAME = 26;
	static int const ATTACK_ANGEL_UP_START_FRAME = 27;
	static int const ATTACK_ANGEL_UP_END_FRAME = 35;
	static int const ATTACK_ANGEL_HORIZONTAL_ATTCK_WIDTH = 148;
	static int const ATTACK_ANGEL_HORIZONTAL_ATTCK_HEIGHT = 37;
	static int const ATTACK_ANGEL_VERTICAL_ATTCK_WIDTH = 37;
	static int const ATTACK_ANGEL_VERTICAL_ATTCK_HEIGHT = 148;
	SDL_Rect attackAngelClip[ATTACK_ANGEL_ROW * ATTACK_ANGEL_COLUMN];
	static int const TELEPORT_ANGEL_HEIGHT = 74;
	static int const TELEPORT_ANGEL_WIDTH = 37;
	static int const TELEPORT_ANGEL_ROW = 1;
	static int const TELEPORT_ANGEL_COLUMN = 8;
	static int const TELEPORT_ANGEL_CELLS = 8;
	static int const TELEPORT_ANGEL_START_FRAME = 0;
	static int const TELEPORT_ANGEL_END_FRAME = 7;
	SDL_Rect teleportAngelClip[TELEPORT_ANGEL_ROW * TELEPORT_ANGEL_COLUMN];
	static int const ATTACK_ANGEL_COST = 10;
	static int const TELEPORT_ANGEL_COST = 25;
	static int const GHOST_CHANGER_ICON_START_X = 435;
	//Add 74 for each new move.
	static int const GHOST_CHANGER_ICON_END_X = 509;
	static int const GHOST_CHANGER_ARROW_Y = 347;
	static int const GHOST_CHANGER_ICON_Y = 380;
	static int const GHOST_X_CHANGE = 74;

	static std::string const MORA_FILENAME;
	static std::string const MORA_POWER_UP_FILENAME;
	static std::string const GHOST_CHANGER_ARROW_FILENAME;
	static std::string const GHOST_CHANGER_ATTACK_FILENAME;
	static std::string const GHOST_CHANGER_TELEPORT_FILENAME;
	static std::string const ATTACK_ANGEL_FILENAME;

	static std::string const MORA_LEFT_EYESIGHT_ICON_FILENAME;
	static std::string const MORA_RIGHT_EYESIGHT_ICON_FILENAME;
	static std::string const MORA_UP_EYESIGHT_ICON_FILENAME;
	static std::string const MORA_DOWN_EYESIGHT_ICON_FILENAME;
	static std::string const ATTACK_ANGEL_ICON_FILENAME;
	static std::string const TELEPORT_ANGEL_ICON_FILENAME;
	static std::string const TELEPORT_ANGEL_BLANKET_FILENAME;
	static std::string const TELEPORT_ANGEL_FILENAME;
	static std::string const TELEPORT_CURSOR_FILENAME;
};

#endif