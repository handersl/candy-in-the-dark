#pragma once
#ifndef MONSTERS_H
#define MONSTERS_H

#include "Mora.h"
#include "PathFinding.h"
#include "HPBar.h"
#include "MusicPlayer.h"

//This is an abstract class for monsters.  All monsters must
//use this class.

//Tells which direction the monster is facing.
enum monsterStatus
{
	mIdle, mLeft, mRight, mUp, mDown
};

//This holds the all the different types of attacks
//monsters can have.  When we apply damage we will check
//what damage is being dealt.
enum monsterAttacks
{
	melee
};

class Monsters
{
public:
	//The initialization of the monster.
	virtual bool init() = 0;
	//Set the music player.
	virtual void setMusicPlayer(MusicPlayer* musPlay) = 0;
	//The clean up to make sure there are no leaks.
	virtual void cleanup() = 0;
	//Update the monsters actions per frame.
	virtual void update(double delta, Mora m) = 0;
	//Draw the monster.
	virtual void render(SDL_Surface* dest) = 0;
	//Return the collision box of the monster.
	virtual CollisionBox getMonsterBox() = 0;
	//Return the attack box of the monster.
	virtual CollisionBox getAttackbox() = 0;
	//Creates the pathfinding map for the monster so that
	//it can navigate through the map.
	virtual void makeMap(int widthLevel, int heightLevel, std::vector<Collidable> listOfCollidables) = 0;
	//This applies damage taken.
	virtual void applyDamage(int dmg) = 0;
	//Get the damage of the attack.
	virtual int getDamage() = 0;
	//Get the amount of fear it will cause;
	virtual int getFearDamage() = 0;
	//Checks if the monster is attacking.
	virtual bool getAttacking() = 0;
	//Checks if it is time to check the attackBox.
	virtual bool getAttackActivated() = 0;
	//Checks if the monster is dead.
	virtual bool getDead() = 0;
	//Is this monster paused.
	virtual bool getPause() = 0;
	virtual void pauseMonster() = 0;
	virtual void unpauseMonster() = 0; 
};

#endif 