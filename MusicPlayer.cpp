#include "MusicPlayer.h"

std::string const MusicPlayer::CANDYINTHEDARK_SOUND_FILENAME = "Sound/CandyInTheDark.wav";
std::string const MusicPlayer::FOOTSTEP_SOUND_FILENAME = "Sound/FootstepSound.wav";
std::string const MusicPlayer::STAB_SOUND_FILENAME = "Sound/SlicingFleshSound.wav";
std::string const MusicPlayer::CHATTER_SOUND_FILENAME = "Sound/CrunchingSound.wav";
std::string const MusicPlayer::TELEPORT_SOUND_FILENAME = "Sound/TeleportSound.wav";
std::string const MusicPlayer::SELECT_SOUND_FILENAME = "Sound/SelectSound.wav";
std::string const MusicPlayer::MONSTER_SCREAM_FILENAME = "Sound/MonsterScreamSound.wav";
std::string const MusicPlayer::WOODCREAK_FILENAME = "Sound/WoodCreakSound.wav";
std::string const MusicPlayer::PANIC_SOUND_FILENAME = "Sound/ScreamSound.wav";
std::string const MusicPlayer::HEART_BEAT_FILENAME = "Sound/HeartbeatSound.wav";

bool MusicPlayer::init()
{
	//Create the audio player.
	if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
    {
        return false;    
    }
	else
	{
		Mix_AllocateChannels(NUM_SOUND_CHANNELS);
		return true;
	}
}

//Load all the music files.
bool MusicPlayer::loadFiles()
{
	footSteps = Mix_LoadWAV(FOOTSTEP_SOUND_FILENAME.c_str());
	Mix_VolumeChunk(footSteps, FOOTSTEP_VOLUME);

	stab = Mix_LoadWAV(STAB_SOUND_FILENAME.c_str());
	Mix_VolumeChunk(stab, STAB_VOLUME);

	chatter = Mix_LoadWAV(CHATTER_SOUND_FILENAME.c_str());
	Mix_VolumeChunk(chatter, CHATTER_VOLUME);

	teleport = Mix_LoadWAV(TELEPORT_SOUND_FILENAME.c_str());
	Mix_VolumeChunk(teleport, TELEPORT_VOLUME);

	select = Mix_LoadWAV(SELECT_SOUND_FILENAME.c_str());
	Mix_VolumeChunk(select, SELECT_VOLUME);

	monsterScream = Mix_LoadWAV(MONSTER_SCREAM_FILENAME.c_str());
	Mix_VolumeChunk(monsterScream, MONSTER_SCREAM_VOLUME);

	woodCreak = Mix_LoadWAV(WOODCREAK_FILENAME.c_str());
	Mix_VolumeChunk(woodCreak, WOODCREAK_VOLUME);

	panic = Mix_LoadWAV(PANIC_SOUND_FILENAME.c_str());
	Mix_VolumeChunk(panic, PANIC_VOLUME);

	heartbeat = Mix_LoadWAV(HEART_BEAT_FILENAME.c_str());
	Mix_VolumeChunk(heartbeat, HEARTBEAT_VOLUME);

	candyInTheDark = Mix_LoadMUS(CANDYINTHEDARK_SOUND_FILENAME.c_str());
	Mix_VolumeMusic(MUSIC_VOLUME);

	return true;
}

//Clean up the music loaded.
void MusicPlayer::cleanup()
{
	Mix_FreeChunk(footSteps);
	Mix_FreeChunk(stab);
	Mix_FreeChunk(chatter);
	Mix_FreeChunk(teleport);
	Mix_FreeChunk(select);
	Mix_FreeChunk(monsterScream);
	Mix_FreeChunk(woodCreak);
	Mix_FreeChunk(panic);
	Mix_FreeChunk(heartbeat);

	Mix_FreeMusic(candyInTheDark);

	Mix_CloseAudio();
}

//Play or halt the music as nessesary.
void MusicPlayer::playFootSteps()
{
	//Play the effect only once one the next avalible channel.
	Mix_PlayChannel(FOOTSTEP_SOUNDEFFECT_CHANNEL, footSteps, DEFAULT_ZERO );
}

void MusicPlayer::playStab()
{
	Mix_PlayChannel(STAB_SOUNDEFFECT_CHANNEL, stab, DEFAULT_ZERO );
}

void MusicPlayer::playTeethChatter()
{
	Mix_PlayChannel(CHATTER_SOUNDEFFECT_CHANNEL, chatter, DEFAULT_ZERO );
}

void MusicPlayer::playTeleport()
{
	Mix_PlayChannel(TELEPORT_SOUNDEFFECT_CHANNEL, teleport, DEFAULT_ZERO );
}

void MusicPlayer::playSelect()
{
	Mix_PlayChannel(SELECT_SOUNDEFFECT_CHANNEL, select, DEFAULT_ZERO );
}

void MusicPlayer::playScream()
{
	Mix_PlayChannel(MONSTER_SCREAM_SOUNDEFFECT_CHANNEL, monsterScream, DEFAULT_ZERO );
}

void MusicPlayer::playWoodCreak(int dist)
{
	//Play sound according to a distance.
	Mix_VolumeChunk(woodCreak, WOODCREAK_VOLUME*(TILE_SIZE/(dist+.0001)));
	Mix_PlayChannel(WOODCREAK_SOUNDEFFECT_CHANNEL, woodCreak, DEFAULT_ZERO );
}

void MusicPlayer::playPanic()
{
	Mix_PlayChannel(PANIC_SOUND_CHANNEL, panic, DEFAULT_ZERO );
}

void MusicPlayer::playHearbeat()
{
	Mix_PlayChannel(HEART_BEAT_CHANNEL, heartbeat, DEFAULT_ZERO );
}

void MusicPlayer::playCandyInTheDark()
{
	//Loop forever.
	Mix_PlayMusic(candyInTheDark, -1);
}

void MusicPlayer::stopCandyInTheDark()
{
	//Pause the music.
	Mix_PauseMusic();
}