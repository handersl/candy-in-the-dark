#include "Menu.h"
#include "Global.h"

	const std::string Menu::MENU_BACKGROUND_SPRITESHEET = "Art/MenuScreenSpriteSheet.png";
	const std::string Menu::MENU_POINTER_SPRITESHEET = "Art/MenuArrowSpriteSheet.png";

Menu::Menu()
{
}

bool Menu::init()
{
	menuBackgroundSpriteSheet = load_image(MENU_BACKGROUND_SPRITESHEET);
	menuPointerSpriteSheet = load_image(MENU_POINTER_SPRITESHEET);
	
	buttonX = START_BUTTON_X;
	buttonY = START_BUTTON_Y;
	
	currentPointerStatus = MENU_START;
	
	pointerStatusToBeReturned = NA_VALUE;

	currentButtonState = false;
	prevButtonState = false;

	reset = false;

	pause = false;

	return true;
}

void Menu::update()
{
	if(reset)
	{
		pointerStatusToBeReturned = NA_VALUE;
		reset = false;
	}

	if(!pause)
	{
		prevButtonState = currentButtonState;
		currentButtonState = false;
		//Grab key events and update the state of the menu.
		SDL_PumpEvents();
		keystates = SDL_GetKeyState(NULL);
		updateCurrentPointerStatus(keystates);

	}
}

void Menu::updateCurrentPointerStatus(Uint8* keys)
{
	//Move the pointer according to the users specification.
	if(keys[SDLK_UP])
	{
		if(!prevButtonState)
		{
			currentPointerStatus--;
			menuMusicPlayer->playSelect();
		}
		currentButtonState = true;
	}
	else if(keys[SDLK_DOWN])
	{
		if(!prevButtonState)
		{
			currentPointerStatus++;
			menuMusicPlayer->playSelect();
		}
		currentButtonState = true;
	}
	//Once the user confirms their selection we update the status
	//to be returned.
	else if(keys[SDLK_z])
	{
		pointerStatusToBeReturned = currentPointerStatus;
		menuMusicPlayer->playSelect();
		reset = true;
	}

	//Bounds checking.
	if(currentPointerStatus < MENU_START)
	{
		currentPointerStatus = MENU_QUIT;
	}
	else if(currentPointerStatus > MENU_QUIT)
	{
		currentPointerStatus = MENU_START;
	}

	//After we figure out what status the pointer is we
	//update the position to the right place.
	if(currentPointerStatus == MENU_START)
	{
		buttonY = START_BUTTON_Y;
	}
	else if(currentPointerStatus == MENU_LEVEL_EDITOR)
	{
		buttonY = LEVELEDITOR_BUTTON_Y;
	}
	else if(currentPointerStatus == MENU_QUIT)
	{
		buttonY = QUIT_BUTTON_Y;
	}
}

void Menu::render(SDL_Surface* dest)
{
	if(!pause)
	{
		//Draw the background and the pointer in the right places.
		apply_surface(DEFAULT_ZERO, DEFAULT_ZERO, menuBackgroundSpriteSheet, dest, NULL);
		apply_surface(buttonX, buttonY, menuPointerSpriteSheet, dest, NULL);
	}
}

int Menu::getStatus()
{
	return pointerStatusToBeReturned;
}

void Menu::cleanup()
{
	SDL_FreeSurface(menuBackgroundSpriteSheet);
	SDL_FreeSurface(menuPointerSpriteSheet);
}