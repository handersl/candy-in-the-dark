#include "Clickable.h"

#include "Global.h"

//More comments in "Clickable.h"

Clickable::Clickable()
{
	clickableIcon = nullptr;
	tile = nullptr;
}

Clickable::Clickable(int xPos, int yPos, int width, int height, LevelEditorTiles* let, clickableType clickType)
{
	tileBox.x = xPos;
	tileBox.y = yPos;
	tileBox.w = width;
	tileBox.h = height;
	tile = let;
	type = clickType;
	clickableIcon = nullptr;
}

void Clickable::setClickableIcon(std::string clickableIconFilename)
{
	clickableIcon = load_image(clickableIconFilename);
}

void Clickable::render(SDL_Surface* dest)
{
	if(tile != nullptr)
	{
		tile->render(dest);
	}
	else if(clickableIcon != nullptr)
	{
		apply_surface(tileBox.x, tileBox.y, clickableIcon, dest, NULL);
	}
}

void Clickable::cleanup()
{
	if(tile != nullptr)
	{
		tile->cleanup();
		delete tile;
		tile = nullptr;
	}
	if(clickableIcon != nullptr)
	{
		SDL_FreeSurface(clickableIcon);
		clickableIcon = nullptr;
	}
}
void Clickable::cleanTile()
{
	if(tile != nullptr)
	{
		tile->cleanup();
		delete tile;
		tile = nullptr;
	}
}


SDL_Rect Clickable::getBox()
{
	return tileBox;
}

LevelEditorTiles Clickable::getTile()
{
	return *tile;
}

bool Clickable::isTileNull()
{
	if(tile == nullptr)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Clickable::setTileReaderString(std::string readerString)
{
	tile->setReaderPrintString(readerString);
}

void Clickable::setTilesNextLevelString(std::string nlString)
{
	tile->setNextLevelString(nlString);
}

void Clickable::setTileDialogueString(std::string diaString)
{
	tile->setDialogueString(diaString);
}

void Clickable::setTileDialogueIcon(std::string iconStr)
{
	tile->setDialogueIconString(iconStr);
}

void Clickable::setTileDialogueCharacter(std::string diaCharString)
{
	tile->setDialogueCharacterString(diaCharString);
}