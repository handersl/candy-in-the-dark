#pragma once
#ifndef MAP_H
#define MAP_H

#include <SDL.h>
#include <vector>
#include <string>
#include "Tile.h"
#include "Collidable.h"

//The map holds the tiles and collision for the level.

class Map
{
public:
	Map();
	//Set the sprite sheet to be used in this level.
	void setTileSheet(std::string tileSetString);
	//Clean up the map/level.
	void cleanup();
	//This function gets inputed a list of collision boxes.  It then draws the tiles that the
	//list of collision boxes touches on the specified surface.
	void renderNewTiles(SDL_Surface* dest,  std::vector<CollisionBox> listOfCollis);
	//This draws all tiles.
	void drawAllTiles(SDL_Surface* dest);
	//This adds a new tile to the map.
	void addTile(Tile* tile)
	{listOfTiles.push_back(tile);}
	std::vector<Tile*> getTiles()
	{return listOfTiles;}
	std::vector<Collidable> getLargeWalls()
	{ return listOfLargeWalls; }
	//The redraws the tile around the box.
	std::vector<Tile*> getRedrawTiles(CollisionBox cBox);
	//This merges the collision boxes into squares and rectangles.
	//We use this so that we can check collisions with as few boxes as possible.
	std::vector<Collidable> getConsolidatedWalls();
private:
	SDL_Surface* tileSheet;
	std::vector<Collidable> listOfLargeWalls;
	std::vector<Tile*> listOfTiles;
};

#endif