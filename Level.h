#pragma once
#ifndef LEVEL_H
#define LEVEL_H

#include <SDL.h>
#include <vector>
#include <string>
#include "Mora.h"
#include "Collidable.h"
#include "Monsters.h"
#include "Readable.h"
#include "Map.h"

typedef struct
{
	CollisionBox nextLevelBox;
	std::string nextLevelString;
} NextLevelTile;

class Level
{
public:
	Level();
	bool init(std::string tileSheet);
	void makeMapForMonster(std::vector<Collidable> listOfCollidables, int widthLevel = SCREEN_HEIGHT, int heightLevel = SCREEN_HEIGHT);
	void cleanup();
	//This cleans up everything but Mora.  This is so that we can keep Mora's
	//stats throughout the game.
	void softCleanup();
	void reset();
	void update(double delta);
	void render(SDL_Surface* dest);
	//Returns the collisions that make up the walls.  Walls should
	//follow the tiles and be placed in the right places and be the
	//right size.
	std::vector<Collidable> getCollisionBoxes()
	{ return listOfWalls; }
	//This places the monsters onto the map.
	std::vector<Monsters*> getMonsters()
	{ return listOfMonsters; }
	//return all items besides readables.
	std::vector<Items*> getItems()
	{ return listOfItems; }
	Map getMap()
	{ return levelMap; }
	std::string getNextLevelString()
	{ return nextLevelName; }
	Mora getMora()
	{ return mora; }
	//This adds Mora.
	void addMora(Mora mora);
	//This adds a monster to the monster list.
	void addMonster(Monsters* mon);
	//This adds a collidable.
	void addCollidable(Collidable colli);
	//This adds a tile to the map.
	void addTileToMap(Tile* t);
	//This adds a new readable object.
	void addReadable(Readable* read);
	//This holds the common items.
	void addItems(Items* newItem);
	//This adds a next level tile.
	void addNextLevelTile(NextLevelTile nlt);
	//Set the position of Mora in the game.
	void setMoraPosition(double x, double y);
	//Get the place Mora should go to.  Place Mora near, but not on top of,
	//where the door should be(The designer needs to place this door. The engine
	//figures out where to place Mora but will not place the door).
	CollisionBox getMoraEntryBox(NextLevelTile ntl);
	void clearNextLevelName();
private:
	SDL_Surface* blackoutSpriteSheet;
	Mora mora;
	Map levelMap;
	std::vector<Monsters*> listOfMonsters;
	std::vector<Collidable> listOfWalls;
	std::vector<Items*> listOfItems;
	std::vector<NextLevelTile> listOfNextLevels;
	std::string nextLevelName;

	bool firstRun;
};

#endif