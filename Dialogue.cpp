#include "Dialogue.h"

const std::string Dialogue::DIALOGUE_FRAME_SPRITE_SHEET = "Art/DialogueBoxSpriteSheet.png";

//Create the dialogue box with neccesary sprites.
Dialogue::Dialogue(int xPos, int yPos, std::string dialogueSprites, std::string dialogueCharacterSprite, std::string charSpriteSheet)
{
	listOfDialogue = load_image(dialogueSprites);
	iconSprite = load_image(dialogueCharacterSprite);
	if(charSpriteSheet != "")
	{
		characterSprite = load_image(charSpriteSheet);
	}
	else
	{
		characterSprite = nullptr;
	}

	dialogueFrame = load_image(DIALOGUE_FRAME_SPRITE_SHEET);

	dialogueBox.w = TILE_SIZE;
	dialogueBox.h = TILE_SIZE;
	dialogueBox.x = xPos;
	dialogueBox.y = yPos;

	active = false;
	deactivated = false;
	finished = false;
	currentButtonPress = false;
	prevButtonPress = false;

	currentDialogueSection = DEFAULT_ZERO;

	//Cut up the Dialouge image into parts to show.
	int xCutUpPos = DEFAULT_ZERO;
	int yCutUpPos = DEFAULT_ZERO;
	while(yCutUpPos < listOfDialogue->h)
	{
		SDL_Rect newClip;
		newClip.w = DIALOGUE_FRAME_W;
		newClip.h = DIALOUGE_FRAME_H;
		newClip.x = DEFAULT_ZERO;
		newClip.y = yCutUpPos;
		dialogueClips.push_back(newClip);

		yCutUpPos += DIALOUGE_FRAME_H;
	}
}
bool Dialogue::init()
{
	return true;
}

//Clean up the space used.
void Dialogue::cleanup()
{
	SDL_FreeSurface(characterSprite);
	SDL_FreeSurface(listOfDialogue);
	SDL_FreeSurface(iconSprite);
}

//Update the item and check for collisions with Mora.
void Dialogue::update(double delta, Mora mora)
{
	prevButtonPress = currentButtonPress;
	currentButtonPress = false;

	//We do not want to send false message twice.
	if(currentDialogueSection == DEFAULT_ZERO && finished)
	{
		finished = false;
	}

	SDL_PumpEvents();
	keystates = SDL_GetKeyState(NULL);

	if(checkCollision(mora.getCharacterBox(), dialogueBox) && !deactivated)
	{
		if(characterSprite == nullptr && !active)
		{
			active = true;
			finished = false;
		}
		else if(keystates[SDLK_a])
		{
			if(!active && !prevButtonPress)
			{
				active = true;
				finished = false;

			}
			else if(active && !prevButtonPress)
			{
				currentDialogueSection++;
				if(currentDialogueSection >= dialogueClips.size())
				{
					if(characterSprite == nullptr)
					{
						deactivated = true;
					}
					finished = true;
					active = false;
					currentDialogueSection = DEFAULT_ZERO;
				}
			}
			currentButtonPress = true;
		}
	}
}

//Draw what needs to be drawn.
void Dialogue::render(SDL_Surface* dest)
{
	if(characterSprite != nullptr)
	{
		apply_surface((int)dialogueBox.x, (int)dialogueBox.y, characterSprite, dest, NULL);
	}
}

void Dialogue::renderTop(SDL_Surface* dest)
{
	if(active)
	{
		if(currentDialogueSection < dialogueClips.size())
		{
			apply_surface(DIALOGUE_FRAME_X, DIALOUGE_FRAME_Y, dialogueFrame, dest, NULL);
			//Normally this would not need to be donw but because we do not know how large the
			//dialogue text image will be we can not use an array.
			SDL_Rect* currentClip = new SDL_Rect();
			currentClip->w = dialogueClips[currentDialogueSection].w;
			currentClip->h = dialogueClips[currentDialogueSection].h;
			currentClip->x = dialogueClips[currentDialogueSection].x;
			currentClip->y = dialogueClips[currentDialogueSection].y;
			apply_surface(DIALOGUE_FRAME_X, DIALOUGE_FRAME_Y, listOfDialogue, dest, currentClip);
			apply_surface(DIALOGUE_FRAME_X, DIALOUGE_FRAME_Y, iconSprite, dest, NULL);
		}
	}
}

//Apply changes to Mora if the item is activated.
void Dialogue::moraChanges(Mora* mora)
{
	if(active)
	{
		mora->pauseMora();
	}
	else if(finished)
	{
		mora->unpauseMora();
	}
}

//Apply changes to all the monsters in the current level if needed.
void Dialogue::monsterChanges(std::vector<Monsters*> mons)
{
	if(active)
	{
		for each(Monsters* m in mons)
		{
			m->pauseMonster();
		}
	}
	else if(finished)
	{
		for each(Monsters* m in mons)
		{
			m->unpauseMonster();
		}
	}
}

//Grab the collision box of the item.
CollisionBox Dialogue::getItemCollisionBox()
{
	return dialogueBox;
}

bool Dialogue::isFinished()
{
	return finished;
}