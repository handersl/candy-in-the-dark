#include "PowerUp.h"
#include "Global.h"

bool PowerUp::init()
{
	powerUpSprite = load_image(POWER_UP_FILENAME);
	powerUpBox.x = DEFAULT_ZERO;
	powerUpBox.y = DEFAULT_ZERO;
	powerUpBox.w = TILE_SIZE;
	powerUpBox.h = TILE_SIZE;
	timer = START_TIME;
	activated = false;
	firstStart = false;
	firstFinish = false;

	return true;
}

bool PowerUp::init(double xPos, double yPos)
{
	//Load the power up image.
	powerUpSprite = load_image(POWER_UP_FILENAME);
	//Create a collision box of tile size ant at the correct position.
	powerUpBox.x = xPos;
	powerUpBox.y = yPos;
	powerUpBox.w = TILE_SIZE;
	powerUpBox.h = TILE_SIZE;
	//Set the timer to the max value.
	timer = START_TIME;
	//Nothing is activated or finished yet.
	activated = false;
	firstStart = false;
	firstFinish = false;

	return true;
}

void PowerUp::cleanup()
{
	//Clean up the image.
	SDL_FreeSurface(powerUpSprite);
}

void PowerUp::update(double delta, Mora mora)
{
	//If we are activated we will start to count down.
	if(activated)
	{
		timer -= .001 * delta;
	}

	//Check if mora hits the power up or not.
	if(checkCollision(powerUpBox, mora.getCharacterBox()) && timer == START_TIME)
	{
		//If she did we activate and start the timer and move the powerUp to a
		//place off the screen.
		activated = true;
		firstStart = true;
		powerUpBox.x = NA_VALUE;
		powerUpBox.y = NA_VALUE;
	}

	//If we already sent the finished message once we do not 
	//want to send it again.
	if(firstFinish)
	{
		firstFinish = false;
	}

	//If the timer is done we deactivate the power up for Mora.
	if(timer <= DEFAULT_ZERO && firstStart)
	{
		activated = false;
		firstFinish = true;
		firstStart = false;
	}
}

void PowerUp::render(SDL_Surface* dest)
{
	//If the power up is not yet picked up we draw it.
	if(!activated)
	{
		apply_surface(powerUpBox.x, powerUpBox.y, powerUpSprite, dest, NULL);
	}
}

void PowerUp::renderTop(SDL_Surface* dest)
{
	//Has no overlay component.
}

void PowerUp::moraChanges(Mora* mora)
{
	//Call Mora's power up function to power her up.
	//Call Mora's power down function to power her down.
	if(activated && firstStart)
	{
		mora->powerUp();
	}
	else if(timer <= DEFAULT_ZERO && firstFinish)
	{
		mora->powerDown();
	}
}

void PowerUp::monsterChanges(std::vector<Monsters*> mons)
{
	//Do nothing.  This item only affects Mora.
}

CollisionBox PowerUp::getItemCollisionBox()
{
	return powerUpBox;
}

bool PowerUp::isFinished()
{
	return firstFinish;
}