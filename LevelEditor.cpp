#include "LevelEditor.h"

#include <cmath>
#include <fstream>
#include <iostream>
#include "Global.h"

std::string const LevelEditor::EMPTY_TILE_FILENAME = "Art/EmptyTileSpriteSheet.png";
std::string const LevelEditor::UP_ARROW_ICON_FILENAME = "Art/UpArrowSpriteSheetBobaGame.png";
std::string const LevelEditor::DOWN_ARROW_ICON_FILENAME = "Art/DownArrowSpriteSheetBobaGame.png";
std::string const LevelEditor::LEFT_ARROW_ICON_FILENAME = "Art/LeftArrowSpriteSheetBobaGame.png";
std::string const LevelEditor::RIGHT_ARROW_ICON_FILENAME = "Art/RightArrowSpriteSheetBobaGame.png";
std::string const LevelEditor::SAVE_ICON_FILENAME = "Art/SaveIconSpriteSheet.png";

LevelEditor::LevelEditor()
{
}

void LevelEditor::init()
{
	//This creates the level editors selectable icons on the left hand side.
	//These tiles can be clicked and placed on the level editor's map.

	leftMouseNow = false;
	leftMousePast = false;
	redraw = false;
	currentMouseQuad = NAQuad;
	
	gridArea = Clickable(MORA_AREA_WIDTH, DEFAULT_ZERO, SCREEN_WIDTH, SCREEN_HEIGHT, nullptr, tiled);
	moraArea = Clickable(DEFAULT_ZERO, DEFAULT_ZERO, MORA_AREA_WIDTH, MORA_AREA_HEIGHT, nullptr, tiled);
	monsterArea = Clickable(DEFAULT_ZERO, MORA_AREA_HEIGHT, MONSTER_AREA_WIDTH, MONSTER_AREA_HEIGHT, nullptr, tiled);
	itemArea = Clickable(DEFAULT_ZERO, MORA_AREA_HEIGHT + MONSTER_AREA_HEIGHT, ITEM_AREA_WIDTH, ITEM_AREA_HEIGHT, nullptr, tiled);
	groundTileArea = Clickable(DEFAULT_ZERO, MORA_AREA_HEIGHT + MONSTER_AREA_HEIGHT + ITEM_AREA_HEIGHT, GROUND_TILE_AREA_WIDTH, GROUND_TILE_AREA_HEIGHT, nullptr, tiled);
	saveArea = Clickable(DEFAULT_ZERO, MORA_AREA_HEIGHT + MONSTER_AREA_HEIGHT + ITEM_AREA_HEIGHT + GROUND_TILE_AREA_HEIGHT, SAVE_AREA_WIDTH, SAVE_AREA_HEIGHT, nullptr, tiled);

	LevelEditorTiles* mora = new LevelEditorTiles(levelEditorTileType::mora);
	mora->setPosition(MORA_ICON_X, MORA_ICON_Y);
	moraIcon = Clickable(mora->getXPosition(), mora->getYPosition(), TILE_SIZE, TILE_SIZE, mora, selectable);

	moraTile = Clickable(NA_VALUE, NA_VALUE, TILE_SIZE, TILE_SIZE, nullptr, selectable);

	monsterLeft = Clickable(MONSTER_LEFT_X, MONSTER_LEFT_Y, TILE_SIZE, TILE_SIZE, nullptr, selectable);
	monsterLeft.setClickableIcon(LEFT_ARROW_ICON_FILENAME);
	monsterRight = Clickable(MONSTER_RIGHT_X, MONSTER_RIGHT_Y, TILE_SIZE, TILE_SIZE, nullptr, selectable);
	monsterRight.setClickableIcon(RIGHT_ARROW_ICON_FILENAME);
	LevelEditorTiles* bbabyMonster = new LevelEditorTiles(monster);
	bbabyMonster->setPosition(MONSTER_ICON_X, MONSTER_ICON_Y);
	bbabyMonster->setMonsterType(Bbaby);
	Clickable bbabYMonsterClickable = Clickable(MONSTER_ICON_X, MONSTER_ICON_Y, TILE_SIZE, TILE_SIZE, bbabyMonster, selectable);
	monsterIcon.push_back(bbabYMonsterClickable);
	currentMonster = DEFAULT_ZERO;
	
	itemUp = Clickable(ITEM_UP_X, ITEM_UP_Y, TILE_SIZE, TILE_SIZE, nullptr, selectable);
	itemUp.setClickableIcon(UP_ARROW_ICON_FILENAME);
	itemDown = Clickable(ITEM_DOWN_X, ITEM_DOWN_Y, TILE_SIZE, TILE_SIZE, nullptr, selectable);
	itemDown.setClickableIcon(DOWN_ARROW_ICON_FILENAME);
	itemLeft = Clickable(ITEM_LEFT_X, ITEM_LEFT_Y, TILE_SIZE, TILE_SIZE, nullptr, selectable);
	itemLeft.setClickableIcon(LEFT_ARROW_ICON_FILENAME);
	itemRight = Clickable(ITEM_RIGHT_X, ITEM_RIGHT_Y, TILE_SIZE, TILE_SIZE, nullptr, selectable);
	itemRight.setClickableIcon(RIGHT_ARROW_ICON_FILENAME);
	LevelEditorTiles* readerItem = new LevelEditorTiles(item);
	readerItem->setPosition(ITEM_ICON_X, ITEM_ICON_Y);
	readerItem->setItemType(reader);
	readerItem->setReaderTileType(clipboardReaderType);
	Clickable readerItemClickable = Clickable(ITEM_ICON_X, ITEM_ICON_Y, TILE_SIZE, TILE_SIZE, readerItem, selectable);
	itemIcon.push_back(readerItemClickable);
	LevelEditorTiles* dialogueItem = new LevelEditorTiles(item);
	dialogueItem->setPosition(ITEM_ICON_X, ITEM_ICON_Y);
	dialogueItem->setItemType(dialogue);
	Clickable dialogueItemClickable = Clickable(ITEM_ICON_X, ITEM_ICON_Y, TILE_SIZE, TILE_SIZE, dialogueItem, selectable);
	itemIcon.push_back(dialogueItemClickable);
	LevelEditorTiles* powerUpItem = new LevelEditorTiles(item);
	powerUpItem->setPosition(ITEM_ICON_X, ITEM_ICON_Y);
	powerUpItem->setItemType(powerUp);
	Clickable powerUpItemClickable = Clickable(ITEM_ICON_X, ITEM_ICON_Y, TILE_SIZE, TILE_SIZE, powerUpItem, selectable);
	itemIcon.push_back(powerUpItemClickable);
	currentItemX = DEFAULT_ZERO;
	currentItemY = DEFAULT_ZERO;
	
	LevelEditorTiles* groundTile00 = new LevelEditorTiles(ground);
	groundTile00->setPosition(GROUND_TILE_ROW_ONE_X, GROUND_TILE_ROW_ONE_Y);
	groundTile00->setGroundTile("00");
	Clickable groundTile00Clickable = Clickable(GROUND_TILE_ROW_ONE_X, GROUND_TILE_ROW_ONE_Y, TILE_SIZE, TILE_SIZE, groundTile00, selectable);
	levelSpriteSheet.push_back(groundTile00Clickable);
	LevelEditorTiles* groundTile01 = new LevelEditorTiles(ground);
	groundTile01->setPosition(GROUND_TILE_ROW_ONE_X + TILE_SIZE, GROUND_TILE_ROW_ONE_Y);
	groundTile01->setGroundTile("01");
	Clickable groundTile01Clickable = Clickable(GROUND_TILE_ROW_ONE_X + TILE_SIZE, GROUND_TILE_ROW_ONE_Y, TILE_SIZE, TILE_SIZE, groundTile01, selectable);
	levelSpriteSheet.push_back(groundTile01Clickable);
	LevelEditorTiles* groundTile02 = new LevelEditorTiles(ground);
	groundTile02->setPosition(GROUND_TILE_ROW_ONE_X + TILE_SIZE + TILE_SIZE, GROUND_TILE_ROW_ONE_Y);
	groundTile02->setGroundTile("02");
	Clickable groundTile02Clickable = Clickable(GROUND_TILE_ROW_ONE_X + TILE_SIZE + TILE_SIZE, GROUND_TILE_ROW_ONE_Y, TILE_SIZE, TILE_SIZE, groundTile02, selectable);
	levelSpriteSheet.push_back(groundTile02Clickable);
	LevelEditorTiles* groundTile03 = new LevelEditorTiles(ground);
	groundTile03->setPosition(GROUND_TILE_ROW_ONE_X + TILE_SIZE + TILE_SIZE + TILE_SIZE, GROUND_TILE_ROW_ONE_Y);
	groundTile03->setGroundTile("03");
	Clickable groundTile03Clickable = Clickable(GROUND_TILE_ROW_ONE_X +  TILE_SIZE + TILE_SIZE + TILE_SIZE, GROUND_TILE_ROW_ONE_Y, TILE_SIZE, TILE_SIZE, groundTile03, selectable);
	levelSpriteSheet.push_back(groundTile03Clickable);
	LevelEditorTiles* groundTile04 = new LevelEditorTiles(ground);
	groundTile04->setPosition(GROUND_TILE_ROW_TWO_X, GROUND_TILE_ROW_TWO_Y);
	groundTile04->setGroundTile("04");
	Clickable groundTile04Clickable = Clickable(GROUND_TILE_ROW_TWO_X, GROUND_TILE_ROW_TWO_Y, TILE_SIZE, TILE_SIZE, groundTile04, selectable);
	levelSpriteSheet.push_back(groundTile04Clickable);
	LevelEditorTiles* groundTile05 = new LevelEditorTiles(ground);
	groundTile05->setPosition(GROUND_TILE_ROW_TWO_X + TILE_SIZE, GROUND_TILE_ROW_TWO_Y);
	groundTile05->setGroundTile("05");
	Clickable groundTile05Clickable = Clickable(GROUND_TILE_ROW_TWO_X + TILE_SIZE, GROUND_TILE_ROW_TWO_Y, TILE_SIZE, TILE_SIZE, groundTile05, selectable);
	levelSpriteSheet.push_back(groundTile05Clickable);
	LevelEditorTiles* groundTile06 = new LevelEditorTiles(ground);
	groundTile06->setPosition(GROUND_TILE_ROW_TWO_X + TILE_SIZE + TILE_SIZE, GROUND_TILE_ROW_TWO_Y);
	groundTile06->setGroundTile("06");
	Clickable groundTile06Clickable = Clickable(GROUND_TILE_ROW_TWO_X + TILE_SIZE + TILE_SIZE, GROUND_TILE_ROW_TWO_Y, TILE_SIZE, TILE_SIZE, groundTile06, selectable);
	levelSpriteSheet.push_back(groundTile06Clickable);
	LevelEditorTiles* groundTile07 = new LevelEditorTiles(ground);
	groundTile07->setPosition(GROUND_TILE_ROW_TWO_X + TILE_SIZE + TILE_SIZE + TILE_SIZE, GROUND_TILE_ROW_TWO_Y);
	groundTile07->setGroundTile("07");
	Clickable groundTile07Clickable = Clickable(GROUND_TILE_ROW_TWO_X + TILE_SIZE + TILE_SIZE + TILE_SIZE, GROUND_TILE_ROW_TWO_Y, TILE_SIZE, TILE_SIZE, groundTile07, selectable);
	levelSpriteSheet.push_back(groundTile07Clickable);
	LevelEditorTiles* groundTile08 = new LevelEditorTiles(ground);
	groundTile08->setPosition(GROUND_TILE_ROW_THREE_X, GROUND_TILE_ROW_THREE_Y);
	groundTile08->setGroundTile("08");
	Clickable groundTile08Clickable = Clickable(GROUND_TILE_ROW_THREE_X, GROUND_TILE_ROW_THREE_Y, TILE_SIZE, TILE_SIZE, groundTile08, selectable);
	levelSpriteSheet.push_back(groundTile08Clickable);
	LevelEditorTiles* groundTile09 = new LevelEditorTiles(ground);
	groundTile09->setPosition(GROUND_TILE_ROW_THREE_X + TILE_SIZE, GROUND_TILE_ROW_THREE_Y);
	groundTile09->setGroundTile("09");
	Clickable groundTile09Clickable = Clickable(GROUND_TILE_ROW_THREE_X + TILE_SIZE, GROUND_TILE_ROW_THREE_Y, TILE_SIZE, TILE_SIZE, groundTile09, selectable);
	levelSpriteSheet.push_back(groundTile09Clickable);
	LevelEditorTiles* groundTile10 = new LevelEditorTiles(ground);
	groundTile10->setPosition(GROUND_TILE_ROW_THREE_X + TILE_SIZE + TILE_SIZE, GROUND_TILE_ROW_THREE_Y);
	groundTile10->setGroundTile("10");
	Clickable groundTile10Clickable = Clickable(GROUND_TILE_ROW_THREE_X + TILE_SIZE + TILE_SIZE, GROUND_TILE_ROW_THREE_Y, TILE_SIZE, TILE_SIZE, groundTile10, selectable);
	levelSpriteSheet.push_back(groundTile10Clickable);

	saveIcon = Clickable(SAVE_ICON_X, SAVE_ICON_Y, TILE_SIZE, TILE_SIZE, nullptr, selectable);
	saveIcon.setClickableIcon(SAVE_ICON_FILENAME);

	int groundXPosition = MORA_AREA_WIDTH;
	int groundYPosition = DEFAULT_ZERO;
	for(int i = DEFAULT_ZERO; i < TILES_IN_HEIGHT; i++)
	{
		for(int x = DEFAULT_ZERO; x < TILES_IN_WIDTH; x++)
		{
			Clickable groundClickable = Clickable(groundXPosition, groundYPosition, TILE_SIZE, TILE_SIZE, nullptr, selectable);
			groundClickable.setClickableIcon(EMPTY_TILE_FILENAME);
			groundTiles.push_back(groundClickable);
			groundXPosition += TILE_SIZE;
		}
		groundYPosition += TILE_SIZE;
		groundXPosition = MORA_AREA_WIDTH;
	}

	levelEditorTextBox.init(TEXT_BOX_X, TEXT_BOX_Y);
	currentlySaving = false;
	currentlyCreatingReaderItem = false;
	currentlyCreatingDoor = false;
	currentlyCreatingDialogue = false;
	dialogueCreationStage = DEFAULT_ZERO;
}

void LevelEditor::update(double delta)
{
	//Updates the mouse.
	updateMouseState();
	//Find out which section of the editor the mouse is in.
	mouseQuad(mouseXPosition, mouseYPosition);

	if(!currentlySaving && !currentlyCreatingReaderItem && !currentlyCreatingDoor && !currentlyCreatingDialogue)
	{
		if(SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(1))
		{
			if(currentMouseQuad == gridQuad)
			{
				placer();
			}
			else if(currentMouseQuad != NAQuad)
			{
				changeCurrentMouse();
				if(currentMouseQuad == monsterQuad)
				{
					monsterArrows();
				}
				else if(currentMouseQuad == itemQuad)
				{
					itemArrows();
				}
				else if(currentMouseQuad == groundQuad)
				{
					pickGroundTile(mouseXPosition, mouseYPosition);
				}
				else if(currentMouseQuad == saveQuad)
				{
					levelEditorTextBox.enable();
					levelEditorTextBox.setInstructionString("Please enter text file to save:");
					currentlySaving = true;
				}
			}
		}
		else if((SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(3)) && currentMouseQuad == gridQuad)
		{
			deletion();
		}
	}

	if(currentlySaving)
	{
		if(!levelEditorTextBox.isEnabled())
		{
			createLevelFile(levelEditorTextBox.getText());
			currentlySaving = false;
			levelEditorTextBox.cleanText();
			redraw = true;
		}
	}

	if(currentlyCreatingReaderItem)
	{
		if(!levelEditorTextBox.isEnabled())
		{
			itemTiles[itemTiles.size() - 1].setTileReaderString("Art/" + levelEditorTextBox.getText());
			currentlyCreatingReaderItem = false;
			levelEditorTextBox.cleanText();
			redraw = true;
		}
	}

	if(currentlyCreatingDoor)
	{
		if(!levelEditorTextBox.isEnabled())
		{
			for each(Clickable groundT in groundTiles)
			{
				if(!groundT.isTileNull())
				{
					if(std::atoi(groundT.getTile().getGroundTile().c_str()) == DOOR_TILE_NUMBER && groundT.getTile().getNextLevelString() == "")
					{
						if(!levelEditorTextBox.isEnabled())
						{
							groundT.setTilesNextLevelString("Levels/" + levelEditorTextBox.getText());
							currentlyCreatingDoor = false;
							levelEditorTextBox.cleanText();
							redraw = true;
						}
					}
				}
			}
		}
	}

	if(currentlyCreatingDialogue)
	{
		if(dialogueCreationStage == 0 && !levelEditorTextBox.isEnabled())
		{
			//Set the dialouge image.
			itemTiles[itemTiles.size() - 1].setTileDialogueString("Art/" + levelEditorTextBox.getText());
			if(itemTiles[itemTiles.size() - 1].getTile().getDialogueString() != "")
			{
				redraw = true;
				dialogueCreationStage++;
				levelEditorTextBox.cleanText();
				levelEditorTextBox.disable();
				levelEditorTextBox.enable();
				levelEditorTextBox.setInstructionString("Please enter dialogue icon.");
			}
		}
		else if(dialogueCreationStage == 1 && !levelEditorTextBox.isEnabled())
		{
			if(levelEditorTextBox.getText() != "")
			{
				//Set the dialogue icon.
				itemTiles[itemTiles.size() - 1].setTileDialogueIcon("Art/" + levelEditorTextBox.getText());
				levelEditorTextBox.cleanText();
				if(itemTiles[itemTiles.size() - 1].getTile().getIconString() != "")
				{
					redraw = true;
					dialogueCreationStage++;
					levelEditorTextBox.cleanText();
					levelEditorTextBox.disable();
					levelEditorTextBox.enable();
					levelEditorTextBox.setInstructionString("Please enter dialogue speaker.");
				}
			}
			else
			{
				levelEditorTextBox.enable();
				levelEditorTextBox.setInstructionString("Please enter dialogue speaker.");
			}
		}
		else if(dialogueCreationStage == 2 && !levelEditorTextBox.isEnabled())
		{
			//Type "NONE" if there is no character image for this.
			if(levelEditorTextBox.getText() != "")
			{
				//Set the dialogue character icon.
				itemTiles[itemTiles.size() - 1].setTileDialogueCharacter("Art/" + levelEditorTextBox.getText());
				dialogueCreationStage = DEFAULT_ZERO;
				redraw = true;
				currentlyCreatingDialogue = false;
				levelEditorTextBox.cleanText();
				levelEditorTextBox.disable();
			}
			else
			{
				levelEditorTextBox.enable();
			}
		}
	}

	leftMousePast = leftMouseNow;
	levelEditorTextBox.update(delta);
}

//Draw the level editor.
void LevelEditor::render(SDL_Surface* dest)
{	
	if(redraw)
	{
		SDL_FillRect(dest ,NULL,0x000000);
		for each(Clickable cli in groundTiles)
		{
			cli.render(dest);
		}
		redraw = false;
	}

	moraIcon.render(dest);
	
	monsterLeft.render(dest);
	monsterRight.render(dest);
	
	monsterIcon[currentMonster].render(dest);

	itemUp.render(dest);
	itemDown.render(dest);
	itemLeft.render(dest);
	itemRight.render(dest);

	itemIcon[currentItemX].render(dest);
	
	saveIcon.render(dest);

	for each(Clickable cli in levelSpriteSheet)
	{
		cli.render(dest);
	}
	
	for each(Clickable cli in groundTiles)
	{
		cli.render(dest);
	}
	for each(Clickable cli in monsterTiles)
	{
		cli.render(dest);
	}
	for each(Clickable cli in itemTiles)
	{
		cli.render(dest);
	}

	moraTile.render(dest);

	levelEditorTextBox.render(dest);
}

//Clean up the level editor.
void LevelEditor::cleanup()
{
	//We do not delete the current mouse because
	//It is just a shallow copy of the other icons.
	//If we delete it, it will be a double deletion.
	//currentMouse.cleanup();
	
	gridArea.cleanup();
	moraArea.cleanup();
	monsterArea.cleanup();
	itemArea.cleanup();
	groundTileArea.cleanup();

	moraIcon.cleanup();
	monsterLeft.cleanup();
	monsterRight.cleanup();
	
	for each (Clickable cli in monsterIcon)
	{
		cli.cleanup();
	}

	itemUp.cleanup();
	itemDown.cleanup();
	itemLeft.cleanup();
	itemRight.cleanup();

	for each(Clickable cli in itemIcon)
	{
		cli.cleanup();
	}
	
	for each(Clickable cli in levelSpriteSheet)
	{
		cli.cleanup();
	}
	
	moraTile.cleanup();
	
	for each(Clickable cli in groundTiles)
	{
		cli.cleanup();
	}
	for each(Clickable cli in monsterTiles)
	{
		cli.cleanup();
	}
	for each(Clickable cli in itemTiles)
	{
		cli.cleanTile();
	}

	saveIcon.cleanup();
	saveArea.cleanup();

	levelEditorTextBox.cleanup();
}

//Figure out the quad. of the mouse.
void LevelEditor::mouseQuad(double xPos, double yPos)
{
	CollisionBox mouseBox;
	mouseBox.x = xPos;
	mouseBox.y = yPos;
	mouseBox.w = 1;
	mouseBox.h = 1;

	if(checkIfOn(xPos, yPos, gridArea))
	{
		currentMouseQuad = gridQuad;
	}
	else if(checkIfOn(xPos, yPos, moraArea))
	{
		currentMouseQuad = moraQuad;
	}
	else if(checkIfOn(xPos, yPos, monsterArea))
	{
		currentMouseQuad = monsterQuad;
	}
	else if(checkIfOn(xPos, yPos, itemArea))
	{
		currentMouseQuad = itemQuad;
	}
	else if(checkIfOn(xPos, yPos, groundTileArea))
	{
		currentMouseQuad = groundQuad;
	}
	else if(checkIfOn(xPos, yPos, saveArea))
	{
		currentMouseQuad = saveQuad;
	}
}

//Place the tile.  If it is a ground tile then we find the closest ground tile to
//place it at.
void LevelEditor::placer()
{
	if(currentMouse.getTileType() == ground)
	{
		int closestTile = NA_VALUE;
		for(int i = 0; i < groundTiles.size(); i++)
		{
			CollisionBox mouseBox;
			mouseBox.x = mouseXPosition;
			mouseBox.y = mouseYPosition;
			mouseBox.w = 1;
			mouseBox.h = 1;
			CollisionBox gridBox;
			gridBox.x = groundTiles[i].getBox().x;
			gridBox.y = groundTiles[i].getBox().y;
			gridBox.w = groundTiles[i].getBox().w;
			gridBox.h = groundTiles[i].getBox().h;
			if(checkCollision(mouseBox, gridBox))
			{
				closestTile = i;
			}
		}
		if(closestTile != NA_VALUE)
		{
			LevelEditorTiles* newGroundTile = new LevelEditorTiles(ground);
			newGroundTile->setPosition(groundTiles[closestTile].getBox().x, groundTiles[closestTile].getBox().y);
			newGroundTile->setGroundTile(currentMouse.getGroundTile());
			groundTiles[closestTile] = Clickable(groundTiles[closestTile].getBox().x, groundTiles[closestTile].getBox().y, TILE_SIZE, TILE_SIZE, newGroundTile, tiled);
			if(std::atoi(currentMouse.getGroundTile().c_str()) == DOOR_TILE_NUMBER)
			{
				currentlyCreatingDoor = true;
				levelEditorTextBox.enable();
				levelEditorTextBox.setInstructionString("Please enter level text file corresponding to this door:");
			}
		}
	}
	else if(currentMouse.getTileType() == mora && leftMousePast == false)
	{
		LevelEditorTiles* newMoraTile = new LevelEditorTiles(mora);
		newMoraTile->setPosition(mouseXPosition, mouseYPosition);
		Clickable newMoraClickablePlaced((int)mouseXPosition, (int)mouseYPosition, TILE_SIZE, TILE_SIZE, newMoraTile, tiled);
		moraTile = newMoraClickablePlaced;
	}
	else if(currentMouse.getTileType() == monster && leftMousePast == false)
	{
		LevelEditorTiles* newMonsterTile = new LevelEditorTiles(monster);
		newMonsterTile->setMonsterType(monsterIcon[currentMonster].getTile().getMonsterType());
		newMonsterTile->setPosition(mouseXPosition, mouseYPosition);
		Clickable newMonsterClickablePlaced((int)mouseXPosition, (int)mouseYPosition, TILE_SIZE, TILE_SIZE, newMonsterTile, tiled);
		monsterTiles.push_back(newMonsterClickablePlaced);
	}
	else if(currentMouse.getTileType() == item && leftMousePast == false)
	{
		LevelEditorTiles* newItemTile = new LevelEditorTiles(item);
		newItemTile->setItemType(itemIcon[currentItemX].getTile().getItemType());
		newItemTile->setPosition(mouseXPosition, mouseYPosition);
		newItemTile->setReaderTileType(itemIcon[currentItemX].getTile().getReaderTileType());
		Clickable newItemClickablePlaced((int)mouseXPosition, (int)mouseYPosition, TILE_SIZE, TILE_SIZE, newItemTile, tiled);
		itemTiles.push_back(newItemClickablePlaced);
		if(itemIcon[currentItemX].getTile().getItemType() == reader)
		{
			levelEditorTextBox.enable();
			levelEditorTextBox.setInstructionString("Please enter reader print print art:");
			currentlyCreatingReaderItem = true;
		}
		else if(itemIcon[currentItemX].getTile().getItemType() == dialogue)
		{
			levelEditorTextBox.enable();
			levelEditorTextBox.setInstructionString("Please enter dialouge art:");
			currentlyCreatingDialogue = true;
		}
	}
}

//Figure out what quad. the mouse is in and pick up the tile when clicked in
//the quadrant.
void LevelEditor::changeCurrentMouse()
{
	if(currentMouseQuad == moraQuad)
	{
		currentMouse = moraIcon.getTile();
	}
	else if(currentMouseQuad == monsterQuad)
	{
		currentMouse = monsterIcon[currentMonster].getTile();
	}
	else if(currentMouseQuad == itemQuad)
	{
		currentMouse = itemIcon[currentItemX].getTile();
	}
	else if(currentMouseQuad == groundQuad)
	{
		currentMouse = pickGroundTile(mouseXPosition, mouseYPosition);
	}
}

//If monster arrows are clicked we move the monsters accordingly.
void LevelEditor::monsterArrows()
{
	if(currentMouseQuad == monsterQuad)
	{
		if(checkIfLeftClickedOnOnce(mouseXPosition, mouseYPosition, monsterLeft))
		{
			if(currentMonster > DEFAULT_ZERO)
			{
				currentMonster--;
				redraw = true;
			}
		}
		else if(checkIfLeftClickedOnOnce(mouseXPosition, mouseYPosition, monsterRight))
		{
			if(currentMonster < monsterIcon.size() - 1)
			{
				currentMonster++;
				redraw = true;
			}
		}
	}
}

//If item arrows are clicked we move the items accordingly.
void LevelEditor::itemArrows()
{
	if(currentMouseQuad == itemQuad)
	{
		if(checkIfLeftClickedOnOnce(mouseXPosition, mouseYPosition, itemLeft))
		{
			if(currentItemY == READERS_Y_LEVEL)
			{
				currentItemX--;
				redraw = true;
				
				if(currentItemX < ITEM_ZERO_START)
				{
					currentItemX = ITEM_ZERO_END;
				}
			}
			else if(currentItemY == POWER_UPS_Y_LEVEL)
			{
				currentItemX--;
				redraw = true;
				if(currentItemX < ITEM_ONE_START)
				{
					currentItemX = ITEM_ONE_END;
				}
			}
		}
		else if(checkIfLeftClickedOnOnce(mouseXPosition, mouseYPosition, itemRight))
		{
			if(currentItemY == READERS_Y_LEVEL)
			{
				currentItemX++;
				redraw = true;
				if(currentItemX > ITEM_ZERO_END)
				{
					currentItemX = ITEM_ZERO_START;
				}
			}
			else if(currentItemY == POWER_UPS_Y_LEVEL)
			{	
				currentItemX++;
				redraw = true;
				if(currentItemX > ITEM_ONE_END)
				{
					currentItemX = ITEM_ONE_START;
				}
			}
		}
		else if(checkIfLeftClickedOnOnce(mouseXPosition, mouseYPosition, itemUp))
		{
			currentItemY--;
			if(currentItemY < READERS_Y_LEVEL)
			{
				currentItemX = ITEM_ONE_START;
				currentItemY = POWER_UPS_Y_LEVEL;
			}
			else if(currentItemY == READERS_Y_LEVEL)
			{
				currentItemX = ITEM_ZERO_START;
			}
			else if(currentItemY == POWER_UPS_Y_LEVEL)
			{
				currentItemX = ITEM_ONE_START;
			}
			redraw = true;
		}
		else if(checkIfLeftClickedOnOnce(mouseXPosition, mouseYPosition, itemDown))
		{
			currentItemY++;
			if(currentItemY > ITEM_MAX)
			{
				currentItemX = ITEM_ZERO_START;
				currentItemY = DEFAULT_ZERO;
			}
			else if(currentItemY == DEFAULT_ZERO)
			{
				currentItemX = ITEM_ZERO_START;
			}
			else if(currentItemY == POWER_UPS_Y_LEVEL)
			{
				currentItemX = ITEM_ONE_START;
			}
			redraw = true;
		}
	}
}

//If the mouse clicks on a gound tile we copy that tuiles data.
LevelEditorTiles LevelEditor::pickGroundTile(double xPos, double yPos)
{
	for each(Clickable cli in levelSpriteSheet)
	{
		if(checkIfLeftClickedOn(xPos, yPos, cli))
		{
			return cli.getTile();
		}
	}

	return levelSpriteSheet[DEFAULT_ZERO].getTile();
}

//Update the state of the mouse.
void LevelEditor::updateMouseState()
{
	SDL_PumpEvents();
	SDL_GetMouseState(&mouseXPosition, &mouseYPosition);
	if(SDL_GetMouseState(NULL, NULL) && SDL_BUTTON(1))
	{
		leftMouseNow = true;
	}
	else
	{
		leftMouseNow = false;
	}
}

//Checks right clicks to delete tiles.  It will always delete the top clickable first.
void LevelEditor::deletion()
{
	bool monsterErased = false;
	std::vector<Clickable>::iterator monstersIterator;
	for(monstersIterator = monsterTiles.begin(); monstersIterator != monsterTiles.end(); monstersIterator++)
	{
		if(!monsterErased)
		{
			CollisionBox monsterBox;
			monsterBox.x = monstersIterator->getBox().x;
			monsterBox.y = monstersIterator->getBox().y;
			monsterBox.w = monstersIterator->getBox().w;
			monsterBox.h = monstersIterator->getBox().h;

			CollisionBox mouseBox;
			mouseBox.x = mouseXPosition;
			mouseBox.y = mouseYPosition;
			mouseBox.w = TILE_SIZE/2;
			mouseBox.h = TILE_SIZE/2;

			if(checkCollision(mouseBox, monsterBox))
			{
				monsterTiles.erase(monstersIterator);
				monstersIterator = monsterTiles.end() - 1;
				monsterErased = true;
				redraw = true;
			}
		}
	}

	bool itemErased = false;
	if(!monsterErased)
	{
		std::vector<Clickable>::iterator itemIterator;
		for(itemIterator = itemTiles.begin(); itemIterator != itemTiles.end(); itemIterator++)
		{
			if(!itemErased)
			{
				CollisionBox itemBox;
				itemBox.x = itemIterator->getBox().x;
				itemBox.y = itemIterator->getBox().y;
				itemBox.w = itemIterator->getBox().w;
				itemBox.h = itemIterator->getBox().h;

				CollisionBox mouseBox;
				mouseBox.x = mouseXPosition;
				mouseBox.y = mouseYPosition;
				mouseBox.w = TILE_SIZE/2;
				mouseBox.h = TILE_SIZE/2;

				if(checkCollision(mouseBox, itemBox))
				{
					itemTiles.erase(itemIterator);
					itemIterator = itemTiles.end() - 1;
					itemErased = true;
					redraw = true;
				}
			}
		}
	}

	if(!monsterErased && !itemErased)
	{
		int xDifference = NA_VALUE;
		int yDifference = NA_VALUE;
		int closestTile = NA_VALUE;
		for(int i = 0; i < groundTiles.size(); i++)
		{
			if((std::abs(mouseXPosition - groundTiles[i].getBox().x) < xDifference) || (std::abs(mouseYPosition - groundTiles[i].getBox().y) < yDifference))
			{
				xDifference = std::abs(mouseXPosition - groundTiles[i].getBox().x);
				yDifference = std::abs(mouseYPosition - groundTiles[i].getBox().y);
				closestTile = i;
			}
		}
		Clickable groundClickable = Clickable(groundTiles[closestTile].getBox().x, groundTiles[closestTile].getBox().y, TILE_SIZE, TILE_SIZE, nullptr, selectable);
		groundClickable.setClickableIcon(EMPTY_TILE_FILENAME);
		groundTiles[closestTile] = groundClickable;
	}
}

void LevelEditor::createLevelFile(std::string  fileNameAndLocation)
{
	//Output the level in text file.
	std::ofstream levelOutputStream;
	levelOutputStream.open("Levels/" + fileNameAndLocation);
	//Print out the tiles.
	int xPos = DEFAULT_ZERO;
	for(int i = 0; i < groundTiles.size(); i++)
	{
		if(!groundTiles[i].isTileNull() && groundTiles[i].getTile().getGroundTile() != "")
		{
			levelOutputStream << groundTiles[i].getTile().getGroundTile();
		}
		else
		{
			levelOutputStream << "00";
		}
		xPos++;
		if(xPos < TILES_IN_WIDTH)
		{
			levelOutputStream << " "; 
		}
		else
		{
			levelOutputStream << "\n";
			xPos = DEFAULT_ZERO;
		}
	}

	levelOutputStream << "\n";

	//Print out the next level tile.
	bool noNextLevels = false;
	for(int i = 0; i < groundTiles.size(); i++)
	{
		if(!groundTiles[i].isTileNull() && std::atoi(groundTiles[i].getTile().getGroundTile().c_str()) == DOOR_TILE_NUMBER)
		{
			noNextLevels = true;
			//We have to translate the level editor tile to thr right position.  We have to do this becayse
			//the level editor is larger than the game screen.
			levelOutputStream << groundTiles[i].getTile().getXPosition() - MORA_AREA_WIDTH;
			levelOutputStream << " ";
			levelOutputStream << groundTiles[i].getTile().getYPosition();
			levelOutputStream << "\n";
			levelOutputStream << groundTiles[i].getTile().getNextLevelString();
			levelOutputStream << "\n";
		}
	}
	if(!noNextLevels)
	{
		levelOutputStream << "\n";
	}
	
	levelOutputStream << "\n";


	//Print out the Mora position.
	if(!moraTile.isTileNull())
	{
		levelOutputStream << moraTile.getTile().getXPosition() - MORA_AREA_WIDTH;
		levelOutputStream << " ";
		levelOutputStream << moraTile.getTile().getYPosition();
		levelOutputStream << "\n";
	}
	else
	{
		levelOutputStream << "0";
		levelOutputStream << " ";
		levelOutputStream << "0";
		levelOutputStream << "\n";
	}

	levelOutputStream << "\n";

	//Print out the monsters.
	for(int i = 0; i < monsterTiles.size(); ++i)
	{
		if(monsterTiles[i].getTile().getMonsterType() == Bbaby)
		{
			levelOutputStream << BBABY_MONSTER_NUMBER;
		}
		levelOutputStream << "\n";
		levelOutputStream << monsterTiles[i].getTile().getXPosition() - MORA_AREA_WIDTH;
		levelOutputStream << " ";
		levelOutputStream << monsterTiles[i].getTile().getYPosition();
		levelOutputStream << "\n";
	}
	if(monsterTiles.size() == DEFAULT_ZERO)
	{
		levelOutputStream << "\n";
	}

	levelOutputStream << "\n";

	//Print out the items.
	for(int i = 0; i < itemTiles.size(); i++)
	{
		if(itemTiles[i].getTile().getItemType() == reader)
		{
			levelOutputStream << READER_ITEM_NUMBER;
			levelOutputStream << "\n";
		}
		else if(itemTiles[i].getTile().getItemType() == powerUp)
		{
			levelOutputStream << POWER_UP_ITEM_NUMBER;
			levelOutputStream << "\n";
		}
		else if(itemTiles[i].getTile().getItemType() == dialogue)
		{
			levelOutputStream <<DIALOGUE_ITEM_NUMBER;
			levelOutputStream << "\n";
		}
		if(itemTiles[i].getTile().getItemType() == reader && itemTiles[i].getTile().getReaderTileType() == clipboardReaderType)
		{
			levelOutputStream << READER_CLIPBOARD_ITEM_NUMBER;
			levelOutputStream << "\n";
		}
		levelOutputStream << itemTiles[i].getTile().getXPosition() - MORA_AREA_WIDTH;
		levelOutputStream << " ";
		levelOutputStream << itemTiles[i].getTile().getYPosition();
		levelOutputStream << "\n";
		if(itemTiles[i].getTile().getItemType() == reader)
		{
			levelOutputStream << itemTiles[i].getTile().getReaderPrintString();
			levelOutputStream << "\n";
		}
		else if(itemTiles[i].getTile().getItemType() == dialogue)
		{
			levelOutputStream << itemTiles[i].getTile().getDialogueString();
			levelOutputStream << "\n";
			levelOutputStream << itemTiles[i].getTile().getIconString();
			levelOutputStream << "\n";
			levelOutputStream << itemTiles[i].getTile().getCharacterString();
			levelOutputStream << "\n";
		}
	}
	if(itemTiles.size() == DEFAULT_ZERO)
	{
		levelOutputStream << "\n";
	}

	levelOutputStream << "\n";

	levelOutputStream.close();
}

//Check if mouse if clicked on a clickable.
bool LevelEditor::checkIfOn(double xPos, double yPos, Clickable cli)
{
	CollisionBox mouseBox;
	mouseBox.x = xPos;
	mouseBox.y = yPos;
	mouseBox.w = 1;
	mouseBox.h = 1;
	CollisionBox clickBox;
	clickBox.x = cli.getBox().x;
	clickBox.y = cli.getBox().y;
	clickBox.w = cli.getBox().w;
	clickBox.h = cli.getBox().h;

	if(checkCollision(mouseBox, clickBox))
	{
		return true;
	}

	return false;
}

//Check if mouse if left clicked on a clickable.
bool LevelEditor::checkIfLeftClickedOn(double xPos, double yPos, Clickable cli)
{
	CollisionBox mouseBox;
	mouseBox.x = xPos;
	mouseBox.y = yPos;
	mouseBox.w = 1;
	mouseBox.h = 1;
	CollisionBox clickBox;
	clickBox.x = cli.getBox().x;
	clickBox.y = cli.getBox().y;
	clickBox.w = cli.getBox().w;
	clickBox.h = cli.getBox().h;

	if(checkCollision(mouseBox, clickBox) && SDL_BUTTON(1))
	{
		return true;
	}

	return false;
}

//Check if mouse if right clicked on a clickable.
bool LevelEditor::checkIfRightClickedOn(double xPos, double yPos, Clickable cli)
{
	CollisionBox mouseBox;
	mouseBox.x = xPos;
	mouseBox.y = yPos;
	mouseBox.w = 1;
	mouseBox.h = 1;
	CollisionBox clickBox;
	clickBox.x = cli.getBox().x;
	clickBox.y = cli.getBox().y;
	clickBox.w = cli.getBox().w;
	clickBox.h = cli.getBox().h;

	if(checkCollision(mouseBox, clickBox) && SDL_BUTTON(3))
	{
		return true;
	}

	return false;
}

//Check if mouse if left clicked and released on a clickable.
bool LevelEditor::checkIfLeftClickedOnOnce(double xPos, double yPos, Clickable cli)
{
	CollisionBox mouseBox;
	mouseBox.x = xPos;
	mouseBox.y = yPos;
	mouseBox.w = 1;
	mouseBox.h = 1;
	CollisionBox clickBox;
	clickBox.x = cli.getBox().x;
	clickBox.y = cli.getBox().y;
	clickBox.w = cli.getBox().w;
	clickBox.h = cli.getBox().h;

	if(checkCollision(mouseBox, clickBox) && SDL_BUTTON(1) && leftMousePast == false)
	{
		return true;
	}

	return false;
}