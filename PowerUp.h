#pragma once
#ifndef POWERUP_H
#define POWERUP_H

#include "Items.h"

//This class gives Mora a power up.

class PowerUp : public Items
{
public:
	//Most of these functions are explained in the
	//"Items.h"
	virtual bool init();
	//Init. the power up at a position.
	bool init(double xPos, double yPos);
	virtual void cleanup();
	virtual void update(double delta, Mora mora);
	virtual void render(SDL_Surface* dest);
	virtual void renderTop(SDL_Surface* dest);
	virtual void moraChanges(Mora* mora);
	virtual void monsterChanges(std::vector<Monsters*> mons);
	virtual CollisionBox getItemCollisionBox();
	virtual bool isFinished();
private:
	//The image of the power up.
	SDL_Surface* powerUpSprite;
	//The collision box for the item.
	CollisionBox powerUpBox;
	//The timer to keep track of when the power up runs out.
	double timer;
	//Bools to keep track of when the item is activated, started and finished.
	bool activated;
	bool firstStart;
	bool firstFinish;

	//Max time for the power up.
	static int const START_TIME = 5;
};

#endif